<?php
class BankAccount
{
   public $Balance; // member variable
   public $AccountNumber;
   public $CustomerName;

   public function _construct(){
      $this->AccountNumber = 0;//The "$this" means the member variable $AccountNumber
      $this->Balance = 0;
      $this->CustomerName = "";
   }

   public function withdrawal($Amount){
      $this->Balance -= $Amount;
   }

   public function deposit($Amount){
      $this->Balance += $Amount;
   }
/*
   public function setName($newName){
      $name = $this->newName;
   }

   public function getName() {
      return $this-> name;
   }
*/
}// end Person



if ( class_exists("BankAccount"))
   $Checking = new BankAccount();
else
   exit("The BankAccount class is not available!");

print("Your checking account balance is $Checking->Balance\n");
$Cash = 200;
$Checking->deposit($Cash);
print("After deposit, your checking account balance is $Checking->Balance\n");

$Cash = 600;
$Checking->withdrawal($Cash);
print("After withdrawal, your checking account balance is $Checking->Balance\n");
?>
