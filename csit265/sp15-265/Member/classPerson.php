<?php
class BankAccount
{
	private $Balance;
	private $AccountNumber;
	private $CustomerName;

	function _construct(){
		$this->AccountNumber = 0;
		$this->Balance = 0;
		$this->CustomerName = "";
	}


	public function withdrawal($Amount){
		$this->Balance -= $Amount;
	}

	public function deposit($Amount){
		$this->Balance += $Amount;
	}

	public function setBalance($newValue){
		$this->Balance = $newValue;
	}

	public function getBalance() {
		return $this->Balance;
	}
}// end Person

?>