<?php
class BankAccount
{
	private $Balance;
	private $AccountNumber;
	private $CustomerName;

	function _construct(){
		$this->AccountNumber = 0;
		$this->Balance = 0;
		$this->CustomerName = "";
	}

	function _destruct(){

	}

	public function withdrawal($Amount){
		$this->Balance -= $Amount;
	}

	public function deposit($Amount){
		$this->Balance += $Amount;
	}

	public function setBalance($newValue){
		$this->Balance = $newValue;
	}

	public function getBalance() {
		return $this->Balance;
	}

}// end Person



if ( class_exists("BankAccount"))
	$Checking = new BankAccount();
else
	exit("The BankAccount class is not available!");

$Checking->setBalance(1000);
$bal = $Checking->getBalance();
print("Your checking account balance is $bal\n");

$Cash = 200;
$Checking->deposit($Cash);
$bal = $Checking->getBalance();
print("After deposit, your checking account balance is $bal\n");

$Cash = 600;
$Checking->withdrawal($Cash);
$bal = $Checking->getBalance();
print("After withdrawal, your checking account balance is $bal\n");
?>