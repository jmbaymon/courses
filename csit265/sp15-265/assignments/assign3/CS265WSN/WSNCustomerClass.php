<?php
class customer{
   private $first;
   private $last;
   private $email;
   private $alias;
   private $accountNum;
   private  $pinCode;
   private $balance;
   private $limit;

public function __construct($first,$last, $email,$alias,$accountNum,$pinCode,$balance,$limit){

$this-> first = $first;
$this-> last =$last;
$this-> email =$email;
$this-> alias = $alias;
$this-> accountNum = $accountNum;
$this-> pinCode = $pinCode;
$this-> balance = $balance;
$this-> limit = $limit;
}
//getter functions
public function getFirstName(){

 return $this-> first;

   }
 public function getLastName(){
    return $this -> last;

   }
public function getEmail(){

return $this -> email;

}
public function getAlias(){

return $this -> alias;

}
public function getAccountNum(){

return $this -> accountNum;

}
public function getPinCode(){

return $this -> pinCode;

}
public function getBalance(){

return $this -> balance;

}
public function getLimit(){

return $this -> limit;

}
//setters
public function setFirstName($fname){
$this -> first = $fname;
 return $this-> first;

   }
 public function setLastName($lname){
   $this -> last = $lname;
    return $this -> last;

   }
public function setEmail($mail){

$this -> email = $mail;
return $this -> email;


}
public function setAlias($nickname){
$this -> alias = $nickname;
return $this -> alias;

}
public function setAccountNum($number){
$this -> accountNum = $number;
return $this -> accountNum;

}
public function setPinCode($code){
$this -> pinCode  = $code;
return $this -> pinCode;

}
public function setBalance($bal){
$this -> balance  = $bal;
return $this -> balance;

}
public function setLimit($max){
$this -> limit  = $max;
return $this -> limit;

}


}
?>
