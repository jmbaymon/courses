<?php
class Sales
{
   private $BkID;
   private $NumSold;

   function _construct($a, $b){
      $this->BkID = $a;
      $this->NumSold = $b;
   }

   // setters
   public function setBkID($newValue){
      $this->BkID = $newValue;
   }
   public function setNumSold($newValue){
      $this->NumSold = $newValue;
   }

   // getters
   public function getBkID(){
      return $this->BkID;
   }
   public function getNumSold(){
      return $this->NumSold;
   }


}// end Sales

?>
