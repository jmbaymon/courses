<html>
<head>
   <title> TU-Zon Any Book You Want </title>
   <link rel="stylesheet" type="text/css" href="stylesheet1.css"/>
</head>
<body>
<?php include "classBook.php"; ?>
<?php include "ClassSales.php"; ?>
<h1> TU-Zon Weekly Sales Report </h1>

<?php

function printList($bookList)
{
      // get array size
      $arrsize = sizeof($bookList);
      print ("Array size in printList is $arrsize\n");
      print ("<h2> Print array of objects </h2>");
      print("<hr>");

      print("<table border=\"1\">");
      // print column headings
      print("<th align=\"right\"> Book Title </th>");
      print("<th align=\"right\"> Book ID </th>");
      print("<th align=\"right\"> Weeks in Release </th>");
      print("<th align=\"right\"> Units Sold this Week</th>");
      print("<th align=\"right\"> Book Type </th>");
      print("<th align=\"right\"> Book Format</th>");
      print("<th align=\"right\"> Cost per Unit</th>");
      print("<th align=\"right\"> Weekly Gross </th>");
      print("</tr>");

      // print table row contents from array of objects
      //foreach ($bookList as $oneRec)
      for ( $idx = 0; $idx < $arrsize; $idx++)
      {
         print("<tr>");
         $oneRec = $bookList[$idx];
         $title = $oneRec->getTitle();
         $id = $oneRec->getBookID();
         $weeks = $oneRec->getWeeks();
         $sold = $oneRec->getWeekSales();
         $btype = $oneRec->getBkType();
         $bform = $oneRec->getBkFormat();
         $cost = $oneRec->getCost();
         $wkGross = $oneRec->getWkGross();

         print("<td align=\"right\"> $title</td>");
         print("<td align=\"right\"> $id</td>");
         print("<td align=\"right\"> $weeks</td>");
         print("<td align=\"right\"> $sold</td>");
         // print("<td align=\"right\"> $Btype</td>");
         // print("<td align=\"right\"> $Bform</td>");

         // display FULL book type
         if ( strncmp($btype,"t", 1 ) == 0)
            print("<td align=\"right\"> Textbook</td>");
         else if( strncmp($btype,"f", 1) == 0)
            print("<td align=\"right\"> Fiction </td>");
         else
            print("<td align=\"right\"> Nonfiction</td>");

         // display FULL book form
         if ( $bform[0] == "e")
            print("<td align=\"right\"> Electronic</td>");
         if( $bform[0] == "h")
            print("<td align=\"right\"> Hardcover </td>");
         if ($bform[0] == "p")
            print("<td align=\"right\"> Paperback </td>");

         print("<td align=\"right\"> $cost</td>");
         $fwkGross = number_format($wkGross, 2,'.','');
         print("<td align=\"right\"> $fwkGross</td>");
         print("</tr>");
      }// end foreach
      print("</table>");
 }// end printList function


function createBookList($bfile, &$bookList)
{
      // read one record from the input file
      $bookRec = fgets($bfile);

      // loop until end of file
      while (!feof($bfile))
      {
         //print("<h2> processing inside loop </h2>");
         $tempRec = new Book();
         $bkType = " ";
         $bkFormat = " ";
         // separate record into fields
         list($btitle, $weeks, $numSold, $bkType, $bkFormat, $cost,$id) = explode(",", $bookRec);
         $bkType = strtolower(trim($bkType));
         $bkFormat = strtolower(trim($bkFormat));
         $id = trim($id);

         //print("<h2> Book id = $id </h2>");
         // assign fields to tempRec object
         $tempRec->setTitle($btitle);
         $tempRec->setBookID($id);
         $tempRec->setWeeks($weeks);
         $tempRec->setWeekSales($numSold);
         $tempRec->setBkType($bkType);
         $tempRec->setBkFormat($bkFormat);
         $tempRec->setCost($cost);
         $tempRec->computeWkGross();

         // add tempRec to array - bookList
         $bookList[] = $tempRec;

         // get next record from input file
         $bookRec = fgets($bfile);
      }

      // close input file
      fclose($bfile);

}// end createBookList function

function createSalesList($sfile, &$salesList)
{
      // read one record from the input file
      $salesRec = fgets($sfile);

      // loop until end of file
      while (!feof($sfile))
      {
         //print("<h2> processing inside loop </h2>");
         $tempRec = new Sales();

         // separate record into fields
         list($id, $sold) = explode(",", $salesRec);
         $id = trim($id);

         //print("<h2> Book id in sales file = $id </h2>");
         // assign fields to tempRec object
         $tempRec->setBkID($id);
         $tempRec->setNumSold($sold);

         // add tempRec to array - bookList
         $salesList[] = $tempRec;

         // get next record from input file
         $salesRec = fgets($sfile);
      }

      // close input file
      fclose($sfile);

}// end createSalesList function

function printSales($sList)
{
      // get array size
      $arrsize = sizeof($sList);
      print ("Array size in printSales is $arrsize\n");
      print ("<h2> Print array of objects </h2>");
      print("<hr>");

      print("<table border=\"1\">");
      // print column headings
      print("<th align=\"right\"> Book ID </th>");
      print("<th align=\"right\"> Units Sold this Week</th>");
      print("</tr>");

      // print table row contents from array of objects
      //foreach ($bookList as $oneRec)
      for ( $idx = 0; $idx < $arrsize; $idx++)
      {
         print("<tr>");
         $oneRec = $sList[$idx];

         $id = $oneRec->getBkID();
         $sold = $oneRec->getNumSold();

         print("<td align=\"right\"> $id</td>");
         print("<td align=\"right\"> $sold</td>");

         print("</tr>");
      }// end foreach
      print("</table>");
 }// end printSales function

function updateSales(&$bookList, $salesList){
   $bkSize = sizeof($bookList);
   $sSize = sizeof($salesList);

   print("<h2> Books = $bkSize  and Sales = $sSize</h2>");

   for ( $sindex = 0; $sindex < $sSize; $sindex++)
   {
      $keyId = $salesList[$sindex]->getBkID();
      $found = false;
      print("<h2> Searching for update to book id $keyId </h2>");

      for ($bindex = 0; $bindex < $bkSize && !$found; $bindex++)
      {
         $bkId = $bookList[$bindex]->getBookID();
         //print("<h2> Compare to book id $bkId </h2>");
         if ( $keyId  == $bkId) {
            print("<h2>Match found! Book id is $bkId </h2>");
            $newSales = $salesList[$sindex]->getNumSold();
            $bookList[$bindex]->setWeekSales($newSales);
            //$bookList[$bindex]->computeWkGross();
            $found = true;
         }
      }
      print("<h2>****************</h2>");
   }// end for index
}// end updateSales


// program body
if ( !class_exists("Book"))
   exit("<h1>The Book class is not available! </h1>");

if ( !class_exists("Sales"))
   exit("<h1>The Sales class is not available! </h1>");

// get filename from form
$fileName = $_POST['fileName'];
print ("Book file name is $fileName<br>");

$sfileName = $_POST['sfileName'];
print ("Sales file name is $sfileName<br>");

// open file to read - exit if file does not exist
$bfile = fopen("$fileName", "r") or exit("<h1>$filename not found!</h1>");
$sfile = fopen("$sfileName", "r") or exit("<h1>$sfilename not found!</h1>");

// check for empty file
if( feof($bfile) || feof($sfile))
   exit("<h1>$fileName or $sfilename is empty</h1>");
else
{

   $bookList = array();
   createBookList($bfile, $bookList);
   printList($bookList);

   $salesList = array();
   createSalesList($sfile, $salesList);
   printSales($salesList);

   updateSales($bookList, $salesList);
   printList($bookList);

}// end if file not empty

?>
</body>
