<?php
   setcookie('userName', 'thomasc', time()+7200);

   $strSvname = $_SERVER['SERVER_NAME'];
   $strAddress = $_SERVER['REMOTE_ADDR'];
   $strBrowser = $_SERVER['HTTP_USER_AGENT'];
   $strPath = getenv(path);
   $strInfo="$strSvname::$strAddress::$strBrowser::$strPath";

   setcookie('myCookie', $strInfo, time()+7200);
   //echo "Cookie set: $strInfo<br>";

   // view all cookies
   //echo "View all cookies <br>";
   //print_r($_COOKIE);

   //echo "View all servers settings <br>";
   //print_r($_SERVER);

   //echo "View all posts <br>";
   //print_r($_POST);
?>
<html>
<head> <title> PHP Script Using Cookies </title></head>

<body>

<?php
   echo "<h2> Retrieving Cookies </h2>";

   // view all cookies
   echo "View all cookies <br>";
   print_r($_COOKIE);

   echo"<p> User Name: ".$_COOKIE['userName'];

   $strReadCookie = $_COOKIE['myCookie'];
   list( $sname, $addr, $cbrow, $path) = explode("::", $strReadCookie);
   echo "<p> Cookie retrieved: $strInfo</p>";

   echo "<p> Server Name is : $sname </p>";
   echo "<p> IP Address is : $addr </p>";
   echo "<p> Client Browser is : $cbrow </p>";
   echo "<p> Path : $path </p>";
 ?>
</body>
</html>
