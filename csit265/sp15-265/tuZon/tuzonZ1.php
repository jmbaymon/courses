<html>
<head>
   <title> TU-Zon Any Book You Want </title>
     <link rel="stylesheet" type="text/css" href="stylesheet1.css"/>
</head>
<body>
<?php include "classBook.php"; ?>
<h1> TU-Zon Weekly Sales Report </h1>

<?php
function printBookList($bookList)
{
   $arrsize = sizeof($bookList);
   print ("Array size is $arrsize\n");
   print ("<h2> Print array of objects </h2>");
   print("<hr>");

   print("<table border=\"1\">");
   // print column headings
   print("<th align=\"right\"> Book Title </th>");
   print("<th align=\"right\"> Weeks in Release </th>");
   print("<th align=\"right\"> Units Sold this Week</th>");
   print("<th align=\"right\"> Book Type </th>");
   print("<th align=\"right\"> Book Format</th>");
   print("<th align=\"right\"> Cost per Unit</th>");
   print("<th align=\"right\"> Weekly Gross </th>");
   print("</tr>");

   foreach ($bookList as $oneRec)
   {
      print("<tr>");
      $title = $oneRec->getTitle();
      $weeks = $oneRec->getWeeks();
      $sold = $oneRec->getWeekSales();
      $btype = $oneRec->getBkType();
      $bform = $oneRec->getBkFormat();

      $cost = $oneRec->getCost();
      $wkGross = $oneRec->getWkGross();

      print("<td align=\"right\"> $title</td>");
      print("<td align=\"right\"> $weeks</td>");
      print("<td align=\"right\"> $sold</td>");

      if ( strncmp($btype,"t", 1 ) == 0)
         print("<td align=\"right\"> Textbook</td>");
      else if( strncmp($btype,"f", 1) == 0)
         print("<td align=\"right\"> Fiction </td>");
      else
         print("<td align=\"right\"> Nonfiction</td>");

      if ( $bform[0] == "e")
         print("<td align=\"right\"> Electronic</td>");
      if( $bform[0] == "h")
         print("<td align=\"right\"> Hardcover </td>");
      if ($bform[0] == "p")
         print("<td align=\"right\"> Paperback </td>");

      print("<td align=\"right\"> $cost</td>");
      print("<td align=\"right\"> $wkGross</td>");
      print("</tr>");
   }
   print("</table>");
}// end printBookList

function createBookList($bfile,&$bookList)
{
   $bookList = array();
   $bookRec = fgets($bfile);

   while (!feof($bfile))
   {
      $tempRec = new Book();
      $bkType = " ";
      $bkFormat = " ";
      list($btitle, $weeks, $numSold, $bkType, $bkFormat, $cost) = explode(",", $bookRec);
      $bkType = strtolower(trim($bkType));
      $bkFormat = strtolower(trim($bkFormat));

      $tempRec->setTitle($btitle);
      $tempRec->setWeeks($weeks);
      $tempRec->setWeekSales($numSold);
      $tempRec->setBkType($bkType);
      $tempRec->setBkFormat($bkFormat);
      $tempRec->setCost($cost);
      $tempRec->computeWkGross();

      $bookList[] = $tempRec;
      $bookRec = fgets($bfile);
   }// end while not end of file
   fclose($bfile);
}//end function createBookList

if ( !class_exists("Book"))
   exit("<h1>The Book class is not available! </h1>");

$fileName = $_POST['fileName'];
print ("File name is $fileName\n");

$bfile = fopen("$fileName", "r") or exit("<h1>$filename not found!</h1>");

if( feof($bfile))
   exit("<h1>$fileName is empty</h1>");
else
   print ("<h2>Initiating file read</h2>");

createBookList($bfile, $bookList);
printBookList($bookList);


?>
</body>
