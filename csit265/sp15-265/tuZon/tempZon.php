<html>
<head>
</head>
<body>
<?php include "classBook.php"; ?>
<h1> TU-Zon Weekly Sales Report </h1>

<?php


if ( !class_exists("Book"))
   exit("<h1>The Book class is not available! </h1>");

$fileName = $_POST['fileName'];
print ("File name is $fileName\n");
$bfile = fopen("$fileName", "r") or exit("<h1>$filename not found!</h1>");

if( feof($bfile))
   exit("<h1>$fileName is empty</h1>");
else
   print ("<h2>Initiating file read</h2>");

$bookList = array();
$bookRec = fgets($bfile);

while (!feof($bfile))
{
   $tempRec = new Book();
   $bkType = " ";
   $bkFormat = " ";

   list($btitle, $weeks, $numSold, $bkType, $bkFormat, $cost) = explode(",", $bookRec);
   $bkType = strtolower(trim($bkType));
   $bkFormat = strtolower(trim($bkFormat));

   print ("<p> Title -$btitle  $weeks  $cost </p>");
   print ("<p> Sold - $numSold  Type - $bkType Format - $bkFormat</p>");

   $tempRec->setTitle($btitle);
   $tempRec->setWeeks($weeks);
   $tempRec->setWeekSales($numSold);
   $tempRec->setBkType($bkType);
   $tempRec->setBkFormat($bkFormat);
   $tempRec->setCost($cost);
   $tempRec->computeWkGross();

   $bookList[] = $tempRec;
   $bookRec = fgets($bfile);
}
fclose($bfile);

$arrsize = sizeof($bookList);
print ("Array size is $arrsize\n");
print ("<h2> Print array of objects </h2>");
print("<hr>");

print("<table border=\"1\">");
// print column headings
/*print("<th align=\"right\"> Book Title </th>");
print("<th align=\"right\"> Weeks in Release </th>");
print("<th align=\"right\"> Units Sold this Week</th>");
print("<th align=\"right\"> Book Type </th>");
print("<th align=\"right\"> Book Format</th>");
print("<th align=\"right\"> Cost per Unit</th>");
print("<th align=\"right\"> Weekly Gross </th>");
print("</tr>");
*/
foreach ($bookList as $oneRec)
{
   print("<tr>".$oneRec->getTitle().$oneRec->getWeeks().$oneRec->getWeekSales().$oneRec->getBkType().$oneRec->getBkFormat().$oneRec->getCost().$oneRec->getWkGross();
   print("</tr>");
}
print("</table>");

?>
</body>
