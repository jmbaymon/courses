<?php
class Book
{
	private $BkTitle;
	private $Weeks;
	private $WeekSales;
	private $BkType;
	private $BkFormat;
	private $Cost;
	private $WkGross;

	function _construct(){
		$this->BkTitle = "";
		$this->Weeks = 0;
		$this->WeekSales = 0;
		$this->BkType = "";
		$this->BkFormat = "";
		$this->Cost = 0;
		$this->WkGross = 0;
	}

	public function computeWkGross(){
		if ( $this->Weeks > 20)
			$this->Cost = $this->Cost *.6;
		$this->WkGross = $this->Cost * $this->WeekSales;
	}

	// setters
	public function setTitle($newValue){
		$this->BkTitle = $newValue;
	}
	public function setWeeks($newValue){
		$this->Weeks = $newValue;
	}
	public function setWeekSales($newValue){
		$this->WeekSales = $newValue;
	}
	public function setBkType($newValue){
		$this->BkType = $newValue;
	}
	public function setBkFormat($newValue){
		$this->BkFormat = $newValue;
	}
	public function setCost($newValue){
		$this->Cost = $newValue;
	}

	// getters
	public function getTitle(){
		return $this->BkTitle;
	}
	public function getWeeks(){
		return $this->Weeks;
	}
	public function getWeekSales(){
		return $this->WeekSales;
	}
	public function getBkType(){
		return $this->BkType;
	}
	public function getBkFormat(){
		return $this->BkFormat;
	}
	public function getCost(){
		return $this->Cost;
	}
	public function getWkGross() {
		return $this->WkGross;
	}

}// end Book

?>