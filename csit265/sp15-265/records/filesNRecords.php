<html>
<head>
   <title> CS Department Report </title>
   <link rel="stylesheet" type="text/css" href="stylesheet1.css"/>
</head>
<body>
<?php
$fileName = $_POST[inputFile];

$fileVariable = fopen("$fileName", "r") or exit ("<h1>$fileName was not opened successfully</h1>");

// read input information from file as a record
$studentRec = fgets($fileVariable);//read entire line a string type
//fgets(reads strings ) fgetc (read a character)
print("<h1> CS Department GPA Report </h1>");

print("<table border=\"2\">");
print("<tr><td> Student ID</td><td>Math GPA</td><td>CS GPA</td></tr>");
// loop while not end of input file
while ( !feof($fileVariable))
{
   // split the record into individual variables
   list($studentID, $mathGPA, $csGPA) = explode("-", $studentRec);
   print("<tr><td> $studentID</td><td>$mathGPA</td><td>$csGPA</td></tr>");

   // update input record
   $studentRec = fgets($fileVariable);
}// end while not end of file

print("</table>");
// close input file
fclose($fileVariable);
?>
</body>
</html>
