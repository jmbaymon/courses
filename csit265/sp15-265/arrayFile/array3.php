<?php
// Name:			Your Name Here
// Course Name CSCI 150 Introduction to Computer Science
// Section: 	Section Number
// Lab Day:		Tuesday or Thursday

// Assignment: Lab ???
// Due Date:   Date the assignment is due
// Problem Description:
//					Provide a brief overview of the program assignment
//
//
$colors = array("blue","red", "green", "yellow");

print ("using while with current and next\n");
$color = current($colors);
print (" $color \n");

while ($color = next($colors))
	print (" $color \n");

print ("using foreach\n");
foreach ($colors as $color)
	print (" $color \n");


print ("using foreach\n");
foreach ($colors as $idx => $color)
	print (" $idx  $color \n");

?>