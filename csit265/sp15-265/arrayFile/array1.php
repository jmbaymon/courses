<?php
// Name:			Your Name Here
// Course Name CSCI 150 Introduction to Computer Science
// Section: 	Section Number
// Lab Day:		Tuesday or Thursday

// Assignment: Lab ???
// Due Date:   Date the assignment is due
// Problem Description:
//					Provide a brief overview of the program assignment
//
//

// create an array using assignment statements
$cs402Student[0] = "Jamar";
$cs402Student[1] = "Ashley";
$cs402Student[2] = "Alston";
$cs402Student[3] = "QuayShawn";
$cs402Student[4] = "Latandra";

print("Enter any number between  0 and 4: ");
$who = trim(fgets(STDIN));
print ("Student selected =  $cs402Student[$who]\n");

$lastNames = array("Washington", "Lewis", "Walker", "Ivey", "Shepherd");
print("Student's last name is $lastNames[$who]\n");
?>