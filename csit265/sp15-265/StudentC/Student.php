<?php
   class Student
   {
      private $name;
      private $age;
      private $gpa;

      public function __construct($name, $age, $gpa)
      {
         $this -> name = $name;
         $this -> age = $age;
         $this -> gpa = $gpa;
      } // constructor

      public function getName()// a method that are used get member variable
      {
         return $this -> name;
      } // function getName


      public function getAge()
      {
         return $this -> age;
      } // function getAge


      public function getGPA()
      {
         return $this -> gpa;
      } // function getGPA
   } // class Student
?>
