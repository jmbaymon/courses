<?php
   include("Student.php");

   // Test driver
   //Student $who;

   $who = new Student('James Bond', 45, 3.25);
   $students[] = $who;
   $who = new Student('John Doe', 22, 3.5);
   $students[] = $who;
   $who = new Student('Jane Doe', 20, 3.75);
   $students[] = $who;

   for ($index = 0; $index < sizeof($students); $index++)
      echo $students[$index]->getName(), ' ', $students[$index] -> getAge(), ' ', $students[$index]->getGPA(), "\n";
?>
