<html>
<head>
   <title> CFT Inc. Weekly Wage Report </title>
   <link rel="stylesheet" type="text/css" href="mypage.css"/>
</head>
<body>
<?php

   // receive input file name from HTML
   $wageFileName = $_POST['wageFile'];

   print("<h3>File selected: $wageFileName</h3>");

   // open input file for processing
   $timesheetFile =fopen("$wageFileName","r") or exit("<h1>Unable to open file</h1>");

   // initialize $empCount
   $empCount = 0;

   // initialize $totalSalary
   $totalSalary = 0;

   //initialize lcv - read an employee record from input file - $timesheetFile
   $empRecord = fgets($timesheetFile);

   if ( feof($timesheetFile))
      print("<h2> Input File was Empty </h2>");
   else
   {
      print("<h1>WEEKLY WAGE REPORT</h1>");

      // display table header
      print("<table border=\"5\">");
      print("<tr><td>NAME</td><td>HOURS</td><td>HRLY WAGE</td><td>SALARY</td></tr>");

      // test lcv  check for the end of the file
      while (!feof($timesheetFile))
      {
         // split up the $empRecord
         //list($firstName,$lastName,$hours,$payRate) = explode(":",$empRecord);

         list($firstName,$lastName,$hours,$payRate) = split(":",$empRecord);

         // increment $empCount
         $empCount++;

         // compute $salary
         $salary = $hours * $payRate;

         // compute $totalSalary
         $totalSalary = $totalSalary + $salary;

         // display employee data
         print("<tr><td>$firstName $lastName</td>");
         print("<td>$hours</td>");
         print("<td>$payRate</td><td>$salary</td></tr>");

         // get next $empRecord update lcv
         $empRecord = fgets($timesheetFile);
      }// end while not end of $timesheetFile

      // disconnect
      fclose($timesheetFile);

      // end table
      print("</table>");

      $averageSalary = $totalSalary / $empCount;

      // display summary results
      print("<h1>WAGE REPORT SUMMARY</h1>");
      print("<p>Number of employees: $empCount.</p>");
      print("<p>Average Salary: $$averageSalary.</p>");
   }// end else - file not Empty
?>
</body>
</html>
