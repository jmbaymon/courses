<?php
  echo "Contents of POST array<br>";
  print_r($_POST);
  echo "<br>";
  echo "***<br>";

  echo "All the Contents of SERVER array<br>";
  foreach ($_SERVER as $key_name => $key_value) {
     print $key_name." = ".$key_value."<br>";
   }
  echo "***<br>";

  echo "Contents of ENV array<br>";
  $path = getenv('path');
  echo "<p> The path is : $path </p>";

  $osys = getenv('os');
  echo "<p> The operating system is : $osys </p>";

  $addr = getenv('remote_addr');
  echo "<p> The IP address is : $addr </p>";


?>
