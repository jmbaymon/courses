<head> <title> PHP Script Getting Environment </title></head>

<body>
<?php

  phpinfo(INFO_ENVIRONMENT);

$path = getenv('path');
echo "<p> The path is : $path </p>";

$addr = getenv('remote_addr');
echo "<p> The IP address is : $addr </p>";

$scrName = getenv('script_name');
echo "<p> The script name is : $scrName </p>";

?>
</body>
</html>
