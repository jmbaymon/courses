#include<iostream>

#include<iomanip>

using namespace std;



void makeMilesKmTable(double start, double end, double increment);



int main( )

{

 double start, end, increment;

 cout<<"Please enter start, end and increment, in miles: ";

 cin>>start>>end>>increment;

 makeMilesKmTable(start, end, increment);



 return 0;

}



void makeMilesKmTable(double start, double end, double increment)

{

     cout<<fixed<<setprecision(2);

     cout<<setw(10)<<"miles"<<setw(12)<<"kilometers"<<endl;

     for(double i=start; i<=end; i=i+increment)

       cout<<setw(10)<<i<<setw(12)<<i*1.6<<endl;



     return;

}
