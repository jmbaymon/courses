
<?php
// Name: Your Name Here
// Course Name CSCI 150 Introduction to Computer Science
// Section: Section 01
// Lab Day: Tuesday
// Assignment: Lab ???
// Due Date:   Date the assignment is due
// Problem Description:
//  Provide a brief overview of the program assignment
// define constants
$REGULAR_FEE = 125.75;
$SCHOLAR_FEE = 80.55;
$MIN_GPA = 3.75;
// Receive/Input Statements
$idNumber = $_POST['idNumber'];
$creditHours = $_POST['creditHours'];
$gpa = $_POST['gpa'];
// determine $tuition and $message
if ($gpa >= $MIN_GPA)
{
 $tuition = $creditHours * $SCHOLAR_FEE;
 $message = "Scholarship Awarded";
}// end if gpa at least 3.75
else
{
 $tuition = $creditHours * $REGULAR_FEE;
 $message = "No Scholarship Awarded";
}// end if gpa is not at least 3.75
// display $tuition, $message
print("<h2> Tuition Calculator Results Page</h2>");
print("<hr>");
print("<h4>Student #: $idNumber has $creditHours credit hours and a $gpa GPA </h4>");
print("<h4> $message and the tuition is $$tuition</h4>");
print("<hr>");
print ("<a href=\"stateTuition.html\">Return to Form</a>");
?>
