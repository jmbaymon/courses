<?php
// Name:			Your Name Here
// Course Name CSCI 150 Introduction to Computer Science
// Section: 	Section Number
// Lab Day:		Tuesday or Thursday

// Assignment: Lab ???
// Due Date:   Date the assignment is due
// Problem Description:
//					Provide a brief overview of the program assignment
//
//

// Assignment statements
$hourlyWage = 15.75;
$hoursWorked = 19;

// computations
$weeklyWage = $hourlyWage x $hoursWorked;

// display results
print ("\nYour hourly wage is $$hourlyWage and you worked $hoursWorked hours.\n");
print (" Your wages are $weeklyWage.\n");

?>