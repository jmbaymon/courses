<?php
// Name:       Your Name Here
// Course Name CSCI 150 Introduction to Computer Science
// Section:    Section 01
// Lab Day:    Tuesday

// Assignment: Lab ???
// Due Date:   Date the assignment is due
// Problem Description:
//             Provide a brief overview of the program assignment
//
//

// Input Statements
print("Enter your character name:");
$charName = trim(fgets(STDIN));

print("Enter your character type:");
$charType = trim(fgets(STDIN));

print("Enter the number of experience tokens:");
$expTokens = trim(fgets(STDIN));

print("Enter the number of health tokens:");
$healthTokens = trim(fgets(STDIN));

print("Enter the number of supply tokens:");
$supplyTokens = trim(fgets(STDIN));

// computations - assignment statements, selection statements, repitition statements...
$cost = $expTokens/2 + $healthTokens/10 + $supplyTokens/25;

// display results
print("Character name chosen $charName\n");
print("Character type chosen $charType\n");
print("$expTokens experience tokens were purchased\n");
print("$healthTokens health tokens were purchased\n");
print("$supplyTokens supply tokens were purchased\n");
print("Final cost $$cost\n");
?>
