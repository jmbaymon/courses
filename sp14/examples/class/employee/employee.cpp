//  file name -- employee.cpp
//  This file contains the implementations of the member function of
//  Employee class.

//  ========================= header files =======================
#include<iostream>                      // for console I/O
#include<cstring>                       // for c string library
#include "employee.h"                   // for Employee class

//  ======================= namespaces used ======================
using namespace std;                    // for std functions


Employee::Employee()
// purpose: intialize the data members with default values
// precondition: none
// postcondition: strings are set to null, numeric values to 0
{
   strcpy(name, "");
   strcpy(ssn, "");
   gender = ' ';
   age = 0;
   salary = 0;
} // end of default constructor


Employee::Employee(NameString new_name,
                   SSNString new_ssn,
                   char new_gender,
                   int new_age,
                   float new_salary)
// purpose: intialize the data members with values passed to constructor
// precondition: values passed are valid
// postcondition: data members are assigned to their values of the
//               corresponding parameters
{
   strcpy(name, new_name);
   strcpy(ssn, new_ssn);
   gender = new_gender;
   age = new_age;
   salary = new_salary;
} // end of set value constructor


char* Employee::getName()
// purpose: return name of current employee
// precondition: none
// postcondition: name is of the employee is returned
{
   return name;
} // end of getName


char* Employee::getSSN()
// purpose: return SSN of current employee
// precondition: none
// postcondition: SSN is of the employee is returned
{
   return ssn;
} // end of getSSN;


char Employee::getGender()
// purpose: return gender of current employee
// precondition: none
// postcondition: gender is of the employee is returned
{
   return gender;
} // end of getGender


int Employee::getAge()
// purpose: return age of current employee
// precondition: none
// postcondition: age is of the employee is returned
{
   return age;
} // end of getAge


float Employee::getSalary()
// purpose: return salary of current employee
// precondition: none
// postcondition: salary is of the employee is returned
{
   return salary;
} // end of getSalary


void Employee::setName(NameString new_name)
// purpose: update name of current employee
// precondition: new_name contains a valid employee name
// postcondition: name is assigned the new name
{
   strcpy(name, new_name);
} // end of setName


void Employee::setSSN(SSNString new_ssn)
// purpose: update SSN of current employee
// precondition: new_SSN contains a valid SSN
// postcondition: SSN is assigned the new SSN
{
   strcpy(ssn, new_ssn);
} // end of setSSN


void Employee::setGender(char new_gender)
// purpose: update gender of current employee
// precondition: new_gender is positive
// postcondition: gender is assigned the new gender
{
   gender = new_gender;
} // end of setGender


void Employee::setAge(int new_age)
// purpose: update age of current employee
// precondition: new_age is positive
// postcondition: age is assigned the new age
{
   age = new_age;
} // end of setAge


void Employee::setSalary(float new_salary)
// purpose: update salary of current employee
// precondition: new_salary is positive
// postcondition: salary is assigned the new salary
{
   salary = new_salary;
} // end of setSalry
