//  file name -- record.cpp
//  This program reads data from a text file in which each employee
//  record is stored on three separate lines:

//        Name                       line 1
//        SSN                        line 2
//        gender, age, salary        line 3 (separated by one space)

//  Data are read from the file and displayed on the screen. When the
//  screen becomes full, the program pauses the screen. After the user
//  presses the Enter key, the program continues to display records on
//  the screen. For each screen, the output includes proper column
//  headings.

//  ========================= header files ===========================
#include <iostream>                  // for console I/O
#include <fstream>                   // for file I/O
#include <iomanip>                   // for output control
#include <stdlib.h>                  // for converting to numeric values
#include "utility.h"                 // for utility functions
#include "employee.h"                // for Employee class

// ======================== namespaces used =========================
using namespace std;


//  ========================= symbolic constants =====================
const int MAX_EMPLOYEE = 50;
const int WIDTH1 = 5;
const int WIDTH2 = 5;
const int WIDTH3 = 5;
const int WIDTH4 = 17;
const int WIDTH5 = 9;
const int WIDTH6 = 4;
const int WIDTH7 = 7;
const int WIDTH8 = 3;
const int WIDTH9 = 10;
const int WIDTH10 = 13;
const int MAX_LINE = 60;
const int DECIMAL = 2;
//const int MAX_NAME = 21;
//const int MAX_SSN = 12;
const int SCREEN_SIZE = 15;
const int MAX_AGE = 3;        // to read age as a string from file
const int MAX_SALARY = 11;    // to read salary as a string from file


//  ========================= datatype declarations ==================
//typedef char NameString[MAX_NAME];
typedef Employee EmployeeArray[MAX_EMPLOYEE];


//  ========================= function prototypes ====================
void openFile(ifstream &);
void readData(ifstream &, EmployeeArray, int &);
void displayRecords(EmployeeArray, int);
void displayARecord(Employee);
void displayHeadings();


int main()
{
   EmployeeArray company;
   int size;
   ifstream infile;

   //clearScreen();
   openFile(infile);
   readData(infile, company, size);
   displayRecords(company, size);

   return 0;
}  // end of main


void openFile(ifstream &infile)
// purpose: This function attempts to open a file and associate it with infile.
//          If the file name entered does not exist, then the function will ask
//          the user to reenter the file name. This process continues until the
//          file is opened successfully.
// precondition: none
// postcondition: a text file has been opened successfully and it is associated
//               with infile.
{
   NameString infile_name;

   do
   {
      cout << "Enter in the input data file name: ";
      cin >> infile_name;
      infile.open(infile_name);
      if (!infile)
      {
         cout << infile_name
              << " was not opened successfully."
              << endl;
      }  // if
   } while (!infile);
   cin.ignore(SIZE, EOLN);
}  // end of openFile


void readData(ifstream &infile,
              EmployeeArray company,
              int &n)
// purpose: This function reads data from the input data file record by record
//          and records read from the file are stored in an array of records.
//          The number of records read from the file is updated as well.
// precondition: infile has been associated with a text file which has been
//               opened successfully
// postcondition: formatted employee records are read in from the file (infile)
//               and stored in the array company, and the number of records
//               read in from the file is saved in n.
{
   Employee who;
   int index;
   NameString name;
   SSNString ssn;
   char gender;
   int age;
   float salary;

   n = 0;
   index = 0;
   //infile.get(name, csci220_examples::MAX_NAME);
   infile.getline(name, MAX_NAME);
   while (infile)
   {
      //infile.ignore(SIZE, EOLN);
      n++;
      infile.getline(ssn, MAX_SSN);
      infile >> gender >> age >> salary;
      infile.ignore(SIZE, EOLN);

cout << n << endl;

      who.setName(name);
      who.setSSN(ssn);
      who.setGender(gender);
      who.setAge(age);
      who.setSalary(salary);

      company[index] = who;
      index++;
      infile.getline(name, MAX_NAME);
   }  // while
}  // end of readData


void displayHeadings()
// purpose: This function clears the screen and displays proper headings with
//          proper spacings between column headings. A line of dashes is
//          shown below the column headings.
// precondition: none
// postcondition: column headings for a table are displayed.
{
   clearScreen();
   cout << "Name"
        << setw(WIDTH4)        // WIDTH4 = 17
        << ""
        << "SSN"
        << setw(WIDTH5)        // WIDTH5 = 9
        << ""
        << "Gender"
        << setw(WIDTH6)        // WIDTH6 = 4
        << ""
        << "Age"
        << setw(WIDTH7)        // WIDTH7 = 7
        << ""
        << "Salary"
        << endl;
   printSymbols('=', MAX_LINE, true);
}  // end of displayHeadings


void displayRecords(EmployeeArray company,
                    int size)
// purpose: This function displays employee records on the screen. If the
//          screen becomes full, the output is paused until the Enter key
//          is pressed. Each screen contains proper column headings and
//          each field of a record is properly aligned within its own output
//          area.
// precondition: company contains valid employee records and size is positive.
// postcondition: records stored in company are displayed in a table form which
//               includes column headings.
{
   int k;
   int index;
   int line_count;

   index = 0;
   line_count = 0;
   displayHeadings();
   for (k = 1; k <= size; k++)
   {
      line_count++;
      displayARecord(company[index]);
      index++;
      if (line_count == SCREEN_SIZE)
      {
         printSymbols('=', MAX_LINE, true);
         cout << endl;
         pauseScreen();
         line_count = 0;
         displayHeadings();
      }  // if
   }  // for k

   // if final screen is not full, pause the output
   if (line_count > 0)
   {
      printSymbols('=', MAX_LINE, true);
      cout << endl;
      pauseScreen();
   }  // if
}  // end of displayRecords


void displayARecord(Employee alo)
// purpose: This function displays an employee record in accordance with
//          the column headings used for each screen. Strings are left
//          justified and numeric values are right justified.
// precondition: alo contains a valid employee record.
// postcondition: the information about the employee is displayed as
//               one row of the table needed.
{
   int length;

   length = MAX_NAME - 1 - strlen(alo.getName());
   cout.setf(ios::floatfield, ios::fixed);
   cout.setf(ios::showpoint);
   cout << alo.getName();
   printSymbols(' ', length, false);
   cout << " "
        << alo.getSSN()
        << setw(WIDTH8)      // WIDTH8 = 3
        << ""
        << alo.getGender()
        << setw(WIDTH9)      // WIDTH9 = 10
        << alo.getAge()
        << setw(WIDTH10)     // WIDTH10 = 13
        << setprecision(DECIMAL)
        << alo.getSalary()
        << endl;
}  // end of displayARecord
