//  file name -- employee.h
//  This file contains the specifications of the Employee class which has
//  the following attributes:

//        Name --(no more than 20 characters)
//        SSN -- (exactly 11 characters)
//        gender -- a single character code
//        age -- int type
//        salary -- float type

// ========================= macro definition =======================
#ifndef CLASS_EMPLOYEE_
#define CLASS_EMPLOYEE_

//  ========================= named constants =======================
   const int MAX_NAME = 21;
   const int MAX_SSN = 12;
   typedef char NameString[MAX_NAME];
   typedef char SSNString[MAX_SSN];

// ========================= class definition =======================
   class Employee
   {
      private: 
         NameString name;
         SSNString ssn;
         char gender;
         int age;
         float salary;

      public:
         Employee();
         Employee(NameString, SSNString, char, int, float);

         char* getName();
         char* getSSN();
         char getGender();
         int getAge();
         float getSalary();
         void setName(NameString);
         void setSSN(SSNString);
         void setGender(char);
         void setAge(int);
         void setSalary(float);
   };  // end of employee

#endif
