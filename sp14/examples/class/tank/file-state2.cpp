// file name -- file-state2.cpp
// This file shows how the state of a file changes.

#include <iostream>
#include <fstream>
using namespace std;

int main()
{
   int value;
   ifstream infile;
   char file_name[20];

   cout << "Enter file name for input: ";
   cin >> file_name;

   infile.open(file_name);
   infile >> value;

   cout << "The state of infile is "
        << infile
        << endl;

   while (infile)
   {
      cout << "The integer read in is "
           << value
           << endl;

      infile >> value;
      cout << "The state of infile is "
           << infile
           << endl;
   } // while

   return 0;
} // function main