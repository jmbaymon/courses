// file name -- file-state1.cpp
// This file shows how the state of a file changes.

#include <iostream>
using namespace std;

int main()
{
   int value;
   float number;
   char c;

   cout << "Enter an integer: ";
   cin >> value;
   cout << "The integer read in is "
        << value
        << endl;

   cout << "Enter a float value: ";
   cin >> number;
   cout << "The float value read in is "
        << number
        << endl;

   cout << "Enter a character: ";
   cin >> c;
   cout << "The character read in is "
        << c
        << endl;

   return 0;
} // function main