# build test-tank04 based on three object files: tank.o, utility.o
# and test-tank04.o. The load module test-tank04 must be rebuilt
# if any of those object files has been changed.

test-tank04: tank.o utility.o test-tank04.o
	c++ tank.o utility.o test-tank04.o -o test-tank04


# build test-tank04.o based on test-tank04.cpp; it must be rebuilt
# if tank.o, utility.o or test-tank04.cpp has been changed.

test-tank04.o: test-tank04.cpp tank.o utility.o
	c++ -c test-tank04.cpp


# build tank.o based on tank.cpp; it must be rebuilt if tank.cpp,
# tank.h, or utility.o has been changed.

tank.o: tank.cpp tank.h utility.o
	c++ -c tank.cpp


# build utility.o based on utility.cpp; it must be rebuilt if utility.h
# or utility.cpp has been changed.

utility.o: utility.cpp utility.h
	c++ -c utility.cpp
