//  file name -- test-tank03.cpp
//  This program shows how to read data from a data file and how
//  to set values to a tank object and display its content.

//  ======================= header files ========================
#include <iostream>                   // for console I/O
#include <fstream>                    // for file I/O
#include <iomanip>                    // for output format control
#include "utility.h"                  // for user-defined functions
#include "tank.h"                     // for Tank class
using namespace std;


//  ==================== symbolic constants =====================
const int MAX_NAME = 51;
const int DECIMAL = 2;


//  ==================== function prototypes ====================
void openFile(ifstream &);
void readData(ifstream &);
void setPrecision(int);
void displayATank(Tank);


int main()
{
   ifstream infile;

   clearScreen();
   openFile(infile);
   readData(infile);

   return 0;
}  // end of main


void openFile(ifstream &infile)
// purpose: open a data file for read access.
// preconditions: none
// postconditions: a data file has been associated with infile.
{
   char infile_name[MAX_NAME];

   do
   {
      cout << "Enter data file name: ";
      cin.getline(infile_name, MAX_NAME);
      infile.open(infile_name);
      if (!infile)
         cout << infile_name
              << " was not opened successfully."
              << endl;
   } while (!infile);
}  // end of openFile


void readData(ifstream &infile)
// purpose: read data about tanks from an input data file and store
//          the values to a tank object, then display the content of
//          the tank object.
// preconditions: infile has been associated with an input data file.
// postconditions: data stored in an input data file are read and set
//          to a tank object; the content of the tank object is
//          displayed.
{
   Tank one;
   int count;
   double capacity, initial_amount;

   pauseScreen();
   count = 0;
   infile >> capacity;
   setPrecision(DECIMAL);
   while (infile)
   {
      count ++;
      infile >> initial_amount;
      one.setCapacity(capacity);
      if (one.getAvailableSpace() > initial_amount)
         one.addFluid(initial_amount);
     clearScreen();
      cout << "Tank #"
           << count
           << endl;
      displayATank(one);
      pauseScreen();
      infile >> capacity;
   }  // while
}  // end of readData


void displayATank(Tank whatever)
// purpose: display the content of a tank
// preconditions: whatever is a well-defined tank object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "Capacity of the tank is "
        << whatever.getCapacity()
        << endl
        << "Current level of the tank is "
        << whatever.getCurrentLevel()
        << endl
        << "Available space of the tank is "
        << whatever.getAvailableSpace()
        << endl;
}  // end of displayATank


void setPrecision(int precision)
// purpose: set precision for the floating value to be printed
// preconditions: precision is a positive number
// postconditions: precision is set for floating values to be printed.
{
   cout.setf(ios::fixed, ios::floatfield);
   cout.setf(ios::showpoint);
   cout << setprecision(precision);
}  // end of setPrecision
