//  file name -- tank.h
//  This header file contains the specifications needed to represent
//  a tank.

#ifndef TANK_CLASS_
#define TANK_CLASS_

class Tank
{
   private:
      double capacity;
      double current;

   public:
      Tank();
      Tank(double);
      Tank(const Tank &);

      void setCapacity(double);
      void addFluid(double);
      void removeFluid(double);
      bool isEmpty();
      bool isFull();
      double getCapacity();
      double getCurrentLevel();
      double getAvailableSpace();
};  // class Tank

#endif
