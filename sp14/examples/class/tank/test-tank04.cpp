//  file name -- test-tank04.cpp
//  This program shows how to read data from a data file and how
//  to set values to tank objects stored in an array of tank
//  objects. Their contents are displayed later on. To use an
//  array of objects, one needs to implement a default constructor.

//  ======================= header files ========================
#include <iostream>                   // for console I/O
#include <fstream>                    // for file I/O
#include <iomanip>                    // for output format control
#include "utility.h"                  // for user-defined functions
#include "tank.h"                     // for Tank class
using namespace std;


//  ==================== symbolic constants =====================
const int MAX_NAME = 51;
const int DECIMAL = 2;
const int MAX_SIZE = 100;


//  ================== data type declarations ===================
typedef Tank TankArray[MAX_SIZE];


//  ==================== function prototypes ====================
void openFile(ifstream &);
void readData(ifstream &, TankArray, int &);
void displayTanks(TankArray, int);
void setPrecision(int);
void displayATank(Tank);


int main()
{
   ifstream infile;
   TankArray tanks;
   int size;
   Tank one[5];

   pauseScreen();
   clearScreen();
   openFile(infile);
   readData(infile, tanks, size);
   displayTanks(tanks, size);

   return 0;
}  // end of main


void openFile(ifstream &infile)
// purpose: open a data file for read access.
// preconditions: none
// postconditions: a data file has been associated with infile.
{
   char infile_name[MAX_NAME];

   do
   {
      cout << "Enter data file name: ";
      cin.getline(infile_name, MAX_NAME);
      infile.open(infile_name);
      if (!infile)
           cout << infile_name
                << " was not opened successfully."
                << endl;
   } while (!infile);
}  // end of openFile


void readData(ifstream &infile,
                   TankArray tanks,
                   int &n)
// purpose: read data about tanks from an input data file and store
//          the values to a tank object of an array of tank objects.
// preconditions: infile has been associated with an input data file.
// postconditions: data stored in an input data file are read and set
//          to tank objects stored in tanks; the size of array is
//          updated properly.
{
   Tank one;
   double capacity, initial_amount;
   int index;

   n = 0;
   index = 0;
   infile >> capacity;
   while (infile)
   {
      infile >> initial_amount;
      one.setCapacity(capacity);
      if (one.getAvailableSpace() > initial_amount)
              one.addFluid(initial_amount);
      tanks[index] = one;
      index ++;
      n ++;
      infile >> capacity;
   }  // while
}  // end of readData


void displayATank(Tank whatever)
// purpose: display the content of a tank
// preconditions: whatever is a well-defined tank object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "Capacity of the tank is "
        << whatever.getCapacity()
        << endl
        << "Current level of the tank is "
        << whatever.getCurrentLevel()
        << endl
        << "Available space of the tank is "
        << whatever.getAvailableSpace()
        << endl;
}  // end of displayATank


void setPrecision(int precision)
// purpose: set precision for the floating value to be printed
// preconditions: precision is a positive number
// postconditions: precision is set for floating values to be printed.
{
   cout.setf(ios::fixed, ios::floatfield);
   cout.setf(ios::showpoint);
   cout << setprecision(precision);
}  // end of setPrecision


void displayTanks(TankArray one,
                            int size)
// purpose: display contents of tank objects stored in an array.
// preconditions: size represents the number of tank objects stored
//          in one.
// postconditions: contents of tank objects are displayed.
{
   int index, k;

   index = 0;
   setPrecision(DECIMAL);
   for (k = 1; k <= size; k++)
   {
      clearScreen();
      cout << "********** Tank #"
           << k
           << " **********"
           << endl;
      displayATank(one[index]);
      index++;
      pauseScreen();
   }  // for k
}  // end of displayTanks
