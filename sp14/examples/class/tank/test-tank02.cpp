//  file name -- test-tank02.cpp
//  This program serves as a test driver to test the correctness
//  of the functions of the class Tank. This program uses the
//  second constructor to instantiate a tank object.

//  ======================= header files ========================
#include <iostream>                   // for console I/O
#include "utility.h"                  // for user-defined functions
#include "tank.h"                     // for Tank class
using namespace std;


//  ==================== function prototypes ====================
void displayATank(Tank);


int main()
{
   Tank one(3000);
   double amount;

   clearScreen();
   cout << "Information about tank one is "
        << endl;
   displayATank(one);
   pauseScreen();

   cout << "Enter an amount to remove from the tank: ";
   cin >> amount;
   if (amount > 0)
   {
      if (amount < one.getCurrentLevel())
         one.removeFluid(amount);
      else
         cout << "Not enought fluid to remove "
              << amount
              << " from the tank"
              << endl;
   }
   else
      cout << "Cannot remove a negative value to the tank"
           << endl;
   displayATank(one);

   return 0;
}  // end of main


void displayATank(Tank whatever)
// purpose: display the content of a tank
// preconditions: whatever is a well-defined tank object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "Capacity of the tank is "
        << whatever.getCapacity()
        << endl
        << "Current level of the tank is "
        << whatever.getCurrentLevel()
        << endl
        << "Available space of the tank is "
        << whatever.getAvailableSpace()
        << endl;
}  // end of displayATank
