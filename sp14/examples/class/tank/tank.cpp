//  file name -- tank.cpp
//  This file contains the implementation of the Tank class.

//  ======================= header files ========================
#include "tank.h"                  // for Tank class
#include <iostream>                // for console I/O
using namespace std;


//  =============== implementation of functions =================
Tank::Tank()
// purpose: set default values to the current object.
// preconditions: none
// postconditions: default values for capicity and current are
//          set for the current object.
{
   capacity = 1000;
   current = 0;
   cout << "default constructor is called"
        << endl;
}  // default constructor


Tank::Tank(double new_capacity)
// purpose: set new_capacity to the current object.
// preconditions: new_capacity is positive.
// postconditions: new_capacity is set for the current object.
{
   capacity = new_capacity;
   current = 0;
   cout << "set value constructor is called"
        << endl;
}  // set value constructor


Tank::Tank(const Tank &source)
// purpose: copy the attributes stored in source to the current
//          object.
// preconditions: source is a well-defined Tank object.
// postconditions: the attributed stored in source are copied to the
//          current object.
{
   capacity = source.capacity;
   current = source.current;
   cout << "copy constructor is called"
        << endl;
}  // copy constructor


void Tank::setCapacity(double new_capacity)
// purpose: change size of the current tank object.
// preconditions: new_capacity is positive.
// postconditions: new_capacity is set for the current object.
{
   capacity = new_capacity;
   current = 0;
}  // end of setCapacity


void Tank::addFluid(double amount)
// purpose: add fluid to the tank.
// preconditions: amount added must be no more than available space
//          left in the tank.
// postconditions: amount of fluid is added to the tank.
{
   current += amount;
}  // end of addFluid


void Tank::removeFluid(double amount)
// purpose: remove fluid from the current tank object.
// preconditions: amount to be removed cannot exceed current level.
// postconditions: amount of fluid is removed from the tank.
{
   current -= amount;
}  // end of removeFluid


double Tank::getCapacity()
// purpose: return the capacity of the current tank object.
// preconditions: current tank object is well defined.
// postconditions: capacity of current tank object is returned.
{
   return capacity;
}  // end of getCapacity


double Tank::getCurrentLevel()
// purpose: return the current level of the tank object.
// preconditions: current tank object is well defined.
// postconditions: current level of the tank object is returned.
{
   return current;
}  // end of getCurrentLevel


double Tank::getAvailableSpace()
// purpose: return the available space of the tank object.
// preconditions: current tank object is well defined.
// postconditions: available space of the tank object is returned.
{
   return capacity - current;
}  // end of getAvailableSpace


bool Tank::isEmpty()
// purpose: check to see if tank is empty.
// preconditions: current tank object is well defined.
// postconditions: return true is tank is empty; otherwise, return
//          false.
{
   return (current == 0);
}  // end of isEmpty


bool Tank::isFull()
// purpose: check to see if tank is full.
// preconditions: current tank object is well defined.
// postconditions: return true is tank is full; otherwise, return
//          false.
{
   return (current == capacity);
}  // end of isFull
