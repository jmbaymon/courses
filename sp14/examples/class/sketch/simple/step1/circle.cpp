// file name -- circle.cpp

// This file contains the definitions of the member functions of
// the Circle class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "circle.h"                     // for Circle class definition
#include <iostream>
using namespace std;


/**
   initialize the object using default values
   precondition: none
   postcondition: all member variables are assigned default values
*/
Circle::Circle()
{
   radius = 10;
   color = BLACK;
   fill = false;
} // default constructor


/**
   initialize the object using default values
   precondition: new_radius is all positive; new_color is one
                 of the predefined colors and new_fill is either true
                 or false
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
Circle::Circle(double new_radius,
               ColorType new_color,
               bool new_fill)
{
   radius = new_radius;
   color = new_color;
   fill = new_fill;
} // set value constructor
