// file name -- driver1.cpp

// This program is served as a test driver to check the correctness of the
// constructors of Circle class.


// ============================== header files ==============================
#include "circle.h"                     // for Circle class definition
#include <iostream>
using namespace std;


int main()
{
   // declare a Circle object named one; the default constructor is called
   // to initialize the member variables of one
   Circle one;

   // declare a Circle object named two; the set value constructor is called
   // to initialize the member variables of two; only three arguments are
   // passed to the constructor (the final two parameters have default values).
   //Circle two(30);


   // declare a Circle object named three; the set value constructor is called
   // to initialize the member variables of three; only four arguments are
   // passed to the constructor (the final parameter has a default value).
   //Circle three(20, ORANGE);

   // declare a Circle object named four; the set value constructor is called
   // to initialize the member variables of four; all five arguments are
   // passed to the constructor
   //Circle four(30, RED, true);
   return 0;
} // default constructor
