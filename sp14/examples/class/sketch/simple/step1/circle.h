// file name -- circle.h

// This file contains the definition of the Circle class. A circle object
// has a radius. A Circle object also has a color attribute. When a Circle
// object is drawn, it can be filled with a color or not be filled with
// any color.

#ifndef CLASS_CIRCLE_
#define CLASS_CIRCLE_

// ============================== header files ==============================
#include <sketch.h>                    // for Sketch class


class Circle
{
   private:
      double radius;
      ColorType color;
      bool fill;

   public:
      Circle();
      Circle(double, ColorType new_color = BLACK,
             bool new_fill = false);

      double getRadius();
      ColorType getColor();
      bool isFill();
      double getArea();
      double getCircumference();

      void changeRadius(double);
      void changeColor(ColorType);
      void changeFill(bool);

      void draw(double, double);
}; // class Circle

#endif
