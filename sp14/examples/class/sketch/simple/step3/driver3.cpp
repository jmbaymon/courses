// file name -- driver3.cpp

// This program is served as a test driver to check the correctness of the
// mutator functions of Circle class via accessor functions.


// ============================== header files ==============================
#include "circle.h"                     // for Circle class definition
#include <iostream>
using namespace std;

// ========================== function prototypes ===========================
void displayCircle(Circle);


int main()
{
   // declare a Circle object named one; the default constructor is called
   // to initialize the member variables of one
   cout << "Circle one created using default constructor"
        << endl;
   Circle one;
   displayCircle(one);

   // change radius to 120
   cout << "change radius to 120"
        << endl;
   one.changeRadius(120);
   displayCircle(one);

   // change color to YELLOW
   cout << "change color to YELLOW"
        << endl;
   one.changeColor(YELLOW);
   displayCircle(one);

   // change fill flag to true
   cout << "change fill flag to true"
        << endl;
   one.changeFill(true);
   displayCircle(one);

   return 0;
} // default constructor


/**
   display the attributes of a Circle object
   precondition: none
   postcondition: the attributes of a Circle object are displayed
*/
void displayCircle(Circle which)
{
   cout << "Radius is "
        << which.getRadius()
        << endl
        << "Color is "
        << which.getColor()
        << endl
        << "Filling circle is "
        << which.isFill()
        << endl;
} // end of displayCircle
