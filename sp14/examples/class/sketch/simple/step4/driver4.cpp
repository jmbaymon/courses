// file name -- driver4.cpp

// This program is served as a test driver to check the correctness of the
// draw function of Circle class. This is the final test of the Circle
// class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "circle.h"                     // for Circle class definition
#include <iostream>
using namespace std;


// ============================ named constants =============================
const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;

// ========================== function prototypes ===========================
void displayCircle(Circle);


int main()
{
   createWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Draw circles");
   // declare a Circle object named one; the default constructor is called
   // to initialize the member variables of one;
   cout << "Circle one created using default constructor"
        << endl;
   Circle one;
   displayCircle(one);
   one.draw(50, 50);

   // declare a Circle object named two; the set value constructor is called
   // to initialize the member variables of two; only three arguments are
   // passed to the constructor (the final two parameters have default values).
   Circle two(30);
   displayCircle(two);
   two.draw(100, 100);


   // declare a Circle object named three; the set value constructor is called
   // to initialize the member variables of three; only four arguments are
   // passed to the constructor (the final parameter has a default value).
   Circle three(20, ORANGE);
   displayCircle(three);
   three.draw(150, 200);

   // declare a Circle object named four; the set value constructor is called
   // to initialize the member variables of four; all five arguments are
   // passed to the constructor
   Circle four(30, PURPLE, true);
   displayCircle(four);
   four.draw(300, 290);

   startDrawing();

   return 0;
} // default constructor


/**
   display the attributes of a Circle object
   precondition: none
   postcondition: the attributes of a Circle object are displayed
*/
void displayCircle(Circle which)
{
   cout << "Radius is "
        << which.getRadius()
        << endl
        << "Color is "
        << which.getColor()
        << endl
        << "Filling circle is "
        << which.isFill()
        << endl;
} // end of displayCircle
