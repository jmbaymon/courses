// file name -- car.cpp

// This file contains the definitions of the member functions of
// the Car class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "car.h"                        // for Car class definition
#include <iostream>
using namespace std;


/**
   initialize the object using default values
   precondition: none
   postcondition: all member variables are assigned default values
*/
Car::Car()
{
   cout << "default constructor of Car class is called"
        << endl;
} // default constructor


/**
   initialize the object using default values
   precondition: none
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
Car::Car(Rectangle top_,
         Rectangle body_,
         Circle front_wheel_,
         Circle rear_wheel_)
{
   // change top of the car
   top.changeWidth(top_.getWidth());
   top.changeHeight(top_.getHeight());
   top.changeColor(top_.getColor());
   top.changeFill(top_.isFill());

   // change body of the car
   body.changeWidth(body_.getWidth());
   body.changeHeight(body_.getHeight());
   body.changeColor(body_.getColor());
   body.changeFill(body_.isFill());

   // change front wheel of the car
   front_wheel.changeRadius(front_wheel_.getRadius());
   front_wheel.changeColor(front_wheel_.getColor());
   front_wheel.changeFill(front_wheel_.isFill());

   // change rear of the car
   rear_wheel.changeRadius(rear_wheel_.getRadius());
   rear_wheel.changeColor(rear_wheel_.getColor());
   rear_wheel.changeFill(rear_wheel_.isFill());

   cout << "set value constructor of Car class is called"
        << endl;
} // set value constructor
