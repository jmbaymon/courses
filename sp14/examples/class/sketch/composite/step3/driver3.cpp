// file name -- driver3.cpp

// This program is served as a test driver to check the correctness of the
// constructors of Car class.


// ============================== header files ==============================
#include <sketch.h>                        // for Sketch class
#include "rectangle.h"                     // for Rectangle class definition
#include "circle.h"                        // for Circle class definition
#include "car.h"                           // for Car class definition
#include <iostream>
using namespace std;


// ============================ named constants =============================
const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;


// ========================== function prototypes ===========================
void displayRectangle(Rectangle);
void displayCircle(Circle);
void displayCar(Car);


int main()
{
   // createWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Draw circles");
   // declare a Car object named my_car; the default constructor is called
   // to initialize the member variables of my_car
   Car my_car;

   // declare the parts of a car
   Rectangle top(150, 50, BLACK, true);
   Rectangle body(200, 70, BLUE, true);
   Circle front_wheel(35, PURPLE, true);
   Circle rear_wheel(35, PURPLE, true);

   Car your_car(top, body, front_wheel, rear_wheel);
   //displayCar(your_car);

   //startDrawing();

   return 0;
} // default constructor
