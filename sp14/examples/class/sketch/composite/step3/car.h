// file name -- car.h

// This file contains the definition of the Car class. A car object consists
// of two wheels, a top and a body. Each wheel is represented by a cirle
// filled with a color. The top of car is represented by a rectangle filled
// with a color. So is the body of the car.

#ifndef CLASS_CAR_
#define CLASS_CAR_

// ============================== header files ==============================
#include <sketch.h>                    // for Sketch class
#include "circle.h"                    // for Circle class
#include "rectangle.h"                 // for Rectangle class


class Car
{
   private:
      Rectangle top;
      Rectangle body;
      Circle front_wheel;
      Circle rear_wheel;

   public:
      Car();
      Car(Rectangle, Rectangle, Circle, Circle);

      Rectangle getTop();
      Rectangle getBody();
      Circle getFrontWheel();
      Circle getRearWheel();

      void changeTop(Rectangle);
      void changeBody(Rectangle);
      void changeFrontWheel(Circle);
      void changeRearWheel(Circle);

      void draw(double, double);
}; // class Car

#endif
