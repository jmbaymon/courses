// file name -- driver1.cpp

// This program is served as a test driver to check the correctness of the
// constructors of Rectangle class.


// ============================== header files ==============================
#include "rectangle.h"                     // for Rectangle class definition
#include <iostream>
using namespace std;


// ========================== function prototypes ===========================
void displayRectangle(Rectangle);


int main()
{
   // declare a Rectangle object named one; the default constructor is called
   // to initialize the member variables of one
   Rectangle one;
   displayRectangle(one);

   // declare a Rectangle object named two; the set value constructor is called
   // to initialize the member variables of two; only four arguments are
   // passed to the constructor (the final two parameters have default values).
   Rectangle two(30, 50);
   displayRectangle(two);


   // declare a Rectangle object named three; the set value constructor is called
   // to initialize the member variables of three; only five arguments are
   // passed to the constructor (the final parameter has a default value).
   Rectangle three(100, 20, ORANGE);
   displayRectangle(three);

   // declare a Rectangle object named four; the set value constructor is called
   // to initialize the member variables of four; all six arguments are
   // passed to the constructor
   Rectangle four(150, 30, PURPLE, true);
   displayRectangle(four);

   return 0;
} // default constructor


/**
   display the attributes of a Rectangle object
   precondition: none
   postcondition: the attributes of a Rectangle object are displayed
*/
void displayRectangle(Rectangle which)
{
   cout << "Width is "
        << which.getWidth()
        << endl
        << "Height is "
        << which.getHeight()
        << endl
        << "Color is "
        << which.getColor()
        << endl
        << "Filling circle is "
        << which.isFill()
        << endl;
} // end of displayRectangle
