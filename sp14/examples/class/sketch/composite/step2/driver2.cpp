// file name -- driver2.cpp

// This program is served as a test driver to check the correctness of the
// constructors of Rectangle class.


// ============================== header files ==============================
#include <sketch.h>                        // for Sketch class
#include "rectangle.h"                     // for Rectangle class definition
#include <iostream>
using namespace std;


// ============================ named constants =============================
const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;


// ========================== function prototypes ===========================
void displayRectangle(Rectangle);


int main()
{
   createWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Draw circles");
   // declare a Rectangle object named one; the default constructor is called
   // to initialize the member variables of one
   Rectangle one;
   displayRectangle(one);
   one.draw(50, 50);

   // declare a Rectangle object named two; the set value constructor is called
   // to initialize the member variables of two; only four arguments are
   // passed to the constructor (the final two parameters have default values).
   Rectangle two(30, 50);
   displayRectangle(two);
   two.draw(100, 100);

   // declare a Rectangle object named three; the set value constructor is called
   // to initialize the member variables of three; only five arguments are
   // passed to the constructor (the final parameter has a default value).
   Rectangle three(100, 20, ORANGE);
   displayRectangle(three);
   three.draw(150, 200);

   // declare a Rectangle object named four; the set value constructor is called
   // to initialize the member variables of four; all six arguments are
   // passed to the constructor
   Rectangle four(150, 30, PURPLE, true);
   displayRectangle(four);
   four.draw(300, 290);

   startDrawing();

   return 0;
} // default constructor


/**
   display the attributes of a Rectangle object
   precondition: none
   postcondition: the attributes of a Rectangle object are displayed
*/
void displayRectangle(Rectangle which)
{
   cout << "Width is "
        << which.getWidth()
        << endl
        << "Height is "
        << which.getHeight()
        << endl
        << "Color is "
        << which.getColor()
        << endl
        << "Filling circle is "
        << which.isFill()
        << endl;
} // end of displayRectangle
