// file name -- rectangle.cpp

// This file contains the definitions of the member functions of
// the Rectangle class.


// ============================== header files ==============================
#include <sketch.h>                        // for Sketch class
#include "rectangle.h"                     // for Rectangle class definition
#include <iostream>
using namespace std;


Rectangle::Rectangle()
{
   width = 15;
   height = 10;
   color = BLUE;
   fill = false;
} // default constructor


Rectangle::Rectangle(double new_width,
                     double new_height,
                     ColorType new_color,
                     bool new_fill)
{
   width = new_width;
   height = new_height;
   color = new_color;
   fill = new_fill;
} // set value constructor


/**
   return the width of the rectangle
   precondition: none
   postcondition: the width of the rectangle is returned
*/
double Rectangle::getWidth()
{
   return width;
} // getWidth


/**
   return the height of the rectangle
   precondition: none
   postcondition: the height of the rectangle is returned
*/
double Rectangle::getHeight()
{
   return height;
} // getHeight


/**
   return the color of the rectangle
   precondition: none
   postcondition: the color of the rectangle is returned
*/
ColorType Rectangle::getColor()
{
   return color;
} // getColor


/**
   return the fill flag of the rectangle
   precondition: none
   postcondition: the fill flag is returned
*/
bool Rectangle::isFill()
{
   return fill;
} // isFill


/**
   change the width of the rectangle
   precondition: new_width is positive
   postcondition: the width of the rectangle is changed to new_width
*/
void Rectangle::changeWidth(double new_width)
{
   width = new_width;
} // changeWidth


/**
   change the height of the rectangle
   precondition: new_height is positive
   postcondition: the height of the rectangle is changed to new_height
*/
void Rectangle::changeHeight(double new_height)
{
   height = new_height;
} // changeHeight


/**
   change the color of the rectangle
   precondition: none
   postcondition: the color of the rectangle is changed to new_color
*/
void Rectangle::changeColor(ColorType new_color)
{
   color = new_color;
} // changeColor


/**
   change the fill flag of the rectangle
   precondition: none
   postcondition: the fill flag is changed to new_fill
*/
void Rectangle::changeFill(bool new_fill)
{
   fill = new_fill;
} // changeFill


/**
   draw the rectangle based on the attributes of the cirle
   precondition: both x and y are positive
   postcondition: the rectangle is drawn based on its attributes
*/
void Rectangle::draw(double x,
                     double y)
{
   setColor(color);
   if (fill)
      fillRectangle(x, y, width, height);
   else
   {
      moveTo(x, y);
      lineTo(x + width - 1, y);
      lineTo(x + width - 1, y + height - 1);
      lineTo(x, y + height - 1);
      lineTo(x, y);
      endOfShape();
   } // rectangle is not filled with a color
} // draw
