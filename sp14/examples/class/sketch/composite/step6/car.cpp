// file name -- car.cpp

// This file contains the definitions of the member functions of
// the Car class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "car.h"                        // for Car class definition
#include <iostream>
using namespace std;


/**
   initialize the object using default values
   precondition: none
   postcondition: all member variables are assigned default values
*/
Car::Car()
{
   cout << "default constructor of Car class is called"
        << endl;
} // default constructor


/**
   initialize the object using default values
   precondition: none
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
Car::Car(Rectangle top_,
         Rectangle body_,
         Circle front_wheel_,
         Circle rear_wheel_)
{
   // change top of the car
   top.changeWidth(top_.getWidth());
   top.changeHeight(top_.getHeight());
   top.changeColor(top_.getColor());
   top.changeFill(top_.isFill());

   // change body of the car
   body.changeWidth(body_.getWidth());
   body.changeHeight(body_.getHeight());
   body.changeColor(body_.getColor());
   body.changeFill(body_.isFill());

   // change front wheel of the car
   front_wheel.changeRadius(front_wheel_.getRadius());
   front_wheel.changeColor(front_wheel_.getColor());
   front_wheel.changeFill(front_wheel_.isFill());

   // change rear of the car
   rear_wheel.changeRadius(rear_wheel_.getRadius());
   rear_wheel.changeColor(rear_wheel_.getColor());
   rear_wheel.changeFill(rear_wheel_.isFill());

   cout << "set value constructor of Car class is called"
        << endl;
} // set value constructor


/**
   return the top of the car
   precondition: none
   postcondition: the top of the car is returned
*/
Rectangle Car::getTop()
{
   return top;
} // getTop


/**
   return the body of the car
   precondition: none
   postcondition: the body of the car is returned
*/
Rectangle Car::getBody()
{
   return body;
} // getBody


/**
   return the front wheel of the car
   precondition: none
   postcondition: the front wheel of the car is returned
*/
Circle Car::getFrontWheel()
{
   return front_wheel;
} // getFrontWheel


/**
   return the rear wheel of the car
   precondition: none
   postcondition: the rear wheel of the car is returned
*/
Circle Car::getRearWheel()
{
   return rear_wheel;
} // getRearWheel


/**
   change the top of the car
   precondition: none
   postcondition: the top is changed to top_
*/
void Car::changeTop(Rectangle top_)
{
   top.changeWidth(top_.getWidth());
   top.changeHeight(top_.getHeight());
   top.changeColor(top_.getColor());
   top.changeFill(top_.isFill());
} // changeTop


/**
   change the body of the car
   precondition: none
   postcondition: the body is changed to body_
*/
void Car::changeBody(Rectangle body_)
{
   body.changeWidth(body_.getWidth());
   body.changeHeight(body_.getHeight());
   body.changeColor(body_.getColor());
   body.changeFill(body_.isFill());
} // changeBody


/**
   change the front wheel of the car
   precondition: none
   postcondition: the front wheel is changed to front_wheel_
*/
void Car::changeFrontWheel(Circle front_wheel_)
{
   front_wheel.changeRadius(front_wheel_.getRadius());
   front_wheel.changeColor(front_wheel_.getColor());
   front_wheel.changeFill(front_wheel_.isFill());
} // changeFrontWheel


/**
   change the rear wheel of the car
   precondition: none
   postcondition: the rear wheel is changed to rear_wheel_
*/
void Car::changeRearWheel(Circle rear_wheel_)
{
   rear_wheel.changeRadius(rear_wheel_.getRadius());
   rear_wheel.changeColor(rear_wheel_.getColor());
   rear_wheel.changeFill(rear_wheel_.isFill());
} // changeRearWheel


/**
   draw the car based on the attributes of the car
   precondition: both x and y are positive
   postcondition: the car is drawn based on its attributes
*/
void Car::draw(double x,
               double y)
{
   double location_x, location_y;

   // calculate body location and draw
   location_x = x;
   location_y = y + front_wheel.getRadius();
   body.draw(location_x, location_y);

   // calculate top location and draw
   location_y += body.getHeight();
   top.draw(location_x, location_y);

   // calculate front wheel location and draw
   location_x = x + 7 + rear_wheel.getRadius();
   location_y = y + rear_wheel.getRadius();
   rear_wheel.draw(location_x, location_y);

   // calculate rear wheel location and draw
   location_x = body.getWidth() + x - 7 - front_wheel.getRadius();
   front_wheel.draw(location_x, location_y);
} // draw

