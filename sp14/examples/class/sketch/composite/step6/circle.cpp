// file name -- circle.cpp

// This file contains the definitions of the member functions of
// the Circle class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "circle.h"                     // for Circle class definition
#include <iostream>
using namespace std;


/**
   initialize the object using default values
   precondition: none
   postcondition: all member variables are assigned default values
*/
Circle::Circle()
{
   radius = 10;
   color = BLACK;
   fill = false;
   cout << "default constructor of Circle class is called"
        << endl;
} // default constructor


/**
   initialize the object using default values
   precondition: new_radius is all positive; new_color is one
                 of the predefined colors and new_fill is either true
                 or false
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
Circle::Circle(double new_radius,
               ColorType new_color,
               bool new_fill)
{
   radius = new_radius;
   color = new_color;
   fill = new_fill;
   cout << "set value constructor of Circle class is called"
        << endl;
} // set value constructor


/**
   return the radius of the circle
   precondition: none
   postcondition: the radius of the circle is returned
*/
double Circle::getRadius()
{
   return radius;
} // getCenterX


/**
   return the color of the circle
   precondition: none
   postcondition: the color of the circle is returned
*/
ColorType Circle::getColor()
{
   return color;
} // getColor


/**
   return the fill flag of the circle
   precondition: none
   postcondition: the fill flag is returned
*/
bool Circle::isFill()
{
   return fill;
} // isFill


/**
   change the radius of the circle
   precondition: new_radius is positive
   postcondition: the radius of the circle is changed to new_radius
*/
void Circle::changeRadius(double new_radius)
{
   radius = new_radius;
} // changeRadius


/**
   change the color of the circle
   precondition: none
   postcondition: the color of the circle is changed to new_color
*/
void Circle::changeColor(ColorType new_color)
{
   color = new_color;
} // changeColor


/**
   change the fill flag of the circle
   precondition: none
   postcondition: the fill flag is changed to new_fill
*/
void Circle::changeFill(bool new_fill)
{
   fill = new_fill;
} // changeFill


/**
   draw the circle based on the attributes of the circle
   precondition: both x and y are positive
   postcondition: the circle is drawn based on its attributes
*/
void Circle::draw(double x,
                  double y)
{
   setColor(color);
   if (fill)
      fillCircle(x, y, radius);
   else
      drawCircle(x, y, radius);
} // draw

