// file name -- driver6.cpp

// This program is served as a test driver to check the correctness of the
// Car class.


// ============================== header files ==============================
#include <sketch.h>                        // for Sketch class
#include "rectangle.h"                     // for Rectangle class definition
#include "circle.h"                        // for Circle class definition
#include "car.h"                           // for Car class definition
#include <iostream>
using namespace std;


// ============================ named constants =============================
const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;


// ========================== function prototypes ===========================
void displayRectangle(Rectangle);
void displayCircle(Circle);
void displayCar(Car);


int main()
{
   createWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Draw circles");

   // declare the parts of a car
   Rectangle top(150, 50, BLACK, true);
   Rectangle body(200, 70, BLUE, true);
   Circle front_wheel(35, PURPLE, true);
   Circle rear_wheel(35, PURPLE, true);

   Car your_car(top, body, front_wheel, rear_wheel);
   displayCar(your_car);
   your_car.draw(100, 100);

   // change top of car
   cout << "The new top color is GREEN =============="
        << endl;
   Rectangle new_top(180, 60, GREEN, true);
   your_car.changeTop(new_top);
   displayCar(your_car);

   // change body of car
   cout << "The new body color is PURPLE =============="
        << endl;
   Rectangle new_body(240, 80, PURPLE, true);
   your_car.changeBody(new_body);
   displayCar(your_car);

   // change front wheel of car
   cout << "The new front wheel size is 42 =============="
        << endl;
   Circle new_front_wheel(42, BLACK, true);
   your_car.changeFrontWheel(new_front_wheel);
   displayCar(your_car);

   // change front wheel of car
   cout << "The new rear wheel size is 42 =============="
        << endl;
   Circle new_rear_wheel(42, ORANGE, true);
   your_car.changeRearWheel(new_rear_wheel);
   displayCar(your_car);

   your_car.draw(320, 240);

   startDrawing();

   return 0;
} // default constructor


/**
   display the attributes of a Rectangle object
   precondition: none
   postcondition: the attributes of a Rectangle object are displayed
*/
void displayRectangle(Rectangle which)
{
   cout << "Width is "
        << which.getWidth()
        << endl
        << "Height is "
        << which.getHeight()
        << endl
        << "Color is "
        << which.getColor()
        << endl
        << "Filling circle is "
        << which.isFill()
        << endl;
} // end of displayRectangle


/**
   display the attributes of a Circle object
   precondition: none
   postcondition: the attributes of a Circle object are displayed
*/
void displayCircle(Circle which)
{
   cout << "Radius is "
        << which.getRadius()
        << endl
        << "Color is "
        << which.getColor()
        << endl
        << "Filling circle is "
        << which.isFill()
        << endl;
} // end of displayCircle


/**
   display the attributes of a Car object
   precondition: none
   postcondition: the attributes of a Car object are displayed
*/
void displayCar(Car car)
{
   cout << "Information about car top is"
        << endl;
   displayRectangle(car.getTop());

   cout << "Information about car body is "
        << endl;
   displayRectangle(car.getBody());

   cout << "Information about front wheel is "
        << endl;
   displayCircle(car.getFrontWheel());

   cout << "Information about rear wheel is "
        << endl;
   displayCircle(car.getRearWheel());
} // end of displayCar
