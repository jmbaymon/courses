// file name -- rectangle.h

// This file contains the definition of the Rectangle class. A rectangle object
// has a width, and a height. A Rectangle object also has a color attribute.
// When a Rectangle object is drawn, it can be filled with a color or not be
// filled with any color.

#ifndef RECTANGLE_CIRCLE_
#define RECTANGLE_CIRCLE_

// ============================== header files ==============================
#include <sketch.h>                    // for Sketch class


class Rectangle
{
   private:
      double width;
      double height;
      ColorType color;
      bool fill;

   public:
      Rectangle();
      Rectangle(double, double,
             ColorType new_color = BLACK,
             bool new_fill = false);

      double getWidth();
      double getHeight();
      ColorType getColor();
      bool isFill();
      double getArea();
      double getPerimeter();

      void changeWidth(double);
      void changeHeight(double);
      void changeColor(ColorType);
      void changeFill(bool);

      void draw(double, double);
}; // class Rectangle

#endif
