//  file name -- rational01.cpp
//  This file consists of the class implementation of rational which
//  represent an ADT of rational numbers in mathematics.


// =========================== header files =========================
#include <iostream>             // for cout
#include "rational01.h"         // for class rational


// ========================= namespaces used ========================
using namespace std;
using namespace csci220_examples;


Rational::Rational()
// purpose: initialize data members of the current Rational number by
//          setting them to 1 (the default values)
// precondition: none
// postcondition: both denominator and numerator are set to zero
{
   p = 1;
   q = 1;
   cout << "Default constructor is called"
        << endl;
}   // end of Rational::Rational


Rational::Rational(int numerator, int denominator)
// purpose: initialize data members of the current Rational number by
//          setting them to the values passed to the parameters
// precondition: none
// postcondition: both denominator and numerator are set to the
//               corresponding values of the parameters
{
   p = numerator;
   q = denominator;
   cout << "Set value constructor is called"
        << endl;
}   // end of Rational::Rational


Rational::Rational(const Rational &source)
// purpose: initialize data members of the current Rational number by
//          copying the data members of source to the current object
// precondition: source is well-defined
// postcondition: the content of source is copied to the current object
{
   p = source.p;
   q = source.q;
   cout << "Copy constructor is called"
        << endl;
}   // end of Rational::Rational


void Rational::setNumerator(int numerator)
// purpose: set new value to the numerator of current object
// precondition: none
// postcondition: the numerator of the current object is set to
//               the new value stored in numerator
{
   p = numerator;
}   // end of Rational::set



void Rational::setDenominator(int denominator)
// purpose: set new value to the denominator of current object
// precondition: none
// postcondition: the denominator of the current object is set to
//               the new value stored in denominaotr
{
   q = denominator;
}   // end of Rational::set


void Rational::add(Rational another)
// purpose: add a Rational to the current Rational object
// precondition: another is a well-defined rational number
// postcondition: another Rational number is added to the current
//               Rational number
{
   p = p * another.q + q * another.p;
   q = q * another.q;
}   // end of Rational::add


void Rational::subtract(Rational another)
// purpose: subtract a Rational from the current Rational object
// precondition: another is a well-defined rational number
// postcondition: another Rational number is subtracted from the
//               current Rational number
{
   p = p * another.q - q * another.p;
   q = q * another.q;
}   // end of Rational::subtract


void Rational::multiply(Rational another)
// purpose: multiply a Rational by the current Rational object
// precondition: another is a well-defined rational number
// postcondition: the current Rational number is multiplied by another
//               Rational number
{
   p = p * another.p;
   q = q * another.q;
}   // end of Rational::multiply


void Rational::divide(Rational another)
// purpose: divide a Rational into the current Rational object
// precondition: another is a well-defined rational number
// postcondition: the current Rational number is divided by another
//               Rational number
{
   p = p * another.q;
   q = q * another.p;
}   // end of Rational::divide


int Rational::compare(Rational another)
// purpose: compare another Rational number with the current Rational
//          object
// precondition: another is a well-defined rational number
// postcondition: if the current Rational number is larger than another
//               Rational number, the function returns 1; if they are
//               the same, the function returns 0; otherwise, the
//               function returns -1.
{
   return 0;
}   // end of Rational::compare


int Rational::getNumerator()
// purpose: return the numerator of the rational number
// precondition: none
// postcondition: the numerator of the rational number is returned
// return the numerator of a rational number
{
   return p;
}   // end of Rational::getNumerator


int Rational::getDenominator()
// purpose: return the Denominator of the rational number
// precondition: none
// postcondition: the denominator of the rational number is returned
{
   return q;
}   // end of Rational::Denominator
