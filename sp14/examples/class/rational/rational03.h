//  file name -- rational03.h
//  This file consists of the class declaration of rational which
//  represent an ADT of rational numbers in mathematics.

#ifndef Rational_class_
#define Rational_class_

namespace csci220_examples
{

   class Rational
   {
      private:
         int p;                    // numerator of a Rational number
         int q;                    // denominator of a Rational number

      public:
         Rational(int x = 1, int y = 1); // set value constructor
         Rational(const Rational &); // copy constructor

         void setNumerator(int);   // set new value to numerator
         void setDenominator(int); // set new value to denominator
         void add(Rational);       // add another Rational number to
				// the current Rational object
         void subtract(Rational);  // subtract a Rational from the
				// current Rational object
         void multiply(Rational);  // multiply a Rational with the
				// current Rational object
         void divide(Rational);    // divide a Rational into the
				// current Rational object
         int compare(Rational);    // compare another Rational number
				// with the current Rational object
         int getNumerator();       // return numerator of a Rational object
         int getDenominator();     // return denominator of a Rational object

         // overloaded operators
         Rational operator+(Rational);
         Rational operator=(Rational);

   };   // end of class Rational

} // end of namespace csci220_examples

#endif  // end of Rational_class_
