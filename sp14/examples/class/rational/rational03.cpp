//  file name -- rational02.cpp
//  This file consists of the class implementation of rational which
//  represent an ADT of rational numbers in mathematics.

//  ========================= header files =======================
#include <iostream>             // for cout
#include "rational03.h"         // for class rational


//  ======================== namespaces used =====================
using namespace std;
using namespace csci220_examples;

/*
Rational::Rational()
// default constructor
{
   p = 1;
   q = 1;
   cout << "Default constructor is called"
        << endl;
}   // end of Rational::Rational
*/

Rational::Rational(int numerator, int denominator)
// set values constructor
{
   p = numerator;
   q = denominator;
   cout << "Set value constructor is called"
        << endl;
}   // end of Rational::Rational


Rational::Rational(const Rational &source)
// copy constructor
{
   p = source.p;
   q = source.q;
   cout << "Copy constructor is called"
        << endl;
}   // end of Rational::Rational


void Rational::setNumerator(int numerator)
// set new value to the numerator of current object
{
   p = numerator;
}   // end of Rational::set



void Rational::setDenominator(int denominator)
// set new value to the denominator of current object
{
   q = denominator;
}   // end of Rational::set


void Rational::add(Rational another)
// add another Rational number to the current Rational object
{
   p = p * another.q + q * another.p;
   q = q * another.q;
}   // end of Rational::add


void Rational::subtract(Rational another)
// subtract a Rational from the current Rational object
{
   p = p * another.q - q * another.p;
   q = q * another.q;
}   // end of Rational::subtract


void Rational::multiply(Rational another)
// multiply a Rational with the current Rational object
{
   p = p * another.p;
   q = q * another.q;
}   // end of Rational::multiply


void Rational::divide(Rational another)
// divide a Rational into the current Rational object
{
   p = p * another.q;
   q = q * another.p;
}   // end of Rational::divide


int Rational::compare(Rational another)
// compare another Rational number with the current Rational object
// if the current Rational number is larger than another Rational
// number, the function returns 1; if they are the same, the
// function returns 0; otherwise, the function returns -1.
{
   return 0;
}   // end of Rational::compare


int Rational::getNumerator()
// return the numerator of a rational number
{
   return p;
}   // end of Rational::getNumerator


int Rational::getDenominator()
// return the Denominator of a rational number
{
   return q;
}   // end of Rational::Denominator


Rational Rational::operator+(Rational another)
// add another Rational number to the current Rational object and
// return a new Rational object.
{
   Rational one;

   one.p = p * another.q + q * another.p;
   one.q = q * another.q;

   return one;
}   // end of Rational::operator+()


Rational Rational::operator=(Rational source)
// assign the content of source to the current object and return
// a new Rational object.
{
   p = source.p;
   q = source.q;

   return *this;
}   // end of Rational::operator=()
