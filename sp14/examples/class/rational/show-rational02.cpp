//  file show-rational02.cpp
//  This program demonstrates how to use the Rational class defined
//  in the rational.h and rational.cpp files.

// =========================== header files =========================
#include <iostream>                  // for console I/O
#include "rational02.h"


// ========================= namespaces used ========================
using namespace std;
using namespace csci220_examples;


int main()
{
   Rational one;          // one created by default constructor
   Rational two(3, 5);    // created by setting value constructor

   // print the content of the object one created by the default
   // constructor
   cout << "The value of Rational number one is "
        << one.getNumerator()
        << "/"
        << one.getDenominator()
        << endl;

   // print the content of the object two created by the second
   // constructor
   cout << "The value of Rational number two is "
        << two.getNumerator()
        << "/"
        << two.getDenominator()
        << endl;

   // change the content of one and display the result
   one.setNumerator(7);
   one.setDenominator(3);
   cout << "After set operation, the new value of one is "
        << one.getNumerator()
        << "/"
        << one.getDenominator()
        << endl;

   // add one to two and display the resullt
   two = one + two;
   cout << "After addition, the new value of one is "
        << one.getNumerator()
        << "/"
        << one.getDenominator()
        << endl;

   two.operator=(one.operator+(two));
   cout << "After addition, the new value of two is "
        << two.getNumerator()
        << "/"
        << two.getDenominator()
        << endl;

   return 0;
}  // end of main
