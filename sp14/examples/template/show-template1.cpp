//  file name -- show-template1.cpp
//  This program demonstrates how to use template in C++.

//  =========================== header files =========================
#include <iostream>           // for console I/O
#include "utility.h"
using namespace std;

//  =========================== function prototypes ==================
template <typename WhoCares>
void swapMe(WhoCares &, WhoCares&);

int main()
{
   int a, b;
   float c, d;

   clearScreen();
   a = 5;
   b = 10;

   cout << "Before swapMe, a = "
        << a
        << " b = "
        << b
        << endl;

   swapMe<int>(a, b);

   cout << "After swapMe, a = "
        << a
        << " b = "
        << b
        << endl;
   pauseScreen();

   clearScreen();
   c = -2.5;
   d = 1.05;

   cout << "Before swapMe, c = "
        << c
        << " d = "
        << d
        << endl;

   swapMe(c, d);

   cout << "After swapMe, c = "
        << c
        << " d = "
        << d
        << endl;


   return 0;
}  // end of main


template <class WhoCares>
void swapMe(WhoCares &one,
            WhoCares &two)
{
   WhoCares temp;

   temp = one;
   one = two;
   two = temp;
}  // end of swapMe
