#  Build an executable module named show-template2 which will be rebuilt
#  if either show-template2.o, pool.o or utility.o has been updated.
#  The command
#
#     c++ show-template2.o pool.o utility.o -o show-template2
#
#  links show-template2.o, pool.o and utility.o to build an executable
#  (or load) module named show-template2.
#
show-template2: show-template2.o pool.o utility.o
	c++ show-template2.o pool.o utility.o -o show-template2

#  Build an object file named show-template2.o which will be rebuilt
#  if show-template2.cpp, utility.o or pool.o has been updated.
#  The command
#
#     c++ -c show-template2.cpp
#
#  compiles the source program show-template2.cpp and builds an object
#  file named show-template2.o.
#
show-template2.o: show-template2.cpp utility.o pool.o
	c++ -c show-template2.cpp

#  Build an object file named utility.o which will be rebuilt
#  if utility.h or utility.cpp has been updated. The command
#
#     c++ -c utility.cpp
#
#  compiles the source program utility.cpp and builds an object
#  file named utility.o.
#
utility.o: utility.cpp utility.h
	c++ -c utility.cpp

#  Build an object file named pool.o which will be rebuilt
#  if pool.h or pool.cpp has been updated. The command
#
#     c++ -c pool.cpp
#
#  compiles the source program pool.cpp and builds an object
#  file named pool.o.
#
pool.o: pool.cpp pool.h
	c++ -c pool.cpp
