// file name -- show-array01.cpp

// This program demonstrates how class template works.

// ======================== header files =========================
#include <iostream>                 // for console I/O
#include "array.h"                  // for array class
using namespace std;

int main()
{
   int size;
   Array<int> one;
   Array<float> two(20);
   Array<long>* three;

   three = new Array<long>(26);

   cout << "The size of array one is "
        << one.getSize()
        << endl
        << "The size of array two is "
        << two.getSize()
        << endl
        << "The size of the array pointed to by three is "
        << three -> getSize()
        << endl;

   return 0;
} // end of main
