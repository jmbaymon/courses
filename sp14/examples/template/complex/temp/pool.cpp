//  file name -- pool.cpp
//  This file contains the implementation of the Pool class.

//  ======================= header files ========================
#include "pool.h"                  // for Pool class


//  =============== implementation of functions =================
Pool::Pool()
// purpose: set default values to the current object.
// preconditions: none
// postconditions: default values for length, width and height are
//          set for the current object.
{
   length = 10;
   width = 10;
   height = 6;
}  // default constructor


Pool::Pool(double new_length,
           double new_width,
           double new_height)
// purpose: set new_length, new_width, and new_height to the current
//          object.
// preconditions: new_length, new_width and new_height are positive.
// postconditions: new_length, new_width and new_height are set for
//          the current object.
{
   length = new_length;
   width = new_width;
   height = new_height;
}  // set value constructor


Pool::Pool(const Pool &source)
// purpose: copy the attributes stored in source to the current
//          object.
// preconditions: source is a well-defined Pool object.
// postconditions: the attributed stored in source are copied to the
//          current object.
{
   length = source.length;
   width = source.width;
   height = source.height;
}  // default constructor


void Pool::changeSize(double new_length,
                      double new_width,
                      double new_height)
// purpose: change size of the current pool object.
// preconditions: new_length, new_width and new_height are positive.
// postconditions: new_length, new_width and new_height are set for
//          the current object.
{
   length = new_length;
   width = new_width;
   height = new_height;
}  // end of changeSize


double Pool::getLength()
// purpose: return the length of the current pool object.
// preconditions: current pool object is well defined.
// postconditions: length of current pool object is returned.
{
   return length;
}  // end of getLength


double Pool::getWidth()
// purpose: return the width of the current pool object.
// preconditions: current pool object is well defined.
// postconditions: width of current pool object is returned.
{
   return width;
}  // end of getWidth


double Pool::getHeight()
// purpose: return the height of the current pool object.
// preconditions: current pool object is well defined.
// postconditions: height of current pool object is returned.
{
   return height;
}  // end of getHeight


double Pool::getVolume()
// purpose: return the volume of the current pool object.
// preconditions: current pool object is well defined.
// postconditions: volume of current pool object is returned.
{
   return length * width * height;
}  // end of getVolume
