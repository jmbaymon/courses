//  file name -- array.h
//  This header file contains the specifications needed to represent
//  a rectangular array.

#ifndef ARRAY_CLASS_
#define ARRAY_CLASS_

const int DEFAULT_SIZE = 30;

template <class ItemType>
class Array
{
   private:
      ItemType *data;
      int size;

   public:
      Array(int size = DEFAULT_SIZE);
      Array(const Array &);
      ~Array();

      ItemType& operator[](int);
      int getSize();
      void operator=(const Array<ItemType> &);
};  // class Array

#endif
