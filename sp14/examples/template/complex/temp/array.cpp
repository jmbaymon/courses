//  file name -- array.cpp
//  This file contains the implementation of the Array class.

#include "array.h"

//  =============== implementation of functions =================
template <class ItemType>
Array<ItemType>::Array(int size)
// purpose: allocate a dynamic array using the size passed to the
//          constructor.
// preconditions: size is positive.
// postconditions: a dynamic array is allocated based on the size and
//          the instance variable size is set to the size of the array.
{
   data = new ItemType[size];
   this -> size = size;
}  // set value constructor


template <class ItemType>
Array<ItemType>::Array(const Array &source)
// purpose: copy the attributes stored in source to the current
//          object.
// preconditions: source is a well-defined Array object.
// postconditions: the attributed stored in source are copied to the
//          current object.
{
   size = source.size;
   data = new ItemType[size];
   for (int index = 0; index < size; index++)
      data[index] = source.data[index];
}  // copy constructor


template <class ItemType>
Array<ItemType>::~Array()
// purpose: free the dynamic array
// preconditions: none
// postconditions: the dynamic array is freed and size set to zero.
{
   delete [] data;
   size = 0;
}  // default constructor


template <class ItemType>
ItemType& Array<ItemType>::operator[](int index)
// purpose: obtain a reference to an element in the array.
// preconditions: index is non-negative
// postconditions: a reference to an array element is returned.
{
   return data[index];
}  // end of operator[]


template <class ItemType>
int Array<ItemType>::getSize()
{
   return size;
} // end of getSize


template <class ItemType>
void Array<ItemType>::operator=(const Array<ItemType> &source)
// purpose: copy the attributes stored in source to the current
//          object.
// preconditions: source is a well-defined Array object.
// postconditions: the attributed stored in source are copied to the
//          current object.
{
   if (this != &source)
   {
      delete[] data;
      size = source.size;
      data = new ItemType[size];
      for (int index = 0; index < size; index++)
         data[index] = source.data[index];
   }
}  // copy operator=
