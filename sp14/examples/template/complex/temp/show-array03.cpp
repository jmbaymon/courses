// file name -- show-array03.cpp

// This program demonstrates how class template works and how to pass
// a function address to a parameter.

// ======================== header files =========================
#include <iostream>                 // for console I/O
#include "array.h"                  // for array class
#include "pool.h"                   // for Pool class
#include "utility.h"                // for utility functions
using namespace std;

// ==================== data type declarations ===================
enum Rating{EXCELLENT, GOOD, SO_SO, BAD, TERRIBLE};

// ====================== function prototypes ====================
template <typename ArrayType>
void displayArray(ArrayType);

// The following function template has two parameter: ArrayType and
// ElementType. The ArrayType indicates the type of Array object
// is passed to the function; ElementType denotes the data type
// of each element in the Array object. This function has two
// parameters. The first parameter receives a copy of an Array object
// and the second parameter is a pointer to a function which has a
// single parameter. The address of this function is passed to it
// when this function is invoked. It is used to call the actual
// function whthin this function.
template <typename ArrayType, typename ElementType>
void displayArray(ArrayType, void (*process)(ElementType));


void displayAPool(Pool*);
void displayAnIntPlus(int);
void displayRating(Rating);

int main()
{
   int size;
   Array<int> one;
   Array<float> two(20);
   Array<long>* three;
   Array<Pool*> four(3);
   Array<Rating> five(4);

   three = new Array<long>(26);

   one[0] = 30;
   one[1] = 25;
   one[5] = one[1] + one[0];
   displayArray(one);
   displayArray(one, displayAnIntPlus);
   pauseScreen();

   two[3] = 0.5;
   two[2] = 12.5;
   two[19] = -3.5;
   displayArray(two);
   pauseScreen();

   (*three)[0] = 234;
   (*three)[2] = 123;
   (*three)[25] = -1234;
   displayArray(*three);
   pauseScreen();

   four[0] = new Pool(10, 20, 30);
   four[1] = new Pool(30, 10, 20);
   four[2] = new Pool(15, 10, 25);

   // The address of displayAPool function is passed to the
   // pointer variable in the displayArray function.
   displayArray(four, displayAPool);

   five[0] = GOOD;
   five[1] = EXCELLENT;
   five[2] = SO_SO;
   five[3] = BAD;
   displayArray(five);
   displayArray(five, displayRating);

   return 0;
} // end of main


template <typename ArrayType>
void displayArray(ArrayType one)
{
   for (int index = 0; index < one.getSize(); index++)
      cout << "["
           << index
           << "] = "
           << one[index]
           << endl;
} // end of displayArray


// The following function template has two parameter: ArrayType and
// ElementType. The ArrayType indicates the type of Array object
// is passed to the function; ElementType denotes the data type
// of each element in the Array object. This function has two
// parameters. The first parameter receives a copy of an Array object
// and the second parameter is a pointer to a function which has a
// single parameter. The address of this function is passed to it
// when this function is invoked. It is used to call the actual
// function whthin this function as shown on line 95.
template <typename ArrayType, typename ElementType>
void displayArray(ArrayType one,
                  void (*process)(ElementType data))
{
   for (int index = 0; index < one.getSize(); index++)
      process(one[index]); // calls displayAPool function
} // end of displayArray


void displayAPool(Pool* one)
{
   cout << "Length: "
        << one -> getLength()
        << endl
        << "Width: "
        << one -> getWidth()
        << endl
        << "Height: "
        << one -> getHeight()
        << endl;
} // end of displayARecord


void displayRating(Rating one)
{
   static int index = 0;

   cout << "newarray["
        << index
        << "] = ";

   switch (one)
   {
      case 0: cout << "EXCELLENT";
              break;
      case 1: cout << "GOOD";
              break;
      case 2: cout << "SO_SO";
              break;
      case 3: cout << "BAD";
              break;
      case 4: cout << "TERRIBLE";
              break;
   } // switch

   cout << endl;
   index++;
} // end of displayARecord


void displayAnIntPlus(int one)
{
   static int index = 0;

   cout << "newarray["
        << index
        << "] = "
        << one
        << endl;
   index++;
} // end of displayARecord
