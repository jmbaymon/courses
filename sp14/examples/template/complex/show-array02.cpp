// file name -- show-array02.cpp

// This program demonstrates how class template works.

// ======================== header files =========================
#include <iostream>                 // for console I/O
#include "array.h"                  // for array class
#include "utility.h"                // for utility functions
using namespace std;

// ====================== function prototypes ====================
template <typename ItemType>
void displayArray(ItemType);

int main()
{
   int size;
   Array<int> one;
   Array<float> two(20);
   Array<long>* three;

   three = new Array<long>(26);

   one[0] = 30;// one.assign(0,30)
   one[1] = 25;
   one[5] = one[1] + one[0];
   displayArray(one);
   pauseScreen();

   two[3] = 0.5;
   two[2] = 12.5;
   two[19] = -3.5;
   displayArray(two);
   pauseScreen();

   (*three)[0] = 234;
   (*three)[2] = 123;
   (*three)[25] = -1234;
   displayArray(*three);
   pauseScreen();

   return 0;
} // end of main


template <typename ItemType>
void displayArray(ItemType one)
{
   for (int index = 0; index < one.getSize(); index++)
      cout << "["
           << index
           << "] = "
           << one[index]
           << endl;
} // end of displayArray
