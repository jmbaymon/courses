#  Build an executable module named show-template1 which will be rebuilt
#  if either show-template1.o or utility.o has been updated. The command
#
#     c++ show-template1.o utility.o -o show-template1
#
#  links show-template1.o and utility.o to build an executable (or load)
#  module named show-template1.
#
show-template1: show-template1.o utility.o
	c++ show-template1.o utility.o -o show-template1

#  Build an object file named show-template1.o which will be rebuilt
#  if show-template1.cpp, utility.h or utility.cpp has been updated.
#  The command
#
#     c++ -c show-template1.cpp
#
#  compiles the source program show-template1.cpp and builds an object
#  file named show-template1.o.
#
show-template1.o: show-template1.cpp utility.h utility.cpp
	c++ -c show-template1.cpp

#  Build an object file named utility.o which will be rebuilt
#  if utility.h or utility.cpp has been updated. The command
#
#     c++ -c utility.cpp
#
#  compiles the source program utility.cpp and builds an object
#  file named utility.o.
#
utility.o: utility.cpp utility.h
	c++ -c utility.cpp
