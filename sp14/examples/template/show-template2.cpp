//  file name -- show-template2.cpp
//  This program demonstrates how to use template in C++.

//  =========================== header files =========================
#include <iostream>           // for console I/O
#include "utility.h"
#include "pool.h"                     // for Pool class
using namespace std;


//  =========================== function prototypes ==================
void displayAPool(Pool);

template <class ItemType>
void swapMe(ItemType &, ItemType&);

int main()
{
   Pool one(10, 20, 30);
   Pool two(20, 5, 12);

   clearScreen();
   
   cout << "Before swapMe, Information about pool one is "
        << endl;
   displayAPool(one);
   cout << "Before swapMe, Information about pool two is "
        << endl;
   displayAPool(two);

   swapMe<Pool>(one, two);

   cout << endl
        << "After swapMe, Information about pool one is "
        << endl;
   displayAPool(one);
   cout << "After swapMe, Information about pool two is "
        << endl;
   displayAPool(two);

   return 0;
}  // end of main


template <class ItemType>
void swapMe(ItemType &one,
            ItemType &two)
{
   ItemType temp;

   temp = one;
   one = two;
   two = temp;
}  // end of swapMe


void displayAPool(Pool whatever)
// purpose: display the content of a pool
// preconditions: whatever is a well-defined pool object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "Lenght of the pool is "
        << whatever.getLength()
        << endl
        << "Width of the pool is "
        << whatever.getWidth()
        << endl
        << "Height of the pool is "
        << whatever.getHeight()
        << endl
        << "Volume of the pool is "
        << whatever.getVolume()
        << endl;
}  // end of displayAPool
