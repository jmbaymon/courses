#  Build an executable module named pointerd which will be rebuilt 
#  if either pointerd.o, utitlity.o, or pool.o has been updated.
#  The command
#
#     c++ pointerd.o pool.o -o pointerd
#
#  link pointerd.o, utility.o and pool.o to the math library to build
#  an executable (or load) module named pointerd.
#

pointerd: pointerd.o pool.o utility.o
	c++ pointerd.o pool.o utility.o  -o pointerd


#  Build an object file named pool.o which will be built
#  if pool.h or pool.cpp has been updated. The command
#
#     c++ -c pool.cpp
#
#  compile the source program pool.cpp and build an object
#  file named pool.o.
#

pool.o: pool.cpp pool.h
	c++ -c pool.cpp


#  Build an object file named pointerd.o which will be built
#  if pointerd.cpp pool.h or pool.cpp has been updated.
#  The command
#
#     c++ -c pointerd.cpp
#
#  compile the source program pointerd.cpp and build an object
#  file named pointerd.o.
#

pointerd.o: pointerd.cpp pool.h pool.cpp
	c++ -c pointerd.cpp


#  Build an object file named utility.o which will be built
#  if utility.cpp or utility.h has been updated.
#  The command
#
#     c++ -c utility.cpp
#
#  compile the source program utility.cpp and build an object
#  file named utility.o.
#

utility.o: utility.cpp utility.h
	c++ -c utility.cpp
