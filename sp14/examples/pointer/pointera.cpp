//  file name -- pointera.cpp
//  This program demonstrate the basic concepts of pointer variables,
//  dynamic memory allocation, and parameters.

//  ======================= header files ========================
#include <iostream>                   // for cin & cout
using namespace std;


//  =================== function prototypes =====================
void readData(double *, double *);
void computeAreaPerimeter(double *, double *, double &, double &);
void printResult(double, double, double, double);

int main()
{
   double *width, *length;
   double area, perimeter;

   // allocate memory locations dynamically
   width = new double;//pointee
   length = new double;//pointee

   readData(length, width);
   computeAreaPerimeter(length, width, area, perimeter);
   printResult(*length, *width, area, perimeter);

   return 0;
}   // end of main


void readData(double *length,
             double *width)
//  This function reads in length and width of a rectangle and
//  stored them in the variables pointed to by pointers.
{
   cout << "Enter the length of the rectangle: ";
   cin >> *length;
   cout << "Enter the width of the rectangle: ";
   cin >> *width;
}   // end readData


void computeAreaPerimeter(double *length,
                          double *width,
                          double &area,
                          double &perimeter)
//  This function computes the area and perimeter of a rectangle
//  based on given length and width.
{
   area = (*length) * (*width);
   perimeter = (*length + *width) * 2.0;
}   // end computeAreaPerimeter


void printResult(double length,
                 double width,
                 double area,
                 double perimeter)
//  This function displays the length, width, area, and perimeter
//  of a rectangle.
{
   cout << endl
        << "The length of rectangle is "
        << length
        << " and the width is "
        << width
        << endl
        << "The area of the rectangle is "
        << area
        << " and its perimeter is "
        << perimeter
        << endl;
}   // end printResult
