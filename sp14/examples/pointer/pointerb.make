#  Build an executable module named pointerb which will be rebuilt 
#  if either pointerb.o, utitlity.o, or pool.o has been updated.
#  The command
#
#     c++ pointerb.o pool.o -o pointerb
#
#  link pointerb.o, utility.o and pool.o to the math library to build
#  an executable (or load) module named pointerb.
#

pointerb: pointerb.o pool.o utility.o
	c++ pointerb.o pool.o utility.o  -o pointerb


#  Build an object file named pool.o which will be built
#  if pool.h or pool.cpp has been updated. The command
#
#     c++ -c pool.cpp
#
#  compile the source program pool.cpp and build an object
#  file named pool.o.
#

pool.o: pool.cpp pool.h
	c++ -c pool.cpp


#  Build an object file named pointerb.o which will be built
#  if pointerb.cpp pool.h or pool.cpp has been updated.
#  The command
#
#     c++ -c pointerb.cpp
#
#  compile the source program pointerb.cpp and build an object
#  file named pointerb.o.
#

pointerb.o: pointerb.cpp pool.h pool.cpp
	c++ -c pointerb.cpp


#  Build an object file named utility.o which will be built
#  if utility.cpp or utility.h has been updated.
#  The command
#
#     c++ -c utility.cpp
#
#  compile the source program utility.cpp and build an object
#  file named utility.o.
#

utility.o: utility.cpp utility.h
	c++ -c utility.cpp
