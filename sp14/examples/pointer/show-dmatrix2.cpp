//  file name -- show-dmatrix2.cpp
//  This program shows how to instantiate a Matrix object dynamically
//  and relinquish the storage.

#include "dmatrix.h"               // for matrix class
#include <iostream>                // for cin & cout
using namespace std;


int main()
{
   Matrix *one;
   Matrix *two;

   // instantiate a matrix object using a pointer
   one = new Matrix(3, 4);
   one -> setElement(2, 1, -5.0);
   one -> setElement(1, 3, 1.25);
   cout << "Matrix one is "
	     << (*one).getRows()
	     << " by "
	     << (*one).getColumns()
	     << " and its contents are: "<< endl;
   (*one).print();

   // instantiate a matrix object using a pointer
   two = new Matrix(6, 2);
   (*two).setElement(4, 1, 5.0);
   (*two).setElement(1, 0, 4.25);
   cout << endl;
   cout << "Matrix two is "
	     << two -> getRows()
	     << " by "
	     << two -> getColumns()
	     << " and its contents are: "<< endl;
   two -> print();

   return 0;
}   // end main
