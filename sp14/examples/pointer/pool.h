//  file name -- pool.h
//  This header file contains the specifications needed to represent
//  a rectangular pool.

#ifndef POOL_CLASS_
#define POOL_CLASS_

class Pool
{
   private:
      double length;
      double width;
      double height;

   public:
      Pool();
      Pool(double, double, double);
      Pool(const Pool &);

      void changeSize(double, double, double);
      double getVolume();
      double getLength();
      double getWidth();
      double getHeight();
};  // class Pool

#endif
