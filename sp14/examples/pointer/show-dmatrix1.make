#  Build an executable module named show-matrix1 which will be rebuilt
#  if either show-matrix1.o or dmatrix.o has been updated. The command
#
#     c++ show-dmatrix1.o dmatrix.o -o show-dmatrix1
#
#  links show-dmatrix1.o and dmatrix.o to build an executable (or load)
#  module named show-dmatrix1.
#
show-dmatrix1: show-dmatrix1.o dmatrix.o
	c++ show-dmatrix1.o dmatrix.o -o show-dmatrix1

#  Build an object file named show-dmatrix1.o which will be rebuilt
#  if show-dmatrix1.cpp, dmatrix.h or dmatrix.cpp has been updated.
#  The command
#
#     c++ -c show-dmatrix1.cpp
#
#  compiles the source program show-dmatrix1.cpp and builds an object
#  file named show-dmatrix1.o.
#
show-dmatrix1.o: show-dmatrix1.cpp dmatrix.h dmatrix.cpp
	c++ -c show-dmatrix1.cpp

#  Build an object file named dmatrix.o which will be rebuilt
#  if dmatrix.h or dmatrix.cpp has been updated. The command
#
#     c++ -c dmatrix.cpp
#
#  compiles the source program dmatrix.cpp and builds an object
#  file named dmatrix.o.
#
dmatrix.o: dmatrix.cpp dmatrix.h
	c++ -c dmatrix.cpp
