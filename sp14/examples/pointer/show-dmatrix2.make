#  Build an executable module named show-matrix2 which will be rebuilt
#  if either show-matrix2.o or dmatrix.o has been updated. The command
#
#     CC show-dmatrix2.o dmatrix.o -o show-dmatrix2
#
#  links show-dmatrix2.o and dmatrix.o to build an executable (or load)
#  module named show-dmatrix2.
#
show-dmatrix2: show-dmatrix2.o dmatrix.o
	CC show-dmatrix2.o dmatrix.o -o show-dmatrix2

#  Build an object file named show-dmatrix2.o which will be rebuilt
#  if show-dmatrix2.cpp, dmatrix.h or dmatrix.cpp has been updated.
#  The command
#
#     CC -c show-dmatrix2.cpp
#
#  compiles the source program show-dmatrix2.cpp and builds an object
#  file named show-dmatrix2.o.
#
show-dmatrix2.o: show-dmatrix2.cpp dmatrix.h dmatrix.cpp
	CC -c show-dmatrix2.cpp

#  Build an object file named dmatrix.o which will be rebuilt
#  if dmatrix.h or dmatrix.cpp has been updated. The command
#
#     CC -c dmatrix.cpp
#
#  compiles the source program dmatrix.cpp and builds an object
#  file named dmatrix.o.
#
dmatrix.o: dmatrix.cpp dmatrix.h
	CC -c dmatrix.cpp
