//  file name -- pointere.cpp
//  This program demonstrates how to instantiate an array of pool
//  objects using pointer variables.

//  ==================== header files ===============================
#include <iostream>                      // for cin & cout
#include <fstream>                       // for file I/O
#include "utility.h"                     // for user-defined functions
#include "pool.h"                        // for Pool calss
using namespace std;

//  ==================== symbolic constants =========================
const int MAX_SIZE = 100;

//  ==================== function prototpyes ========================
void readData(ifstream &infile, Pool*&, int &);
void displayResults(Pool *, int);
void displayAPool(Pool);

int main()
{
   ifstream infile;
   Pool *whatever;
   int size;

   infile.open("pool01.dat");
   readData(infile, whatever, size);
   displayResults(whatever, size);
   infile.close();

   return 0;
}   // end of main


void readData(ifstream &infile,
              Pool*& one,
              int &n)
// purpose: read data from file and store them in an array of pool
//          objects.
// preconditions: data file has been opened successfully.
// postconditions: data have been read from the data file and stored
//          in the objects of an array and size is upated properly.
{
   double length, width, height;
   int index;
   Pool *whatever;

   one = new Pool[MAX_SIZE];
   index = 0;
   n = 0;
   infile >> length;
   while (infile)
   {
      infile >> width >> height;
      whatever = new Pool(length, width, height);
      one[index] = *whatever;
      delete whatever;             // deallocate object to free space
      index++;
      n++;
      infile >> length;
   }   // while
}   // end readData


void displayResults(Pool *two,
                    int size)
// purpose: display the content of pool objects stored in an array.
// preconditions: array objects have been assigned values and size
//          is positive.
// postconditions: the contents of pool objects stored in the array
//          are displayed.
{
   int i;
   int index;

   index = 0;
   for (i = 1; i <= size; i++)
   {
      clearScreen();
      displayAPool(two[index]);
      index++;
      pauseScreen();
   }   // for
}   // end displayResults


void displayAPool(Pool whatever)
// purpose: display the content of a pool
// preconditions: whatever is a well-defined pool object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "Lenght of the pool is "
        << whatever.getLength()
        << endl
        << "Width of the pool is "
        << whatever.getWidth()
        << endl
        << "Height of the pool is "
        << whatever.getHeight()
        << endl
        << "Volume of the pool is "
        << whatever.getVolume()
        << endl;
}  // end of displayAPool
