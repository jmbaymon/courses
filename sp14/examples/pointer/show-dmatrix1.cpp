//  file name -- show-dmatrix1.cpp
//  This program shows how to instantiate a Matrix object that contains
//  a dynamic array. The array storage is allocated by the constructor
//  relinguished by the destructor.

#include "dmatrix.h"               // for matrix class
#include <iostream>                // for cin & cout
using namespace std;


int main()
{
   Matrix one(3, 4);
   Matrix two(6, 2);

   one.setElement(2, 1, -5.0);
   one.setElement(1, 3, 1.25);
   cout << "Matrix one is "
	<< one.getRows()
	<< " by "
	<< one.getColumns()
	<< " and its contents are: "<< endl;
   one.print();
   two.setElement(4, 1, 5.0);
   two.setElement(1, 0, 4.25);
   cout << "Matrix two is "
	<< two.getRows()
	<< " by "
	<< two.getColumns()
	<< " and its contents are: "<< endl;
   two.print();

   return 0;
}   // end main
