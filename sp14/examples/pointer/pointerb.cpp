
//  file name -- pointerb.cpp
//  This program serves as a test driver to test the fucntions
//  in the class Poll. The Pool objects are instantiated by using
//  pointer variables.

//  ======================= header files ========================
#include "pool.h"                     // for class Pool
#include "utility.h"                  // for user defined functions
#include <iostream>                   // for cin & cout
using namespace std;


int main()

{
   Pool *one, *two;

   clearScreen();
   one = new Pool(10, 20, 5);
   cout << "The information about one is:"
        << endl;
   cout << "Length = "
        << one -> getLength()
        << endl
        << "Width = "
        << one -> getWidth()
        << endl
        << "Height = "
        << one -> getHeight()
        << endl;
   pauseScreen();

   two = one;
   cout << "The information about two is:"
        << endl;
   cout << "Length = "
        << (*two).getLength()
        << endl
        << "Width = "
        << (*two).getWidth()
        << endl
        << "Height = "
        << (*two).getHeight()
        << endl;
   pauseScreen();

   two -> changeSize(12.5, 13.5, 6);
   cout << "The information about two is:"
        << endl;
   cout << "Length = "
        << two -> getLength()
        << endl
        << "Width = "
        << two -> getWidth()
        << endl
        << "Height = "
        << two -> getHeight()
        << endl;
   pauseScreen();

   cout << "The information about one is:"
   << endl;
   cout << "Length = "
        << (*one).getLength()
        << endl
        << "Width = "
        << (*one).getWidth()
        << endl
        << "Height = "
        << (*one).getHeight()
        << endl;

   return 0;
}   // end of main
