//  file name -- pointer1.cpp
//  This program demonstrate the basic concepts of pointer variables.

//  ========================= header files ===========================
#include <iostream>                 // for cin & cout
using namespace std;


int main()
{
   int x, y;
   int *pointer1, *pointer2;        // pointer variables

   x = 20;
   y = 5;
   pointer1 = &y;                   // assign address of y to pointer1
   pointer2 = &x;                   // assign address of x to pointer2

   // check the content of the variables before operations
   cout << "x = "
        << x
        << endl;
   cout << "y = "
        << y
        << endl;
   cout << "The variable pointed to by pointer1 = "
        << *pointer1
        << endl;
   cout << "The variable pointed to by pointer2 = "
        << *pointer2
        << endl;

   x = *pointer1 - *pointer2;
   *pointer1 = x + y;

   // check the content of the variables after operations
   cout << endl
        << "x = "
        << x
        << endl;
   cout << "y = "
        << y
        << endl;
   cout << "The variable pointed to by pointer1 = "
        << *pointer1
        << endl;
   cout << "The variable pointed to by pointer2 = "
        << *pointer2
        << endl;

   // show the addresses of x and y and the content of pointer1
   // and pointer2

   cout << endl
        << "address of x is "
        << &x
        << endl
        << "address of y is "
        << &y
        << endl
        << "address stored in pointer1 is "
        << pointer1
        << endl
        << "address stored in pointer2 is "
        << pointer2
        << endl;

   return 0;
}   // end of main
