#include <iostream>
using namespace std;

int main()
{
int *ptr1, *ptr2, *ptr3, x, y;

ptr1 = &x;
ptr3 = new int;
x = 10;
y = 20;
*ptr3 = *ptr1 + y;
ptr2 = new int[5];
*ptr2 = *ptr3 - y;
ptr1 = ptr2;
ptr1 += 3;
ptr1[-1] = 5;
*ptr1 = *ptr3;
x = *ptr1 + *ptr2;
y = *ptr2 + 5;
cout << "x = " << x << endl;
cout << "y = " << y << endl;
cout << "*ptr1 = " << *ptr1 << endl;
cout << "*ptr2 = " << *ptr2 << endl;
cout << "*ptr3 = " << *ptr3 << endl;
cout << "ptr2[2] = " << ptr2[2] << endl;
   return 0;
}
