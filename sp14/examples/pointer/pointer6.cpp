//  file name -- pointer6.cpp
//  This program demonstrate the basic concepts of pointer variables
//  and dynamic memory allocation.

//  ======================= header files ========================
#include <iostream>                   // for cin & cout
using namespace std;


int main()
{
   double *width, *length;
   double area, perimeter;

   // allocate memory locations dynamically
   width = new double;
   length = new double;

   // read data
   cout << "Enter the length of the rectangle: ";
   cin >> *length;
   cout << "Enter the width of the rectangle: ";
   cin >> *width;

   // compute area and perimeter
   area = (*length) * (*width);
   perimeter = (*length + *width) * 2.0;

   // print result
   cout << endl
        << "The length of rectangle is "
        << *length
        << " and the width is "
        << *width
        << endl
        << "The area of the rectangle is "
        << area
        << " and its perimeter is "
        << perimeter
        << endl;

   return 0;
}   // end of main
