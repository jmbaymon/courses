#  Build an executable module named pointerc which will be rebuilt 
#  if either pointerc.o, utitlity.o, or pool.o has been updated.
#  The command
#
#     c++ pointerc.o pool.o -o pointerc
#
#  link pointerc.o, utility.o and pool.o to the math library to build
#  an executable (or load) module named pointerc.
#

pointerc: pointerc.o pool.o utility.o
	c++ pointerc.o pool.o utility.o  -o pointerc


#  Build an object file named pool.o which will be built
#  if pool.h or pool.cpp has been updated. The command
#
#     c++ -c pool.cpp
#
#  compile the source program pool.cpp and build an object
#  file named pool.o.
#

pool.o: pool.cpp pool.h
	c++ -c pool.cpp


#  Build an object file named pointerc.o which will be built
#  if pointerc.cpp pool.h or pool.cpp has been updated.
#  The command
#
#     c++ -c pointerc.cpp
#
#  compile the source program pointerc.cpp and build an object
#  file named pointerc.o.
#

pointerc.o: pointerc.cpp pool.h pool.cpp
	c++ -c pointerc.cpp


#  Build an object file named utility.o which will be built
#  if utility.cpp or utility.h has been updated.
#  The command
#
#     c++ -c utility.cpp
#
#  compile the source program utility.cpp and build an object
#  file named utility.o.
#

utility.o: utility.cpp utility.h
	c++ -c utility.cpp
