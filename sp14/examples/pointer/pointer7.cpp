//  file name -- pointer7.cpp
//  This program demonstrate the basic concepts of pointer variables
//  and dynamic allocation of an array.

//  ======================= header files ========================
#include <iostream>                   // for cin & cout
using namespace std;


//  ==================== symbolic constants =====================
const int MAX_ELEMENTS = 10;


int main()
{
   int *pointer1, *pointer2;          // pointer variables
   int i;

   pointer1 = new int[MAX_ELEMENTS];  // allocate an array of integers
   pointer2 = pointer1;               // save the base address

   // ********** assign values to array elements **********
   for (i = 0; i < MAX_ELEMENTS; i++)
   {
      *pointer1 = i * 2 + 1;
      pointer1++;
   }   // end for i

   // ********** print array forward **********
   cout << "array is printed forward" << endl;
   pointer1 = pointer2;
   for (i = 0; i < MAX_ELEMENTS; i++)
   {
      cout << "numbers["
           << i
           << "] = "
           << *pointer1
           << endl;
      pointer1 ++;                    // move to next element
   }   // end for i

   // ********** print array backward **********
   cout << endl
        << "array is printed backward"
        << endl;
   pointer1 --;                       // move back by one element
   for (i = MAX_ELEMENTS - 1; i >= 0; i--)
   {
      cout << "numbers["
           << i
           << "] = "
           << *pointer1
           << endl;
      pointer1 --;                    // move back by one element
   }   // end for i

   return 0;
}   // end of main
