// file name -- dmatrix.cpp
// this file implements a amtrix class which has a pointer
// variable to allocate a Matrix

#include <iostream>                 // for cin & cout
#include <iomanip>                  // for setw
#include "dmatrix.h"                // for Matrix class
using namespace std;


Matrix::Matrix(int new_rows,
               int new_columns)
//  allocate a one-dimensional array that will be used to represent
//  a two-dimensional array or a Matrix.
//  preconditions -- new_rows and new_columns are positive
//  postconditions -- storage for the Matrix is allocated and its
//                   dimension are set; all elements are set to zero.
{
  int i;

  row_ = new_rows;
  column_ = new_columns;
  storage_ = new float[new_rows * new_columns];
  for (i = 0; i < new_rows * new_columns; i++)
     storage_[i] = 0.0;
} //end Matrix::Matrix(int, int)


Matrix::~Matrix()
//  dellocate the memory location for the Matrix
//  preconditions -- the current Matrix is a well-defined Matrix
//  postconditions -- the storage allocated for the Matrix is
//                   deallocated.
{
   delete[] storage_;
} // end Matrix::~Matrix()


void Matrix::setElement(int row_index,
                        int column_index,
               	        float value)
// assign value to an element in the Matrix
// preconditions -- the row_index and the column_index are in the
//                  range of the current Matrix
// postconditions -- the value is assigned to the element in the
//                  Matrix with subscripts row_index and column_index.
{
   int index;

   index = row_index * column_ + column_index;
   storage_[index] = value;
}   // end Matrix::setElement

void Matrix::print()
//  print the content of the current Matrix in a Matrix form
//  preconditions -- the current Matrix is a well-defined Matrix.
//  postconditions -- the current Matrix is printed in a Matrix form.
{
   int i, j, index;

   cout.setf(ios::fixed, ios::floatfield);
   cout.setf(ios::showpoint);
   index = 0;
   for (i = 0; i < row_; i++)
   {
      for (j = 0; j < column_; j++)
      {
         cout << setw(8)
              << setprecision(2)
              << storage_[index];
         index++;
      }  // for j
      cout << endl;
   }  // for i
} // end Matrix::print


int Matrix::getRows()
// return the number of rows of the current Matrix
// preconditions -- the current Matrix is a well-defined Matrix
// postconditions -- the number of rows of the current Matrix is
//                  returned.
{
   return row_;
}   // end Matrix::getRows


int Matrix::getColumns()
// return the number of columns of the current Matrix
// preconditions -- the current Matrix is a well-defined Matrix
// postconditions -- the number of columns of the current Matrix is
//                  returned.
{
   return column_;
}   // end Matrix::getColumns
