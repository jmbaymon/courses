//  file name -- show-dmatrix2.cpp
//  This program shows the pitfall of shallow copy because there is no
//  implementation of the assignment operator to perform deep copy.

//  ======================== head files ==========================
#include <iostream>                // for cin & cout
#include "dmatrix1.h"              // for matrix class
#include "utility.h"               // for user-defined functions
using namespace std;


void print(Matrix whatever)
// shallow copy if no copy constructor is implemented.
{
   cout << "Matrix is "
        << whatever.getRows()
        << " by "
        << whatever.getColumns()
        << " and its contents are: "
        << endl;
   whatever.print();
}  // end of print

int main()
{
   Matrix one(3, 4);

   clearScreen();
   cout << "Matrix one: ";
   one.setElement(2, 1, -5.0);
   one.setElement(1, 3, -1.25);
   cout << "Matrix is "
        << one.getRows()
        << " by "
        << one.getColumns()
        << " and its contents are: "
        << endl;
   one.print();
   pauseScreen();

   Matrix two(6, 2);
   cout << "Matrix two: ";
   two.setElement(4, 1, 5.0);
   two.setElement(1, 0, 4.25);
   cout << "Matrix is "
        << two.getRows()
        << " by "
        << two.getColumns()
        << " and its contents are: "
        << endl;
   two.print();
   pauseScreen();


   // =================================================================
   // no implementation of Matrix::operator=(...)
   // shallow copy because only address is copied, not array.
   // memory leak when the link is lost.
   // =================================================================
   one = two;

   // =================================================================
   //  When the array stored in one is printed, it looks fine even
   //  though its original array is lost; memory leak will happen
   //  after calling the function print(Matrix).
   // =================================================================
   cout << endl
        << "After assigning two to one, one is "
        << one.getRows()
        << " by "
        << one.getColumns()
        << endl;
   print(one);
   pauseScreen();

   // =================================================================
   // Matrix object three is instantiated and its dynamic array will
   // be allocated using the same space freed in the previous step
   // after the function call -- print(one).
   // =================================================================
   Matrix three(2, 6);
   cout << "Matrix three: ";
   three.setElement(1, 1, 35.0);
   three.setElement(1, 3, -33.25);
   cout << "Matrix is "
        << three.getRows()
        << " by "
        << three.getColumns()
        << " and its contents are: "
        << endl;
   three.print();
   pauseScreen();


   // =================================================================
   // two's array got lost after calling print(one) -- shallow copy
   // what is shown below is not long the original array stored in two.
   // =================================================================
   cout << endl
        << "***the storage of two is destroyed and two "
        << two.getRows()
        << " by "
        << two.getColumns()
        << endl;
   two.print();

   return 0;
}   // end main
