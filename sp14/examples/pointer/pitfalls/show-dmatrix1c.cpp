//  file name -- show-dmatrix1c.cpp
//  This program shows a correct way to avoid shallow copy by passing
//  by reference.

//  ======================== head files ==========================
#include <iostream.h>              // for cin & cout
#include "dmatrix1.h"              // for matrix class
#include "utility.h"               // for user-defined functions


void print(Matrix &whatever)
{
   cout << "Matrix is "
        << whatever.getRows()
        << " by "
        << whatever.getColumns()
        << " and its contents are: "
        << endl;
   whatever.print();
}  // end of print

int main()
{
   Matrix one(3, 4);

   clearScreen();
   cout << "Matrix one: ";
   one.setElement(2, 1, -5.0);
   one.setElement(1, 3, -1.25);
   print(one);
   pause();

   Matrix two(6, 2);
   cout << "Matrix one: ";
   two.setElement(4, 1, 5.0);
   two.setElement(1, 0, 4.25);
   print(two);
   pause();

   cout << endl
        << "After calling function print twice matrix one is "
        << one.getRows()
        << " by "
        << one.getColumns()
        << endl;
   one.print();

   return 0;
}   // end main
