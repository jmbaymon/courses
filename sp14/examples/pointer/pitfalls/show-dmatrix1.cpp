//  file name -- show-dmatrix1.cpp
//  This program shows the pitfall of passing an object which contains
//  a dynamic array to a function by value. The pitfall results from
//  shallow copy because there is no copy constructor to perform deep
//  copy.

//  ======================== head files ==========================
#include <iostream>                // for cin & cout
#include "dmatrix1.h"              // for matrix class
#include "utility.h"               // for user-defined functions
using namespace std;


void print(Matrix whatever)
// shallow copy if no copy constructor is implemented.
{
   cout << "Matrix is "
        << whatever.getRows()
        << " by "
        << whatever.getColumns()
        << " and its contents are: "
        << endl;
   whatever.print();
}  // end of print

int main()
{
   Matrix one(3, 4);

   clearScreen();
   cout << "Matrix one: ";
   one.setElement(2, 1, -5.0);
   one.setElement(1, 3, -1.25);
   print(one);
   pauseScreen();

   Matrix two(6, 2);
   cout << "Matrix two: ";
   two.setElement(4, 1, 5.0);
   two.setElement(1, 0, 4.25);
   print(two);
   pauseScreen();

   cout << endl
        << "After calling function print twice matrix one is "
        << one.getRows()
        << " by "
        << one.getColumns()
        << endl;
   one.print();

   return 0;
}   // end main
