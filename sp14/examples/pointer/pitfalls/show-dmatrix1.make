#  Build an executable module named show-matrix which will be rebuilt
#  if either show-matrix.o, utility.o, or dmatrix1.o has been updated.
#  The command
#
#     c++ show-dmatrix1.o utility.o dmatrix1.o -o show-dmatrix1
#
#  links show-dmatrix1.o, utility.o and dmatrix1.o to build an
#   executable (or load) module named show-dmatrix1.
#
show-dmatrix1: show-dmatrix1.o dmatrix1.o utility.o
	c++ show-dmatrix1.o utility.o dmatrix1.o -o show-dmatrix1

#  Build an object file named show-dmatrix1.o which will be rebuilt
#  if show-dmatrix1.cpp, dmatrix1.h or dmatrix1.cpp has been updated.
#  The command
#
#     c++ -c show-dmatrix1.cpp
#
#  compiles the source program show-dmatrix1.cpp and builds an object
#  file named show-dmatrix1.o.
#
show-dmatrix1.o: show-dmatrix1.cpp dmatrix1.h dmatrix1.cpp
	c++ -c show-dmatrix1.cpp

#  Build an object file named dmatrix1.o which will be rebuilt
#  if dmatrix1.h or dmatrix1.cpp has been updated. The command
#
#     c++ -c dmatrix1.cpp
#
#  compiles the source program dmatrix1.cpp and builds an object
#  file named dmatrix1.o.
#
dmatrix1.o: dmatrix1.cpp dmatrix1.h
	c++ -c dmatrix1.cpp

#  Build an object file named utility.o which will be rebuilt
#  if utility.h or utility.cpp has been updated. The command
#
#     c++ -c utility.cpp
#
#  compiles the source program utility.cpp and builds an object
#  file named utility.o.
#
utility.o: utility.cpp utility.h
	c++ -c utility.cpp
