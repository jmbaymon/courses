#  Build a load module named show-dmatrix1c which will be rebuilt
#  if either show-dmatrix.o, utility.o, or dmatrix2.o has been updated.
#  The command
#
#     CC show-dmatrix1c.o utility.o dmatrix1.o -o show-dmatrix1c
#
#  links show-dmatrix1c.o, utility.o and dmatrix1.o to build an
#   executable (or load) module named show-dmatrix1c.
#
show-dmatrix1c: show-dmatrix1c.o dmatrix1.o utility.o
	CC show-dmatrix1c.o utility.o dmatrix1.o -o show-dmatrix1c

#  Build an object file named show-dmatrix1c.o which will be rebuilt
#  if show-dmatrix1c.cpp, dmatrix1.h or dmatrix1.cpp has been updated.
#  The command
#
#     CC -c show-dmatrix1c.cpp
#
#  compiles the source program show-dmatrix1c.cpp and builds an object
#  file named show-dmatrix1c.o.
#
show-dmatrix1c.o: show-dmatrix1c.cpp dmatrix1.h dmatrix1.cpp
	CC -c show-dmatrix1c.cpp

#  Build an object file named dmatrix1.o which will be rebuilt
#  if dmatrix1.h or dmatrix1.cpp has been updated. The command
#
#     CC -c dmatrix1.cpp
#
#  compiles the source program dmatrix1.cpp and builds an object
#  file named dmatrix1.o.
#
dmatrix1.o: dmatrix1.cpp dmatrix1.h
	CC -c dmatrix1.cpp

#  Build an object file named utility.o which will be rebuilt
#  if utility.h or utility.cpp has been updated. The command
#
#     CC -c utility.cpp
#
#  compiles the source program utility.cpp and builds an object
#  file named utility.o.
#
utility.o: utility.cpp utility.h
	CC -c utility.cpp
