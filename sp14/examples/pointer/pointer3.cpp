//  file name -- pointer3.cpp
//  This program demonstrate the basic concepts of pointer variables.

//  ======================= header files ========================
#include <iostream>                   // for cin & cout
using namespace std;


//  ==================== symbolic constants =====================
const int MAX_ELEMENTS = 10;


//  =================== data type definitions ===================
typedef int IntArray[MAX_ELEMENTS];

int main()
{
   IntArray numbers;
   int *pointer1, *pointer2;          // pointer variables
   int i;

   for (i = 0; i < MAX_ELEMENTS; i++)
      numbers[i] = i * 2 + 1;

   //  *************** print array forward ***************
   cout << "array is printed forward" << endl;
   for (i = 0; i < MAX_ELEMENTS; i++)
   {
      pointer1 = &numbers[i];
      cout << "numbers["
           << i
           << "] = "
           << *pointer1
           << endl;
   }   // end for i

   // *************** print array backward ***************
   cout << endl
        << "array is printed backward"
        << endl;
   for (i = MAX_ELEMENTS - 1; i >= 0; i--)
   {
      pointer2 = &numbers[2];
      cout << "numbers["
           << i
           << "] = "
           << *pointer2
           << endl;
   }   // end for i

   return 0;
}   // end of main
