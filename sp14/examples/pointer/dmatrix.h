// file name -- dmatrix.h
// this header file declares a amtrix class which has a pointer
// variable to allocate a Matrix

#ifndef class_Matrix_
#define class_Matrix_

class Matrix
{
   private:
       float *storage_;           //a pointer used to allocate a Matrix
       int row_;                  // number of rows in a mtrix
       int column_;               // number of columns in Matrix

   public:
       Matrix(int, int);          // allocate a Matrix
       Matrix(const Matrix &);    // copy constructor
       Matrix(int, int, float);   // allocate a Matrix and initialize
       ~Matrix();                 // destructor

       void print();              // print the Matrix
       void setElement(int, int, float);  // set an element a new value
       int getRows();             // return the number of rows
       int getColumns();          // return the number of columns
}; //end class Matrix

#endif   // class_Matrix_
