//  file name -- pointer2.cpp
//  This program demonstrate the basic concepts of pointer variables.

#include <iostream>                   // for cin & cout
using namespace std;


int main()
{
   int x, y;
   int *pointer1, *pointer2;          // pointer variables

   x = 20;
   y = 5;
   pointer1 = &y;
   pointer2 = &x;

   // check to see if the content of pointers are the same
   cout << endl
        << "Initially, ";
   if (pointer1 == pointer2)
      cout << "both pointers store the same address";
   else
      cout << "different addresses are stored in the pointers";
   cout << endl;


   // check to see if the content of the variables pointed to by
   // the pointers are the same before operations
   cout << endl
        << "Before operations, ";
   if (*pointer1 == *pointer2)
      cout << "the variables pointed to by the pointers are the same";
   else
      cout << "the variables pointed to by the pointers are different";
   cout << endl;


   x = *pointer1 - *pointer2;
   *pointer1 = x + y - 5;
   // check to see if the content of the variables pointed to by
   // the pointers are the same after operations
   cout << endl
        << "After operations, ";
   if (*pointer1 == *pointer2)
      cout << "The variables pointed to by the pointers are the same";
   else
      cout << "The variables pointed to by the pointers are different";
   cout << endl;

   cout << endl
        << "x = "
        << x
        << endl;
   cout << "y = "
        << y
        << endl;
   cout << "The variable pointed to by pointer1 = "
        << *pointer1
        << endl;
   cout << "The variable pointed to by pointer2 = "
        << *pointer2
        << endl;

   return 0;
}   // end of main
