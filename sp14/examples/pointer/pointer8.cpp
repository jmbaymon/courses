//  file name -- pointer8.cpp
//  This program demonstrate the basic concepts of pointer variables
//  and dynamic memory allocation of a structure.

//  ======================= header files ========================
#include <iostream>                   // for cin & cout
#include <cstring>                    // for strcpy
using namespace std;


//  ==================== symbolic constants =====================
const int MAX_HITS = 7;
const int MAX_LENGTH = 20;


//  =================== data type definitions ===================
typedef int IntArray[MAX_HITS];
typedef char String20[MAX_LENGTH];

struct Player
{
   int age;
   IntArray hits;
   String20 name;
};   // end Player


//  ==================== function prototypes ====================
void printRecord(Player *);


int main()
{
   Player *who;                // pointer variables

   // ********** initialization **********
   who = new Player;           // allocate a structure
   cout << endl
        << "Original record -- "
        << endl;
   strcpy(who->name, "David");
   who->age = 29;
   who->hits[0] = 2;
   who->hits[1] = 1;
   who->hits[2] = 0;
   who->hits[3] = 3;
   who->hits[4] = 2;
   who->hits[5] = 1;
   who->hits[6] = 3;
   printRecord(who);

   // ********** change structure using pointer variable **********
   cout << endl
        << "Record after changes are made --"
        << endl;
   who -> hits[2] = 2;
   who -> hits[6] = 0;
   (*who).age = 34;
   printRecord(who);

   return 0;
}   // end of main


void printRecord(Player *a)
//  This function displays the content of a player using a
//  pointer variable
{
   int i;

   cout << "Information about a baseball player:" << endl;
   cout << "name: " << a->name << endl;
   cout << "age: " << a->age << endl;
   cout << "Hits: ";
   for (i = 0; i < MAX_HITS; i++)
      cout << a->hits[i] << " ";
   cout << endl;
}   // end printRecord
