//  file name -- pointer5.cpp
//  This program demonstrate the basic concepts of pointer variables
//  and dynamic memory allocation.

//  ======================= header files ========================
#include <iostream>                 // for cin & cout
using namespace std;


int main()
{
   int x, y;
   int *pointer1, *pointer2;        // pointer variables

   x = 20;
   y = 5;
   pointer1 = &y;                   // assign address of y to pointer1
   pointer2 = new int;              // allocate a memory location for
                                    // an integer variable and assign
                                    // its address to pointer2

   // check the content of the variables before operations
   cout << "x = "
	     << x
	     << endl;
   cout << "y = "
	     << y
	     << endl;
   cout << "The variable pointed to by pointer1 = "
	     << *pointer1
	     << endl;
   cout << "The variable pointed to by pointer2 = "
	     << *pointer2
	     << endl;

   x = *pointer1 - y;
   *pointer2 = x + y;

   // check the content of the variables after operations
   cout << endl
	     << "x = "
	     << x
	     << endl;
   cout << "y = "
	     << y
	     << endl;
   cout << "The variable pointed to by pointer1 = "
	     << *pointer1
	     << endl;
   cout << "The variable pointed to by pointer2 = "
	     << *pointer2
	     << endl;

   cout << endl
        << "address of x = "
        << &x
        << endl;
   cout << "address of y = "
        << &y
        << endl;
   cout << "The address of the variable pointed to by pointer1 = "
        << pointer1
        << endl;
   cout << "The address of the variable pointed to by pointer2 = "
        << pointer2
        << endl;

   return 0;
}   // end of main
