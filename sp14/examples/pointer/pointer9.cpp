//  file name -- pointer9.cpp
//  This program demonstrate the basic concepts of pointer variables
//  and dynamic memory allocation of a structure.

//  ======================= header files ========================
#include <iostream>                   // for cin & cout
#include <cstring>                    // for strcpy
using namespace std;


//  ==================== symbolic constants =====================
const int MAX_HITS = 7;
const int MAX_LENGTH = 20;


//  =================== data type definitions ===================
typedef int IntArray[MAX_HITS];
typedef char String20[MAX_LENGTH];

struct Player
{
   int age;
   IntArray hits;
   String20 name;
};   // end Player


//  ==================== function prototypes ====================
void initialization(Player );
void change(Player *);
void printRecord(Player *);

int main()
{
   Player *who;                // pointer variables

   who = new Player;           // allocate a structure

   initialization(*who);

   cout << endl
        << "Record after calling initialization -- "
        << endl;
   printRecord(who);

   change(who);

   cout << endl
        << "Record after changes are made --"
        << endl;
   printRecord(who);

   return 0;
}   // end of main


void initialization(Player who)
{
   strcpy(who.name, "David");
   who.age = 29;
   who.hits[0] = 2;
   who.hits[1] = 1;
   who.hits[2] = 0;
   who.hits[3] = 3;
   who.hits[4] = 2;
   who.hits[5] = 1;
   who.hits[6] = 3;
   cout << "The record changed within initialization is:" << endl;
   printRecord(&who);
}   // end initialization


void change(Player *who)
{
   who->hits[2] = 2;
   who->hits[6] = 0;
   who->age = 34;
}   // end change


void printRecord(Player *a)
{
   int i;

   cout << "Information about a baseball player:" << endl;
   cout << "name: " << a->name << endl;
   cout << "age: " << a->age << endl;
   cout << "Hits: ";
   for (i = 0; i < MAX_HITS; i++)
      cout << a->hits[i] << " ";
   cout << endl;
}   // end printRecord
