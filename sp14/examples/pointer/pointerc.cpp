//  file name -- pointerc.cpp
//  This program serves as a test driver to test the fucntions
//  in the class Poll. The Pool objects are instantiated by using
//  pointer variables.

//  ======================= header files ========================
#include "pool.h"                     // for class Pool
#include "utility.h"                  // for user defined functions
#include <iostream>                   // for cin & cout
using namespace std;

//  ======================= header files ========================
void displayAPool(Pool);// expect pointee
void displayAPool(Pool*);// expect an address


int main()

{
   Pool *one, *two;

   clearScreen();
   one = new Pool(10, 20, 5);
   cout << "The information about one is:"
        << endl;
   displayAPool(*one);
   pauseScreen();

   two = one;
   cout << "The information about two is:"
        << endl;
   displayAPool(*two);
   pauseScreen();

   two -> changeSize(12.5, 13.5, 6);
   cout << "The information about two is:"
        << endl;
   displayAPool(two);
   pauseScreen();

   cout << "The information about one is:"
        << endl;
   displayAPool(one);

   return 0;
}   // end of main


void displayAPool(Pool whatever)
// purpose: display the content of a pool
// preconditions: whatever is a well-defined pool object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "object passed is  call -- Lenght of the pool is "
        << whatever.getLength()
        << endl
        << "Width of the pool is "
        << whatever.getWidth()
        << endl
        << "Height of the pool is "
        << whatever.getHeight()
        << endl
        << "Volume of the pool is "
        << whatever.getVolume()
        << endl;
}  // end of displayAPool


void displayAPool(Pool* whatever)
// purpose: display the content of a pool
// preconditions: whatever is a well-defined pool object
// postconditions: the content of whatever is displayed on the screen.
{
   cout << "Address passed is call --Lenght of the pool is "
        << (*whatever).getLength()
        << endl
        << "Width of the pool is "
        << whatever -> getWidth()
        << endl
        << "Height of the pool is "
        << whatever -> getHeight()
        << endl
        << "Volume of the pool is "
        << (*whatever).getVolume()
        << endl;
}  // end of displayAPool
