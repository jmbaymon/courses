#  Build an executable module named pointerf which will be rebuilt 
#  if either pointerf.o, utitlity.o, or pool.o has been updated.
#  The command
#
#     c++ pointerf.o pool.o -o pointerf
#
#  link pointerf.o, utility.o and pool.o to the math library to build
#  an executable (or load) module named pointerf.
#

pointerf: pointerf.o pool.o utility.o
	c++ pointerf.o pool.o utility.o  -o pointerf


#  Build an object file named pool.o which will be built
#  if pool.h or pool.cpp has been updated. The command
#
#     c++ -c pool.cpp
#
#  compile the source program pool.cpp and build an object
#  file named pool.o.
#

pool.o: pool.cpp pool.h
	c++ -c pool.cpp


#  Build an object file named pointerf.o which will be built
#  if pointerf.cpp pool.h or pool.cpp has been updated.
#  The command
#
#     c++ -c pointerf.cpp
#
#  compile the source program pointerf.cpp and build an object
#  file named pointerf.o.
#

pointerf.o: pointerf.cpp pool.h pool.cpp
	c++ -c pointerf.cpp


#  Build an object file named utility.o which will be built
#  if utility.cpp or utility.h has been updated.
#  The command
#
#     c++ -c utility.cpp
#
#  compile the source program utility.cpp and build an object
#  file named utility.o.
#

utility.o: utility.cpp utility.h
	c++ -c utility.cpp
