//  file name -- hanoi.cpp
//  This program demonstrates how to solve the hanoi tower problem
//  using recursion.

//  ========================= header files =========================
#include <iostream>                  // for console I/O
using namespace std;


//  ========================= function prototypes ==================
void hanoi(int, char, char, char);


int main()
{
   int number_of_disks;

   cout << "Enter the number of disks in the Hanoi tower: ";
   cin >> number_of_disks;

   hanoi(number_of_disks, 'A', 'C', 'B');

   return 0;
}  // end of main


// The solution to the Tower of Hanoi is based on the following
// recursive relationships

// Move n disks from source to destination can be solved by
//   1. Move the first n-1 disks from source to intermediate
//   2. Move nth disk from source to destination
//   3. Move the first n-1 disks from intermediate to destination

// Special case to termincate the recursive solution is when there
// is only one disk left: Move from source to destination without
// going through the intermediate.

void hanoi(int n,
           char source,
           char destination,
           char intermediate)
{
   if (n == 1)
      cout << "Move disk from "
           << source
           << " to "
           << destination
           << endl;
   else
   {
      hanoi(n-1, source, intermediate, destination);
      cout << "Move disk from "
           << source
           << " to "
           << destination
           << endl;
      hanoi(n-1, intermediate, destination, source);
   }  // else
}  // end of hanoi
