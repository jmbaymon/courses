//  file name -- fibonacci1.cpp
//  This program demonstrates how to generate s number from
//  Fibonacci sequence of order 2.

//  Fibonacci sequence of order 2 is defined as follows

//    1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
//
//    F(0) = 1
//    F(1) = 1
//    F(n) = F(n - 1) + F(n - 2)


//  ======================== header files ========================
#include <iostream>                    // for console I/O
#include <iomanip>                     // for output format
using namespace std;                   // for standard library

int Fibonacci(int);

int main()
{
   int n;

   cout << "Enter an integer value: ";
   cin >> n;

   cout << "F("
        << n
        << ") in the Fabonacci sequence is "
        << Fibonacci(n)
        << endl;


   return 0;
} // end of main


int Fibonacci(int n)
{
   static int count = 0;

   count ++;
   cout << "count is "
        << count
        << " and n is "
        << n
        << endl;

   if (n <= 1)
      return 1;
   else
      return Fibonacci(n - 1) + Fibonacci(n - 2);
} // end of Fibonacci
