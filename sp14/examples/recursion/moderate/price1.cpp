//  file name -- price1.cpp
//  This program demonstrates how to guess a number with a
//  given set of bounds.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
#include <iomanip>                     // for output format
using namespace std;                   // for standard library

int priceIsRight(int, int, int);

int main()
{
   int lower_bound, upper_bound, price;

   cout << "Enter the lower bound of the price: ";
   cin >> lower_bound;
   cout << "Enter the upper bound of the price: ";
   cin >> upper_bound;
   cout << "Enter the price in the range: ";
   cin >> price;

   cout << "The price "
        << price
        << " in the range bwteen ["
        << lower_bound
        << ", "
        << upper_bound
        << "] is found in "
        << priceIsRight(price, lower_bound, upper_bound)
        << " guesses."
        << endl;


   return 0;
} // end of main


int priceIsRight(int price,
                 int lower_bound,
                 int upper_bound)
{
   static int count = 0;
   int guessed;

   count ++;
   guessed = (lower_bound + upper_bound) / 2;
   cout << "Guessed price is "
        << guessed
        << endl;
   if (guessed == price)
      return count;
   else if (guessed < price)
      return priceIsRight(price, guessed + 1, upper_bound);
   else
      return priceIsRight(price, lower_bound, guessed - 1);
} // end of priceIsRight
