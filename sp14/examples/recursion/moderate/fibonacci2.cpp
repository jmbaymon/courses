//  file name -- fibonacci2.cpp
//  This program demonstrates how to generate s number from
//  Fibonacci sequence of order 2.

//  Fibonacci sequence of order 2 is defined as follows

//    1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
//
//    F(0) = 1
//    F(1) = 1
//    F(n) = F(n - 1) + F(n - 2)


//  ======================== header files ========================
#include <iostream>                    // for console I/O
#include <iomanip>                     // for output format
using namespace std;                   // for standard library


//  ====================== named constants =======================
const int WIDTH1 = 5;

int Fibonacci(int);

int main()
{
   int n;

   cout << "Enter an integer value: ";
   cin >> n;

   cout << endl;
   for (int i = 1; i <= n; i++)
   {
      cout << setw(WIDTH1)
           << i
           << setw(WIDTH1)
           << Fibonacci(i)
           << endl;
   } // for i

   return 0;
} // end of main


int Fibonacci(int n)
{
   if (n <= 1)
      return 1;
   else
      return Fibonacci(n - 1) + Fibonacci(n - 2);
} // end of Fibonacci
