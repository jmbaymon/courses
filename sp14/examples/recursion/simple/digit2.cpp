//  file name -- digit2.cpp
//  This program demonstrates how function calls work.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
using namespace std;                   // for standard library

void displayDigit1(int);
void displayDigit2(int);
void displayDigit3(int);
void displayDigit4(int);

int main()
{
   cout << "********************************* display 8"
        << endl;
   displayDigit1(8);
   cout << "********************************* display 74"
        << endl;
   displayDigit1(74);
   cout << "********************************* display 129"
        << endl;
   displayDigit1(129);
   cout << "********************************* display 3129"
        << endl;
   displayDigit1(3129);

   return 0;
} // end of main


void displayDigit1(int number)
{
   cout << "dispalyDigit1 is called"
        << endl;
   if (number > 0)
   {
      displayDigit2(number / 10);
      cout << number % 10
           << endl;
   }
} // end of displayDigit1


void displayDigit2(int number)
{
   cout << "dispalyDigit2 is called"
        << endl;
   if (number > 0)
   {
      displayDigit3(number / 10);
      cout << number % 10
           << endl;
   }
} // end of displayDigit2


void displayDigit3(int number)
{
   cout << "dispalyDigit3 is called"
        << endl;
   if (number > 0)
   {
      displayDigit4(number / 10);
      cout << number % 10
           << endl;
   }
} // end of displayDigit3


void displayDigit4(int number)
{
   cout << "dispalyDigit4 is called"
        << endl;
   if (number > 0)
   {
      displayDigit1(number / 10);
      cout << number % 10
           << endl;
   }
} // end of displayDigit4
