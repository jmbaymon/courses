//  file name -- message2.cpp
//  This program demonstrates why a recursive function call may
//  cause problems if the function does not have at least one
//  way to terminate the recursive call.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
using namespace std;                   // for standard library

void displayMessage();

int main()
{
   displayMessage();

   return 0;
} // end of main


void displayMessage()
{
   int big[10000];
   static int count = 0;

   count++;
   cout << "This function call will be terminated faster! "
        << count
        << endl;
   displayMessage();
} // end of displayMessage
