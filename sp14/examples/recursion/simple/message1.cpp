//  file name -- message1.cpp
//  This program demonstrates why a recursive function call may
//  cause problems if the function does not have at least one
//  way to terminate the recursive call.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
using namespace std;                   // for standard library

void displayMessage();

int main()
{
   displayMessage();

   return 0;
} // end of main


void displayMessage()
{
   static int count = 0;

   count++;
   cout << "See you tomorrow! "
        << count
        << endl;
   displayMessage();
} // end of displayMessage
