//  file name -- message3.cpp
//  This program demonstrates how a recursive function works.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
using namespace std;                   // for standard library

void displayMessage(int);

int main()
{
   displayMessage(10);

   return 0;
} // end of main


void displayMessage(int n)
{
   if (n == 0)
      cout << "It is time to stop!"
           << endl;
   else
   {
      cout << "This function call will be terminated faster! "
           << n
           << endl;
      displayMessage(n - 1);
   }
} // end of displayMessage
