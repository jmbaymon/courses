//  file name -- division3.cpp
//  This program demonstrates how a value returning function is
//  called recursively to produce an answer.

//  The function is used to solve the following division

//                            1
//    1 + ------------------------------------------
//                              1
//         1 + -------------------------------------
//                                1
//              1 + --------------------------------
//                                  1
//                   1 + ---------------------------
//                                    1
//                        1 + ----------------------
//                                   ...

//  ======================== header files ========================
#include <iostream>                    // for console I/O
#include <iomanip>                     // for output format
using namespace std;                   // for standard library

//  ====================== named constants =======================
const int DECIMALS = 13;
const int WIDTH1 = 5;
const int WIDTH2 = 20;

double calculate(int);

int main()
{
   int n;

   cout << "Enter an integer value: ";
   cin >> n;

   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout << endl;
   for (int i = 1; i <= n; i++)
   {
      cout << setw(WIDTH1)
           << i
           << setw(WIDTH2)
           << setprecision(13)
           << calculate(i)
           << endl;
   } // for i

   return 0;
} // end of main


double calculate(int n)
{
   if (n <= 0)
      return 1;
   else
      return 1 + 1 / calculate(n - 1);
} // end of calculate
