//  file name -- message6.cpp
//  This program demonstrates how a recursive function works.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
using namespace std;                   // for standard library

void displayMessage(int);

int main()
{
   int n;

   cout << "Enter an integer to start the recursive call: ";
   cin >> n;
   displayMessage(n);

   return 0;
} // end of main


void displayMessage(int n)
{
   if (n <= 0)
      cout << "It is time to stop!"
           << endl;
   else
   {
      cout << "Before recursive call! "
           << n
           << endl;
      displayMessage(n - 1);
      cout << "After recursive call! "
           << n
           << endl;
   }
} // end of displayMessage
