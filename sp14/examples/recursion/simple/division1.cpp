//  file name -- division1.cpp
//  This program demonstrates how a value returning function is
//  called recursively to produce an answer.

//  The function is used to solve the following division

//                            1
//    1 + ------------------------------------------
//                              1
//         1 + -------------------------------------
//                                1
//              1 + --------------------------------
//                                  1
//                   1 + ---------------------------
//                                    1
//                        1 + ----------------------
//                                   ...

//  ======================== header files ========================
#include <iostream>                    // for console I/O
#include <iomanip>                     // for output format
using namespace std;                   // for standard library

//  ====================== named constants =======================
const int DECIMALS = 13;

double calculate(int);

int main()
{
   int n;

   cout << "Enter an integer value: ";
   cin >> n;

   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout << "The result is "
        << setprecision(13)
        << calculate(n)
        << endl;


   return 0;
} // end of main


double calculate(int n)
{
   if (n <= 0)
      return 1;
   else
      return 1 + 1 / calculate(n - 1);
} // end of calculate
