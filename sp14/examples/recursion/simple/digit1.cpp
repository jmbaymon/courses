//  file name -- digit1.cpp
//  This program demonstrates how function calls work.

//  ======================== header files ========================
#include <iostream>                    // for console I/O
using namespace std;                   // for standard library

void displayDigit1(int);
void displayDigit2(int);
void displayDigit3(int);
void displayDigit4(int);

int main()
{
   cout << "************************** display 8"
        << endl;
   displayDigit1(8);
   cout << "************************** display 74"
        << endl;
   displayDigit1(74);
   cout << "************************** display 129"
        << endl;
   displayDigit1(129);
   cout << "************************** display 3129"
        << endl;
   displayDigit1(3129);

   return 0;
} // end of main


void displayDigit1(int number)
{
   cout << "displayDigit1 is called"
        << endl;
   if (number > 0)
   {
      cout << number % 10
           << endl;
      displayDigit2(number / 10);
   }
} // end of displayDigit1


void displayDigit2(int number)
{
   cout << "displayDigit2 is called"
        << endl;
   if (number > 0)
   {
      cout << number % 10
           << endl;
      displayDigit3(number / 10);
   }
} // end of displayDigit2


void displayDigit3(int number)
{
   cout << "displayDigit3 is called"
        << endl;
   if (number > 0)
   {
      cout << number % 10
           << endl;
      displayDigit4(number / 10);
   }
} // end of displayDigit3


void displayDigit4(int number)
{
   cout << "displayDigit4 is called"
        << endl;
   if (number > 0)
   {
      cout << number % 10
           << endl;
      displayDigit1(number / 10);
   }
} // end of displayDigit4
