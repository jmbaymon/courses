//  file name -- address02.cpp
//  This program shows the addresses of a one-dimensional array in C++.

//  ======================== header files =========================
#include <iostream>                   // for console I/O
using namespace std;


//  ===================== function prototypes =====================


int main()
{
   int one[10], index, k;

   index = 0;
   for (k = 1; k <= 10; k++)
   {
      cout << "address of one["
           << index
           << "] is "
           << &one[index]
           << endl;
      index++;
   }  // for k

   cout << "What one represents is "
        << one
        << endl;

   return 0;
}   // end of function main
