//  file name -- address01.cpp
//  This program shows the difference between reference parameters and
//  value parameters in terms of address.

//  ======================== header files =========================
#include <iostream>                  // for console I/O
using namespace std;


//  ===================== function prototypes =====================
void change(int &, int );


int main()
{
   int one, two;

   cout << "The address of one is "
        << &one
        << endl
        << "The address of two is "
        << &two
        << endl;
   change(int(pow(one,2)), two);

   return 0;
}   // end of function main


void change(int &first,
            int second)
{
   cout << "The address of first is "
        << &first
        << endl
        << "The address of second is "
        << &second
        << endl;
}  // end of change
