//  file name -- address03.cpp
//  This program shows the addresses of a two-dimensional array in C++.

//  ======================== header files =========================
#include <iostream>                         // for console I/O
using namespace std;


//  ===================== function prototypes =====================


int main()
{
   int one[3][4], row_index, column_index, k, m;

   row_index = 0;
   for (k = 1; k <= 3; k++)
   {
      column_index = 0;
      for (m = 1; m <= 4; m++)
      {
         cout << "address of one["
              << row_index
              << "]["
              << column_index
              << "] is "
                 << &one[row_index][column_index]
              << endl;
         column_index++;
      } // for m
      row_index++;
   }  // for k

   return 0;
}   // end of function main
