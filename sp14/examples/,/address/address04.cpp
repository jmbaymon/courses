//  file name -- address04.cpp
//  This program shows why a two-dimensional array cannot be allocated
//  dynamically in C++;

//  ======================== header files =========================
#include <iostream>                      // for console I/O
using namespace std;


//  ===================== function prototypes =====================


int main()
{
   int *ebony;
   int rows, columns;

   cout << "Enter the number of rows of the matrix: ";
   cin >> rows;
   cout << "Enter the number of columns of the matrix: ";
   cin >> columns;

   ebony = new int[rows][columns];

   return 0;
}   // end of function main
