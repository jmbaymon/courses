//  file name -- upper-offset.cpp
//  This program shows how to find the offset of an upper case letter
//  from the base 'A'.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <iomanip>
using namespace std;


int main()
{
   char x;
   int offset;

   for (x = 'A'; x <= 'Z'; x++)
   {
      offset = x - 'A';
      cout << x
           << setw(8)
           << offset
           << endl;
   } // for all upper case letters

   return 0;
}  // end of main
