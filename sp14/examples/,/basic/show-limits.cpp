//  file name -- show-limits.cpp
//  This program shows the limits of different data types supported by
//  the compiler. These limits may be different from those printed in
//  the text.

//  ========================= header files ==========================
#include <iostream>             // for console I/O
#include <climits>              // for limits of integral data types
#include <cfloat>               // for limits of floating point values
#include "utility.h"
using namespace std;


int main()
{
   int value;

   // display maximum and minimum char values
   clearScreen();
   cout << "Maximum char value is "
        << CHAR_MAX
        << endl
        << "Minimum char value is "
        << CHAR_MIN
        << endl;

   // display maximum and minimum short values
   cout << "Maximum short value is "
        << SHRT_MAX
        << endl
        << "Minimum short value is "
        << SHRT_MIN
        << endl;

   // display maximum and minimum int values
   cout << "Maximum int value is "
        << INT_MAX
        << endl
        << "Minimum int value is "
        << INT_MIN
        << endl;

   // display maximum and minimum long values
   cout << "Maximum long value is "
        << LONG_MAX
        << endl
        << "Minimum long value is "
        << LONG_MIN
        << endl;
   pauseScreen();

   // display maximum unsign values
   clearScreen();
   cout << "Maximum unsignedchar value is "
        << UCHAR_MAX
        << endl
        << "Maximum unsignedshort value is "
        << USHRT_MAX
        << endl
        << "Maximum unsignedint value is "
        << UINT_MAX
        << endl
        << "Maximum unsignedlong value is "
        << ULONG_MAX
        << endl;
   pauseScreen();

   // display maximum and minimum floating point value
   clearScreen();
   cout << "Maximum floating point value is "
        << FLT_MAX
        << endl
        << "Minimum floating point value is "
        << FLT_MIN
        << endl
        << "Maximum double value is "
        << DBL_MAX
        << endl
        << "Minimum double value is "
        << DBL_MIN
        << endl
        << "Maximum long double value is "
        << LDBL_MAX
        << endl
        << "Minimum long double value is "
        << LDBL_MIN
        << endl;


   return 0;
}  // end of main
