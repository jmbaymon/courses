//  file name -- ascii.cpp
//  This program shows how to display the ASCII value of a char type
//  variable.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
using namespace std;


int main()
{
   char x;
   int value;

   cout << "Enter a character: ";
   cin >> x;
   value = x;
   cout << "The ASCII value of "
        << x
        << " is "
        << int(x)
        << endl;

   return 0;
}  // end of main
