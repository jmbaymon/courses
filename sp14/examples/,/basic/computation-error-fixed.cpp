//  file name -- computation-error-fixed.cpp
//  This program shows how to fix a precision problem with float type.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cmath>
using namespace std;


int main()
{
   float increment, sum;

   increment = 1.0 / 100.0;
   sum = 0.0;
   while (fabs(sum - 1.0) > 0.00001)
   {
      sum += increment;
      cout << sum
           << endl;
   } // while sum is not 1 yet

   return 0;
}  // end of main
