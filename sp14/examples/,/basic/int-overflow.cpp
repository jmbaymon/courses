//  file name -- int-overflow.cpp
//  This program shows overflow problem with int type.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
using namespace std;


int main()
{
   int product;
   int k;

   product = 1;
   for (k = 1; k <= 100; k++)
   {
      product *= k;
      cout << k
           << "! = "
           << product
           << endl;
   } // for k is not greater than 100

   return 0;
}  // end of main
