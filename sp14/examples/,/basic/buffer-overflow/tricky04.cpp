//  file name -- tricky04.cpp
//  This program shows problems with C-style strings.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include <stdio.h>
using namespace std;


//  ===================== symbolic constants ====================
const int MAX_NAME = 21;


//  ================== user defined data types ==================
typedef char NameString[MAX_NAME];
struct Student
{
   NameString name;
   int age;
   float gpa;
}; // struct Student


int main()
{
   Student who;
   char *whatever;
   int index;
   long result;
   long factor;

   // save the address of the age member of who to whatever
   whatever = (char *) &who.age;

   cout << "Address of name is "
	<< &who.name
        << endl
	<< "Address of age is "
	<< &who.age
	<< endl
	<< "Address of gpa is "
	<< &who.gpa
	<< endl;

   cout << "Size of name is "
	<< sizeof(who.name)
        << endl
	<< "Size of age is "
	<< sizeof(who.age)
	<< endl
	<< "Size of gpa is "
	<< sizeof(who.gpa)
	<< endl;
   
   strcpy(who.name, "James Bond");
   who.age = 25;
   who.gpa = 3.75;
   cout << "Name is "
	<< who.name
	<< endl
	<< "age = "
	<< who.age
	<< endl
	<< "gpa = "
	<< who.gpa
	<< endl;

   // the string copied to who.name is longer than the size of
   // the array allocated; therefore, it overlfows to next variable
   // which is who.age
   strcpy(who.name, "My name is Bond, James Bond");
   cout << "Name is "
	<< who.name
	<< endl
	<< "age = "
	<< who.age
	<< endl
	<< "gpa = "
	<< who.gpa
	<< endl;

   // display the content of who.age as a string in C language
   printf("The content of age is %s\n", whatever);

   result = 0;
   factor = 1;
   for (index = 0; index <= 3; index++)
   {
      result += whatever[index] * factor;
      factor = 256 * factor;
      printf("%d\n", whatever[index]);
   }  // for index

   cout << "The value of result is "
	<< result
	<< endl;

   return 0;
}  // end of main
