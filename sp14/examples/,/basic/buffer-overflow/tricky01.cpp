//  file name -- tricky01.cpp
//  This program shows problems with C-style strings.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include <stdio.h>
using namespace std;


//  ===================== symbolic constants ====================
const int MAX_NAME = 21;


//  ================== user defined data types ==================
typedef char NameString[MAX_NAME];
struct Student
{
   NameString name;
   int age;
   float gpa;
}; // struct Student


int main()
{
   Student who;

   strcpy(who.name, "James Bond");
   who.age = 25;
   who.gpa = 3.75;
   cout << "Name is "
   << who.name
   << endl
   << "age = "
   << who.age
   << endl
   << "gpa = "
   << who.gpa
   << endl;

   // the string copied to who.name is longer than the size of
   // the array allocated; therefore, it overlfows to next variable
   // which is who.age
   strcpy(who.name, "My name is Bond, James Bonds??");
   cout << "Name is "
   << who.name
   << endl
   << "age = "
   << who.age
   << endl
   << "gpa = "
   << who.gpa
   << endl;

   return 0;
}  // end of main
