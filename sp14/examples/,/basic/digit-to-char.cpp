//  file name -- digit-to-char.cpp
//  This program shows how to convert an int type digit to a digit
//  character.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
using namespace std;


int main()
{
   char x;
   int value;

   value = 5;
   x = value + '0';
   cout << (x == '4')
        << endl;

   return 0;
}  // end of main
