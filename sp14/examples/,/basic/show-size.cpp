//  file name -- show-size.cpp
//  This program shows the number of bytes used by each primitive
//  data type.

//  ========================= header files ==========================
#include <iostream>             // for console I/O
#include <stdlib.h>             // for system calls
#include "utility.h"
using namespace std;

//=============== data type declarations =================
struct Student
{int age;
   double gpa;
   int scores[3];
};

//  ====================== function prototypes ======================
void clearScreen();
void pauseScreen();


int main()
{
   // display number of bytes used by each data type
   clearScreen();
   cout << "Number of bytes used for char is "
        << sizeof(char)
        << endl
        << "Number of bytes used for bool is "
        << sizeof(bool)
        << endl
        << "Number of bytes used for short is "
        << sizeof(short)
        << endl
        << "Number of bytes used for int is "
        << sizeof(int)
        << endl
        << "Number of bytes used for long is "
        << sizeof(long)
        << endl
        << "Number of bytes used for float is "
        << sizeof(float)
        << endl
        << "Number of bytes used for double is "
        << sizeof(double)
        << endl
        << "Number of bytes used for long double is "
        << sizeof(long double)
        << endl
        << "Number of bytes used for Student Type is "
        << sizeof(Student)
        <<endl;

   return 0;
}  // end of main
