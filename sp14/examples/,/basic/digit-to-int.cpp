//  file name -- digit-to-int.cpp
//  This program shows how to convert an char type digit to an int
//  type digit.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
using namespace std;


int main()
{
   char x;
   int value;

   x = '9';
   value = x - '0';
   cout << (value + 12)
        << endl;

   return 0;
}  // end of main
