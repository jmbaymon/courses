//  file name -- float-underflow.cpp
//  This program shows underflow problem with float type.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
using namespace std;


int main()
{
   float product;
   int k;

   product = 1.0;
   for (k = 1; k <= 100; k++)
   {
      product *= 1.0 / k;
      cout << "1/"
           << k
           << "! = "
           << product
           << endl;
   } // for k is not greater than 100

   return 0;
}  // end of main
