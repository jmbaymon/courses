//  file name -- driver03.cpp
//  This program shows how a test driver is used to test the correctness
//  of a function. The function to be tested is named compareOdds which
//  has specifications as described in the function definition. This test
//  driver uses the steps below to perform tests:
//
//  1. Set up the required input data in the main function.
//  2. After data are read in from the keyboard, a function is called 
//     to validate the input.
//  3. Call the function being tested to obtain the result.
//  4. Display the result.
//
//
//  Since the function compareOdds retuns 3 possible values, this
//  function must be called at least three times to validate the
//  correctness of the function. That means the user has to use
//  three test cases to test the function:
//
//  Case 1: the array contains more odds than evens
//          1, -3, 0, 101, 78, 83
//  expect the function returns the value of 1
//          
//  Case 2: the array contains less odds than evens
//          2, -8, 0, 101, 78, 83
//  expect the function returns the value of -1
//          
//  Case 3: the array contain same odds as evens
//          1, -3, 0, 10, -78, 83
//  expect the function returns the value of 0
//
//
//  This time the test data are stored in a file named odds01.dat.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <fstream>                   // for file I/O
using namespace std;


//  ====================== named constants ======================
const int MAX_ELEMENTS = 6;
const int MAX_SETS = 3;


//  =================== function prototypes =====================
int compareOdds(int [], int);
void displayArray(int [], int);


int main()
{
   int numbers[MAX_ELEMENTS], result, count, index;
   ifstream infile;

   infile.open("odds01.dat");

   for (count = 1; count <= MAX_SETS; count++)
   {
      // read in data from the file
      for (index = 0; index < MAX_ELEMENTS; index++)
      {
         infile >> numbers[index];
      } // for elements in the array

      // validate input
      displayArray(numbers, MAX_ELEMENTS);


      // calling the function to be tested
      result = compareOdds(numbers, MAX_ELEMENTS);


      // display results after function call
      cout << endl
           << "************ Case "
           << count
           << ", and the comparison result is "
           << result 
           << endl;
   } // for all sets of test data

   return 0;
}  // end of main


// display all elements in an int array with subscripts
// precondition: size is positive and no larger than the size
//               of the array pass to the function
// postcondition: the contents of array elements are displayed
//               and their subscripts are shown as well
void displayArray(int values[],
                  int size)
{
   int index;

   for (index = 0; index < size; index++)
   {
      cout << "values ["
           << index
           << "] = "
           << values[index]
           << endl;
   } // for all elements in the array
} // end of displayArray


// compare number of odds and number of evens in the array
// precondition: size is positive and no larger than the size
//               of the array pass to the function
// postcondition: return the value of 1 if number of odds is
//               greater than number of evens; return the value of
//               0 if number of odds is same as number of evens;
//               otherwise the value of -1 is returned.
int compareOdds(int values[],
               int size)
{
   int index, odds, evens;


   odds = 0;
   evens = 0;
   for (index = 0; index < size; index++)
   {
      if (values[index] % 2 == 0)
         evens++;
      else
         odds++;
   } // for all elements in the array

   if (odds > evens)
      return 1;
   else if (odds == evens)
      return 0;
   else
      return -1;
} // end of compareOdds
