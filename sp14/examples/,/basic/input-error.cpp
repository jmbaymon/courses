// file name -- input-error.cpp
// This program demonstrates the possible input errors.


//  ========================= header files ========================
#include <iostream>                    // for console I/O
using namespace std;


int main()
{
   int value, number;

   cout << "Enter an integer value: ";
   cin >> value;
   cout << "The state of cin is "
        << cin
        << endl;

   cout << "Enter an integer value: ";
   cin >> number;
   cout << "The state of cin is "
        << cin.good()
        << endl;

   cout << "First integer entered is "
        << value
        << endl
        << "Second integer entered is "
        << number
        << endl;

   return 0;
} // end of main
