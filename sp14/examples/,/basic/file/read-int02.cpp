// file name -- read-int02.cpp

// This program shows how to set up a while loop to control reading
// data from a file until end of file is reached.

// ===================== header files =====================
#include <iostream>              // for console I/O
#include <fstream>               // for file I/O
using namespace std;


int main()
{
   ifstream infile;
   int number;

   infile.open("numbers.dat");
   infile >> number;
   while (infile)
   {
      cout << number << endl;
      cout << "The state of infile is "
           << infile
           << endl;
      infile >> number;
   } // while infile has valid data

   return 0;
} // end of main
