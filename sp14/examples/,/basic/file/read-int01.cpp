// file name -- read-int01.cpp

// This program shows the problems of not setting up the while loop
// correctly to read in numbers from a text file which contains
// integer values.

// ===================== header files =====================
#include <iostream>              // for console I/O
#include <fstream>               // for file I/O
using namespace std;


int main()
{
   ifstream infile;
   int number;

   infile.open("numbers.dat");
   while (infile)
   {
      infile >> number;
      cout << "The state of infile is "
           << infile
           << endl;
      cout << number << endl;
   } // while infile has valid data

   return 0;
} // end of main
