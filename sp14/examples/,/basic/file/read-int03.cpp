// file name -- read-int03.cpp

// This program shows another situation in which the file pointer state
// changes from good state to bad state. This occurs when the input
// data type does not match the data type of the variable.

// ===================== header files =====================
#include <iostream>              // for console I/O
#include <fstream>               // for file I/O
using namespace std;


int main()
{
   ifstream infile;
   int number;

   infile.open("values.dat");
   infile >> number;
   while (infile)
   {
      cout << number << endl;
      cout << "The state of infile is "
           << infile
           << endl;
      infile >> number;
   } // while infile has valid data
   cout << "After the loop, the state of infile is "
        << infile
        << endl;

   return 0;
} // end of main
