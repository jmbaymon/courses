//  file name -- computation-error.cpp
//  This program shows precision problem with float type.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
using namespace std;


int main()
{
   float increment, sum;

   increment = 1.0 / 100.0;
   sum = 0.0;
   while (sum != 1.0)
   {
      sum += increment;
      cout << sum
           << endl;
   } // while sum is not 1 yet

   return 0;
}  // end of main
