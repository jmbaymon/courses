//  file name -- increment01.cpp
//  This program shows how increment operator works. 

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include <stdio.h>
using namespace std;


int main()
{
   int x;

   x = 5;
   cout << "Value of expression is "
        << ++x
        << endl
        << "value of x is "
        << x
        << endl;

   x = 5;
   cout << "Value of expression is "
        << x++
        << endl
        << "value of x is "
        << x
        << endl;

   return 0;
}  // end of main
