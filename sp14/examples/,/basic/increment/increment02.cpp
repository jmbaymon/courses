//  file name -- increment02.cpp
//  This program shows how increment operator confuses programmers.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include <stdio.h>
using namespace std;


int main()
{
   int x, y;

   x = 5;
   y = 7;
   if (x++ == y)
      cout << y + 2 * x;
   else if (y == ++x)
      cout << ++x;
   else if (y == x)
      cout << x++;
   else
      cout << y + x;

   return 0;
}  // end of main
