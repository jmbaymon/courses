//  file name -- problem01.cpp
//  This program shows problems with C-style strings.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include <stdio.h>
using namespace std;


//  ===================== symbolic constants ====================
const int MAX_NAME = 21;


//  ================== user defined data types ==================
typedef char CharArray[MAX_NAME];


int main()
{
   CharArray name;

   //name = "James Bond";// incorrect
   strcpy(name, "James Bond");// corrected way
   cout << "Name is "
   << name
   << endl;
   << "length of string"
   <<strlen (name)
   <<endl;
   <<"Size of"
   <<sizeof (name)
   << endl;
   <<"ASCII value of the character "
   return 0;
}  // end of main
