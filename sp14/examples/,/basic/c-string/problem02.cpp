//  file name -- problem02.cpp
//  This program shows problems with C-style strings.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include <stdio.h>
using namespace std;


//  ===================== symbolic constants ====================
const int MAX_NAME = 21;
//========================functions prototypes=====================
void displayName();

//  ================== user defined data types ==================
typedef char CharArray[MAX_NAME];


int main()
{
   int age;
   displayName();


   void displayName()
   {
CharArray name;
for (int index = 0; index < MAX_NAME; index++)
   name[0] = 'J';
   name[1] = 'a';
   name[2] = 'm';
   name[3] = 'e';
   name[4] = 's';
   //name[5] = '\0';
   cout << "Name is "
   << name
   << endl;

   }

   return 0;
}  // end of main
