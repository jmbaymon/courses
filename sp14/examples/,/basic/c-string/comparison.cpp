//  file name -- comparison01.cpp
//  This program shows c strings are compared.

//  ======================== header files =======================
#include <iostream>                  // for console I/O
#include <cstring>                   // for string manipulation
#include "utility.h"
using namespace std;


//  ===================== symbolic constants ====================
const int MAX_NAME = 21;


//  ================== user defined data types ==================
typedef char CharArray[MAX_NAME];


int main()
{
   CharArray name1, name2;

   clearScreen();
   // case 1: name 1 is "smaller than" name 2
   strcpy(name1, "James Bond");
   strcpy(name2, "james Bond");
   cout << strcmp(name1, name2)
        << endl;
   pauseScreen();


   // case 2: name 1 is "smaller than" name 2
   strcpy(name1, "Jake Bond");
   strcpy(name2, "James Bond");
   cout << strcmp(name1, name2)
        << endl;
   pauseScreen();


   // case 3: name 1 is "greater than" name 2
   strcpy(name1, "James williamson");
   strcpy(name2, "James William");
   cout << strcmp(name1, name2)
        << endl;
   pauseScreen();


   // case 4: name 1 is "identical to " name 2
   strcpy(name1, "James williamson");
   strcpy(name2, name1);
   cout << strcmp(name1, name2)
        << endl;

   return 0;
}  // end of main
