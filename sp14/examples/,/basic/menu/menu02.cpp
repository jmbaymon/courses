//  file name -- menu02.cpp
//  This program demonstrates how to handle menu-driven programming.
//  The program displays a menu as follows:

//      Unit Conversion
//
//  1.  Convert from yards to inches
//  2.  Convert from yards to feet
//  3.  Convert from feet to inches
//  4.  Exit

//  When the program is executed, the program displays the menu and waits
//  for the user to make a choice. If the user chooses 1, then the program
//  asks the user to enter a value in yards and converts it to the
//  equivalent value in inches. The program pauses the output until the
//  user press the Enter key. After the user presses the Enter key, the
//  program returns to the menu.

//  If the user chooses 2, then the program asks the user to enter a value
//  in yards and converts it to the equivalent value in feet. The program
//  pauses the output until the user press the Enter key. After the user
//  presses the Enter key, the program returns to the menu.

//  If the user chooses 3, then the program asks the user to enter a value
//  in feet and converts it to the equivalent value in inches. The program
//  pauses the output until the user press the Enter key. After the user
//  presses the Enter key, the program returns to the menu.

//  Once the option 4 is chosen, the program terminates.

//  ========================== header files ===========================
#include <iostream>                     // for console I/O
using namespace std;

//  ========================== symbolic constants =====================
const int MAX = 4;
const int MIN = 1;


//  ========================== function prototypes ====================
void displayMenu();
void getChoice(int &);
void takeAction(int);
void yardsToInches();
void yardsToFeet();
void feetToInches();

int main()
{
   int choice;

   do
   {
      displayMenu();
      getChoice(choice);
      takeAction(choice);
   } while (choice != MAX);

   return 0;
}  // end of main


void displayMenu()
//  This function displays the main menu.
{
   cout << "Unit Conversion" << endl;
   cout << endl;
   cout << "1. Convert from yards to inches" << endl;
   cout << "2. Convert from yards to feet" << endl;
   cout << "3. Convert from feet to inches" << endl;
   cout << "4. Exit" << endl;
   cout << endl;
   cout << "Enter your choice ==> ";
}  // end of displayMenu


void getChoice(int &option)
//  This function asks the user to enter a choice based on the menu.
//  If the user enters a choice which is out of bounds, the function
//  asks the user to reenter the value. The process continues until
//  the user enters a choice in the range.
{
   do
   {
      cin >> option;
      if (option < MIN || option > MAX)
      {
         cout << "The choice "
              << option
              << " entered is out of range."
              << endl;
         cout << "Enter the choice again ==> ";
      }  // if
   } while (option < MIN || option > MAX);
}  // end of getChoice


void takeAction(int choice)
//  This function will perform different tasks based on the given choice.
{
   switch (choice)
   {
      case 1: yardsToInches();
              break;
      case 2: yardsToFeet();
              break;
      case 3: feetToInches();
              break;
   }  // switch
}  // end of takeAction


void yardsToInches()
//  This function asks the user to enter a value in yards and converts
//  it to the equivalent value in inches.
{
   cout << "yardsToInches is called, but not finished"
        << endl;
}  // end of yardsToInches


void yardsToFeet()
//  This function asks the user to enter a value in yards and converts
//  it to the equivalent value in feet.
{
   cout << "yardsToFeet is called, but not finished"
        << endl;
}  // end of yardsToFeet


void feetToInches()
//  This function asks the user to enter a value in feet and converts
//  it to the equivalent value in inches.
{
   cout << "feetToInches is called, but not finished"
        << endl;
}  // end of feetToInches
