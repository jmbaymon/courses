// file name -- test-car01.cpp
// This program shows illegal access to private members of
// a class.

// ======================== header files ======================
#include <iostream>                  // for console I/O
#include "car.h"                     // for Car class
using namespace std;                 // for standard library

int main()
{
   Car ethan;

   ethan.color = "Black";

   return 0;
}