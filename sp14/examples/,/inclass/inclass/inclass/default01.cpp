// file name -- decimal.cpp
// This programs shows the problems with accuracy of
// float type.

// ====================== header files =====================
#include <iostream>               // for console I/O
#include <iomanip>                // for precision control
#include <cmath>
using namespace std;              // for standard libary

// ====================== function prototypes=====================
double calculateDistance(double x1 = 0;,double y1 = 0;,double x2 = 0;,double y2 = 0;);


int main()
{
   cout << calculateDistance(1,2,3,4)<< endl;


return 0;
} // end main

double calculateDistance(double x1 = 0,
                          double y1 = 0,
                          double x2 = 0,
                          double y2 = 0){

 return sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

}
