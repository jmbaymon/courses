// file name -- car.h
// This file contains the Car class definition

// ======================== header files ======================
#include <string>                  // for string class
using namespace std;                 // for standard library

class Car
{
   private:
      string color;
      string vendor;
      bool engine_on;

   public:
      void startEngine();
      string getColor();
      string getVendor();
}; // class Car
