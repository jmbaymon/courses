// file name -- test-burger01.cpp
// This program shows how to make your own burger.

// ======================== header files ======================
#include <iostream>                  // for console I/O
#include "burger.h"                  // for Burger class
using namespace std;                 // for standard library


// ==================== function prototypes ===================
void displayBurger(Burger);

int main()
{
   Burger ethan(" bun", 2, "Cheese, Onions, Pickles");

   displayBurger(ethan);
   //displayBurger(mine);

   return 0;
}


void displayBurger(Burger one)
{
   cout << "Type of buns is "
        << one.getBunType()
        << endl
        << "Number of patties is "
        << one.getPatties()
        << "Condiments are "
        << one.getCondiments()
        << endl;
} // end of displayBurger
