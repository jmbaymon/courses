// file name -- burger.cpp
// This file contains function defin itions of Burger class.

// ======================== header files ======================
#include <iostream>                  // for console I/O
#include "burger.h"                  // for Burger class
using namespace std;                 // for standard library


Burger::Burger(string new_bun_type,
               int new_patties,
               string new_condiments)
{
   bun_type = new_bun_type;
   patties = new_patties;
   condiments = new_condiments;
   cout << "set value constructor is called"
        << endl;
} // default constructor


string Burger::getBunType()
{
   return bun_type;
} // getBunType function


int Burger::getPatties()
{
   return patties;
} // getPatties function


string Burger::getCondiments()
{
   return condiments;
} // getCondiments function
