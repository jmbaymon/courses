// file name -- decimal.cpp
// This programs shows the problems with accuracy of
// float type.

// ====================== header files =====================
#include <iostream>               // for console I/O
#include <iomanip>                // for precision control
using namespace std;              // for standard libary


int main()
{
   double value, increment;

   value = 3.0;
   increment = value / 17.0;

   cout << fixed
        << showpoint
        << setprecision(20)
        << value
        << " "
        << increment
        << " "
        << increment * 17.0
        << endl;

   return 0;
} // end main