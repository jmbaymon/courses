// file name -- decimal.cpp
// This programs shows the problems with accuracy of
// float type.

// ====================== header files =====================
#include <iostream>               // for console I/O
#include <iomanip>
#include <string>              // for precision control
using namespace std;              // for standard libary


int main()
{



int a = 3, b = 5;
swap(a, b);

   return 0;
} // end main
void swap(int x, int y)
{
   int temp;

   temp = x;
   x = y;
   y = temp;
}
