// file name -- test-car02.cpp
// This program shows legal access to private members of
// a class.

// ======================== header files ======================
#include <iostream>                  // for console I/O
#include "car.h"                     // for Car class
using namespace std;                 // for standard library

int main()
{
   Car ethan;

   cout << "Color of the car is "
        << ethan.getColor()
        << endl;

   return 0;
}