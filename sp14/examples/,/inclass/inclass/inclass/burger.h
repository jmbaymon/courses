// file name -- burger.h
// This file contains the Burger class definition

// ======================== header files ======================
#include <string>                  // for string class
using namespace std;               // for standard library

#ifndef CLASS_BURGER_
#define CLASS_BURGER_

class Burger
{
   private:
      string bun_type;
      int patties;
      string condiments;

   public:
      Burger(string, int, string);
      string getBunType();
      int getPatties();
      string getCondiments();
}; // class Burger

#endif
