// file name -- decimal.cpp
// This programs shows the problems with accuracy of
// float type.

// ====================== header files =====================
#include <iostream>               // for console I/O
#include <iomanip>
#include <string>              // for precision control
using namespace std;              // for standard libary


int main()
{
   char lastName[15], firstName[15];
   string fullName;
   cout<< "Name Please?"<<endl;
   cin >> fullName;

   cout<< fullName;

   return 0;
} // end main
