// file name -- test-vehicle.cpp

// ======================= header files =======================
#include <iostream>
using namespace std;
#include "car.h"
#include <string>

int main()
{
   Vehicle *one;
   Car *mine;

   one = new Vehicle("My Car", 2012, "Chevorlet");
   cout << "Name of the car is "
        << one -> getName()
        << endl
        << "Year: "
        << one -> getYear()
        << endl
        << "Vendor: "
        << one -> getVendor()
        << endl;

   mine = new Car("Ethan Car", 2009, "Nissan", "V4", "Black", "Almima");
   cout << "Name of the car is "
        << mine -> getName()
        << endl
        << "Year: "
        << mine -> getYear()
        << endl
        << "Vendor: "
        << mine -> getVendor()
        << endl
        << "Engine type: "
        << mine -> getEngineType()
        << endl
        << "Color: "
        << mine -> getColor()
        << endl
        << "Model: "
        << mine -> getModel()
        << endl;


   return 0;
}
