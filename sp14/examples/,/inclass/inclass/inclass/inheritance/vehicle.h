// file name -- vehicle.h

#ifndef CLASS_VEHICLE_
#define CLASS_VEHICLE_

class Vehicle
{
   private:
      string name;
      int year;
      string vendor;

   public:
      Vehicle(string, int, string);
      string getName();
      int getYear();
      string getVendor();
};

#endif