// file name -- car.cpp

// ======================= header files =======================
#include <iostream>
using namespace std;
#include "car.h"
#include <string>


Car::Car(string name,
         int year,
         string vendor,
         string engine_type,
         string color,
         string model)
: Vehicle(name, year, vendor)
{
   this -> engine_type = engine_type;
   this -> color = color;
   this -> model = model;
   cout << "Car constructor is called"
        << endl;
}

string Car::getEngineType()
{
   return engine_type;
}


string Car::getColor()
{
   return color;
}


string Car::getModel()
{
   return model;
}