// file name -- car.h

#ifndef CLASS_CAr_
#define CLASS_CAR_
#include "vehicle.h"

class Car : public Vehicle
{
   private:
      string engine_type;
      string color;
      string model;

   public:
      Car(string, int, string, string, string, string);
      string getEngineType();
      string getColor();
      string getModel();
};

#endif
