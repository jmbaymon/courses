// file name -- vehicle.cpp

// ======================= header files =======================
#include <iostream>
using namespace std;
#include "vehicle.h"
#include <string>


Vehicle::Vehicle(string name,
                 int year,
                 string vendor)
{
   this -> name = name;
   this -> year = year;
   this -> vendor = vendor;
   cout << "Vehicle constructor is called"
        << endl;
}

string Vehicle::getName()
{
   return name;
}


int Vehicle::getYear()
{
   return year;
}


string Vehicle::getVendor()
{
   return vendor;
}