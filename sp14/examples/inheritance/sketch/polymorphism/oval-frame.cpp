// file name -- oval-frame.cpp

// This file contains the definitions of the member functions of
// the OvalFrame class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "oval-frame.h"                 // for OvalFrame class definition
#include <iostream>
using namespace std;


/**
   initialize the object
   precondition: both width and height are positive
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
OvalFrame::OvalFrame(double width,
                     double height,
                     ColorType border_color,
                     double border_width)
   : Frame(border_color, border_width)  // call parent class' constructor
                                        // to initialize member variables
                                        // of parent class
{
   this -> width = width;
   this -> height = height;
} // set value constructor


/**
   return width of the oval frame
   precondition: none
   postcondition: width of the oval frame is returned
*/
double OvalFrame::getWidth()
{
   return width;
} // getWidth


/**
   return height of the oval frame
   precondition: none
   postcondition: height of the oval frame is returned
*/
double OvalFrame::getHeight()
{
   return height;
} // getHeight


/**
   draw the oval frame based on location and scaling factor
   precondition: the (x, y) coordiantes must be inbound and the
                 scaling factor is positive
   postcondition: the oval frame is drawn based on the (x, y)
                  coordinates and the scaling factor
*/
void OvalFrame::draw(double x,
                     double y,
                     double scaling_factor)
{
   double a, b;
   double bound;

   a = width / 2 * scaling_factor;   // scale a based on scaling factor
   b = height / 2 * scaling_factor;  // scale b based on scaling factor
   setColor(border_color);
   x += a;                           // translate x to center of ellipse
   y += b;                           // translate y to center of ellipse
   bound = border_width * scaling_factor;
   for (int n = 1; n <= bound; n++)
   {
      drawEllipse(x, y, a, b);
      a--;
      b--;
   }
} // draw
