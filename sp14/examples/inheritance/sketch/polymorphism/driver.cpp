// file name -- driver.cpp

// This program is served as a test driver to demonstrate how polymorphism works.
// The base class is Frame. Two subclasses, OvalFrame and RectangularFrame are
// derived from the base class. Data about various pictures are read in from a
// text file. The data read in from the file are used to instantiate subclass
// objects based on the type of object to create.

// Each line of the file contains

// a char type (O for OvalFrame and R for RectangularFrame)
// width of the frame
// height of the frame
// the color of the frame border
// a scaling factor
// the (x, y) coordiantes of the lower left corder of the object to be drawn


// ============================== header files ==============================
#include "oval-frame.h"           // for OvalFrame class definition
#include "rect-frame.h"           // for RectangularFrame class definition
#include <iostream>               // for console I/O
#include <fstream>                // for file I/O
using namespace std;


//  ========================== symbolic constants ============================
const int MAX_NAME = 51;
const int MAX_SIZE = 100;
const int DURATION = 1000;

//  ========================= data type declarations =========================
struct FrameDraw
{
   Frame* location;
   double scaling;
   double x;
   double y;
}; // struct FrameDraw

// =========================== function prototypes ===========================
void openFile(ifstream&);
void readData(ifstream&, FrameDraw*, int&);
void display(FrameDraw[], int);
ColorType getColorType(char[]);


int main()
{
   FrameDraw frames[MAX_SIZE];
   ifstream infile;
   int size;

   createWindow(640, 480);
   openFile(infile);
   readData(infile, frames, size);
   display(frames, size);
   startDrawing();

   return 0;
} // default constructor


/**
  purpose: open a data file for read access.
  preconditions: none
  postconditions: a data file has been associated with infile.
*/
void openFile(ifstream &infile)
{
   char infile_name[MAX_NAME];

   do
   {
      cout << "Enter data file name: ";
      cin.getline(infile_name, MAX_NAME);
      infile.open(infile_name);
      if (!infile)
         cout << infile_name
              << " was not opened successfully."
              << endl;
   } while (!infile);
}  // end of openFile


/**
  purpose: read in data about subclass objects from infile and create
           subclass objects accordingly.
  preconditions: infile has been associated with an input data file.
  postconditions: data about subclass objects are read in from the
           file and subclass objects are created and associated with
           pointers of the base class. The size reflects the actual
           number of pointers used in the array of pointers.
*/
void readData(ifstream &infile,
              FrameDraw * list,
              int &size)
{
   char type;
   double width, height, scaling;
   char color[MAX_NAME];
   double x, y;
   ColorType draw_color;

   size = 0;
   infile >> type;
   while (infile)
   {
      infile >> width
             >> height
             >> color
             >> scaling
             >> x
             >> y;
      draw_color = getColorType(color);
      if (toupper(type) == 'O')
         list[size].location = new OvalFrame(width, height, draw_color);
      else
         list[size].location = new RectangularFrame(width, height, draw_color);
      list[size].scaling = scaling;
      list[size].x = x;
      list[size].y = y;
      size ++;
      infile >> type;
   }  // while
}  // end of readData


/**
  purpose: draw frame objects based on their type and location
  preconditions: each object has been properly created
  postconditions: objects of subclass type are drawn based on the draw member function
                  of the class
*/
void display(FrameDraw frames[],
             int n)
{
   delay(DURATION);
   for (int index = 0; index < n; index++)
      frames[index].location -> draw(frames[index].x, frames[index].y, frames[index].scaling);
}  // end of display


/**
  purpose: convert a color string to a ColorType value
  preconditions: code contains a valid string
  postconditions: the color string is converted to a ColorType value and returned
*/
ColorType getColorType(char code[])
{
      if (strcmp("BLACK", code) == 0)
         return BLACK;
      else if (strcmp("BLUE", code) == 0)
         return BLUE;
      else if (strcmp("RED", code) == 0)
         return RED;
      else if (strcmp("GREEN", code) == 0)
         return GREEN;
      else if (strcmp("WHITE", code) == 0)
         return WHITE;
      else if (strcmp("YELLOW", code) == 0)
         return YELLOW;
      else if (strcmp("CYAN", code) == 0)
         return CYAN;
      else if (strcmp("MAGENTA", code) == 0)
         return MAGENTA;
      else if (strcmp("ORANGE", code) == 0)
         return ORANGE;
      else if (strcmp("PURPLE", code) == 0)
         return PURPLE;
      else
         return GRAY;
} // end of getColorType
