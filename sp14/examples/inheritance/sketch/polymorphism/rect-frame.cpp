// file name -- rect-frame.cpp

// This file contains the definitions of the member functions of
// the RectangularFrame class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "rect-frame.h"                 // for RectangularFrame class definition
#include <iostream>
using namespace std;


/**
   initialize the object
   precondition: both width and height are positive
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
RectangularFrame::RectangularFrame(double width,
                                   double height,
                                   ColorType border_color,
                                   double border_width)
   : Frame(border_color, border_width)  // call parent class' constructor
                                        // to initialize member variables
                                        // of parent class
{
   this -> width = width;
   this -> height = height;
} // set value constructor


/**
   return width of the rectangular frame
   precondition: none
   postcondition: width of the rectangular frame is returned
*/
double RectangularFrame::getWidth()
{
   return width;
} // getWidth


/**
   return height of the rectangular frame
   precondition: none
   postcondition: height of the rectangular frame is returned
*/
double RectangularFrame::getHeight()
{
   return height;
} // getHeight


/**
   draw the rectangular frame based on location and scaling factor
   precondition: the (x, y) coordiantes must be inbound and the
                 scaling factor is positive
   postcondition: the rectangular frame is drawn based on the (x, y)
                  coordinates and the scaling factor
*/
void RectangularFrame::draw(double x,
                            double y,
                            double scaling_factor)
{
   double bound;

   setColor(border_color);
   width *= scaling_factor;
   height *= scaling_factor;
   bound = border_width * scaling_factor;
   for (int n = 1; n <= bound; n++)
   {
      moveTo(x, y);
      lineTo(x, y + height);
      lineTo(x + width, y + height);
      lineTo(x + width, y);
      lineTo(x, y);
      width -= 2;
      height -= 2;
      x++;
      y++;
   }

   endOfShape();
} // draw
