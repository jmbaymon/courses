// file name -- rect-frame.h

// This file contains the definition of the RectangularFrame class.

#ifndef CLASS_RECTANGULARFRAME_
#define CLASS_RECTANGULARFRAME_

// ============================== header files ==============================
#include <sketch.h>                    // for Sketch class
#include "frame.h"                     // for Frame class


class RectangularFrame : public Frame
{
   private:
      double width;
      double height;

   public:
      RectangularFrame(double width,
                       double height,
                       ColorType border_color = BLACK,
                       double border_width = 1);

      double getWidth();
      double getHeight();

      void draw(double, double, double scaling_factor = 1);
}; // class RectangularFrame

#endif
