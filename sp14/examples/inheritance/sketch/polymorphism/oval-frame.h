// file name -- oval-frame.h

// This file contains the definition of the OvalFrame class.

#ifndef CLASS_OVALFRAME_
#define CLASS_OVALFRAME_

// ============================== header files ==============================
#include <sketch.h>                    // for Sketch class
#include "frame.h"                     // for Frame class


class OvalFrame : public Frame
{
   private:
      double width;
      double height;

   public:
      OvalFrame(double width,
                double height,
                ColorType border_color = BLACK,
                double border_width = 1);

      double getWidth();
      double getHeight();

      void draw(double, double, double scaling_factor = 1);
}; // class OvalFrame

#endif
