// file name -- driver3.cpp

// This program is served as a test driver to check the correctness of the
// constructor of OvalFram class which is a subclass of the Frame class.


// ============================== header files ==============================
#include "oval-frame.h"                     // for OvalFrame class definition
#include <iostream>
using namespace std;


int main()
{
   OvalFrame one(24, 30);

   return 0;
} // default constructor
