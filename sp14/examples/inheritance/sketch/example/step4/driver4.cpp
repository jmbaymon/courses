// file name -- driver4.cpp

// This program is served as a test driver to check the correctness of the
// draw function of OvalFram class which is a subclass of the Frame class.


// ============================== header files ==============================
#include "oval-frame.h"                     // for OvalFrame class definition
#include <iostream>
using namespace std;


int main()
{
   createWindow(640, 480);
   OvalFrame one(24, 30, ORANGE, 3);
   one.draw(100, 200);
   OvalFrame two(24, 30, BLACK, 3);
   two.draw(200, 300, 3);
   startDrawing();

   return 0;
} // default constructor
