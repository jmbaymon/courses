// file name -- driver1.cpp

// This program is served as a test driver to check the correctness of the
// constructors of Frame class.


// ============================== header files ==============================
#include "frame.h"                     // for Frame class definition
#include <iostream>
using namespace std;


int main()
{
   Frame one;

   return 0;
} // default constructor
