// file name -- frame.h

// This file contains the definition of the Frame class.

#ifndef CLASS_FRAME_
#define CLASS_FRAME_

// ============================== header files ==============================
#include <sketch.h>                    // for Sketch class


class Frame
{
   private:
      ColorType border_color;
      double border_width;

   public:
      Frame(ColorType border_color = BLACK,
            double border_width = 1);

      double getBorderWidth();
      ColorType getBorderColor();

      virtual void draw(double, double, double scaling_factor = 1) = 0;
}; // class Frame

#endif
