// file name -- driver5.cpp

// This program is served as a test driver to check the correctness of the
// draw function of RectangularFrame class which is a subclass of the Frame class.


// ============================== header files ==============================
#include "oval-frame.h"           // for OvalFramee class definition
#include "rect-frame.h"           // for RectangularFramee class definition
#include <iostream>
using namespace std;


int main()
{
   createWindow(640, 480);
   RectangularFrame one(24, 30, ORANGE, 3);
   one.draw(100, 200);
   RectangularFrame two(24, 30, BLACK, 3);
   two.draw(200, 300, 3);
   startDrawing();

   return 0;
} // default constructor
