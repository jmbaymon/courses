// file name -- frame.cpp

// This file contains the definitions of the member functions of
// the Frame class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "frame.h"                      // for Frame class definition
#include <iostream>
using namespace std;


/**
   initialize the object
   precondition: border_width is positive; border_color is one
                 of the predefined colors
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
Frame::Frame(ColorType border_color,
             double border_width)
{
   this -> border_color = border_color;
   this -> border_width = border_width;
} // set value constructor


/**
   return the border color
   precondition: none
   postcondition: border color is returned
*/
ColorType Frame::getBorderColor()
{
   return border_color;
} // getBorderColor


/**
   return the border width
   precondition: none
   postcondition: border width is returned
*/
double Frame::getBorderWidth()
{
   return border_width;
} // getBorderWidth
