// file name -- oval-frame.cpp

// This file contains the definitions of the member functions of
// the OvalFrame class.


// ============================== header files ==============================
#include <sketch.h>                     // for Sketch class
#include "oval-frame.h"                 // for OvalFrame class definition
#include <iostream>
using namespace std;


/**
   initialize the object
   precondition: both width and height are positive
   postcondition: all member variables are assigned based on the values
                 passed to the parameters
*/
OvalFrame::OvalFrame(double width,
                     double height,
                     ColorType border_color,
                     double border_width)
   : Frame(border_color, border_width)  // call parent class' constructor
                                        // to initialize member variables
                                        // of parent class
{
   this -> width = width;
   this -> height = height;
} // set value constructor


/**
   return width of the oval frame
   precondition: none
   postcondition: width of the oval frame is returned
*/
double OvalFrame::getWidth()
{
   return width;
} // getWidth


/**
   return height of the oval frame
   precondition: none
   postcondition: height of the oval frame is returned
*/
double OvalFrame::getHeight()
{
   return height;
} // getHeight
