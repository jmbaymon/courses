// file name -- test-employee02.cpp

//                     ----------
//                    | Employee |
//                     ----------
//                         ^
//                         |
//           -------------------------------
//           |                             |
//    --------------                 -------------
//   |SalaryEmployee|               |WagedEmployee|
//    --------------                 -------------

// This program shows how inheritance works and how to pass either
// an object of WagedEmployee class or an address of an object of
// that class to function.

// ========================= header files =============================
#include <iostream>                  // for console I/O
#include "waged-employee.h"          // for Employee and its subclasses
#include "utility.h"                 // for utility programs
using namespace std;

// =====================function prototypes ===========================
void displayEmployee(WagedEmployee *);
void displayEmployee(WagedEmployee);

int main()
{
   WagedEmployee *who;

   clearScreen();
   who = new WagedEmployee("Joe Smith", 24, 10.25, 25);
   displayEmployee(*who);
   pauseScreen();
   displayEmployee(who);

   return 0;
} // end of main


void displayEmployee(WagedEmployee *one)
{
   cout << "Function receiving a copy of an object address is called"
        << endl
        << "Name: "
        << one -> getName()
        << endl
        << "Age: "
        << one -> getAge()
        << endl
        << "Wage: "
        << one -> getWage()
        << endl
        << "Hours worked: "
        << one -> getHours()
        << endl;
} // end of displayEmployee


void displayEmployee(WagedEmployee one)
{
   cout << "Function receiving a copy of an object is called"
        << endl
        << "Name: "
        << one.getName()
        << endl
        << "Age: "
        << one.getAge()
        << endl
        << "Wage: "
        << one.getWage()
        << endl
        << "Hours worked: "
        << one.getHours()
        << endl;
} // end of displayEmployee
