group=test-employee01.o employee.o salary-employee.o

test-employee01: $(group)
	c++ $(group) -o test-employee01

test-employee01.o: test-employee01.cpp
	c++ -c test-employee01.cpp

employee.o: employee.cpp employee.h
	c++ -c employee.cpp

salary-employee.o: salary-employee.cpp employee.cpp salary-employee.h
	c++ -c salary-employee.cpp
