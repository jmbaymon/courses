group=test-employee02.o employee.o waged-employee.o utility.o

test-employee02: $(group)
	c++ $(group) -o test-employee02

test-employee02.o: test-employee02.cpp
	c++ -c test-employee02.cpp

employee.o: employee.cpp employee.h
	c++ -c employee.cpp

waged-employee.o: waged-employee.cpp employee.cpp waged-employee.h
	c++ -c waged-employee.cpp

utility.o: utility.h utility.cpp
	c++ -c utility.cpp
