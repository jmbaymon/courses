// file name -- test-employee01.cpp

// This program shows how inheritance works.

#include <iostream>                  // for console I/O
#include "salary-employee.h"         // for Employee and its subclasses
using namespace std;

void displayEmployee(Employee *);
void displayEmployee(SalaryEmployee);
//void displayEmployee(SalaryEmployee*);
int main()
{
   SalaryEmployee who("Joe Smith", 24, 45000);
   //Employee whoever("Jojo Wilson");

   displayEmployee(&who);
   displayEmployee(who);

   return 0;
} // end of main


void displayEmployee(Employee *one)//pointer
{
   cout << "The function receiving an address is called"
        << endl
        << "Name: "
        << one -> getName()
        << endl
        << "Age: "
        << one -> getAge()
        << endl;
} // end of displayEmployee


void displayEmployee(SalaryEmployee one)// non pointer
{
   cout << "The function receiving an object is called"
        << endl
        << "Name: "
        << one.getName()
        << endl
        << "Age: "
        << one.getAge()
        <<endl
        <<"Salary"
        << one.getSalary()
        << endl;
} // end of displayEmployee
