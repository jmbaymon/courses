// file name -- employee.cpp

#include <iostream>              // for console I/O
#include <cstring>
#include "employee.h"            // for Employee class
using namespace std;

Employee::Employee()
{
   name = new char[1];
   name[0] = '\0';
   age = 20;
} // Employee::Employee()


Employee::Employee(char *name,
                   int age)
{
   this->name = new char[strlen(name) + 1];
   strcpy(this->name, name);
   this->age = age;
} // Employee::Employee(char*, int)


Employee::Employee(const Employee &source)// alias of who
{
   name = new char[strlen(source.name) + 1];
   strcpy(name, source.name);
   age = source.age;
} // Employee::Employee(const Employee&)


Employee::~Employee()//destructor
{
   delete[] name;
} // end of destructor


char* Employee::getName()
{
   return name;
} // getName


int Employee::getAge()
{
   return age;
} // getAge


void Employee::setName(char* name)
{
   // this->name = name;          SHALLOW COPY
   delete [] name;           // free space for name first
   this->name = new char[strlen(name) + 1];
   strcpy(this->name, name);
} // setName


void Employee::setAge(int age)
{
   this->age = age;
} // setAge
