// file name -- waged-employee.cpp

#include <iostream>              // for console I/O
#include "waged-employee.h"      // for WagedEmployee class
using namespace std;

WagedEmployee::WagedEmployee()
   : Employee("", 0)
{
   wage = 20;
} // WagedEmployee::WagedEmployee()


WagedEmployee::WagedEmployee(char *name,
                             int age,
                             float wage,
                             float hours)
   : Employee(name, age)
{
   this->wage = wage;
   hours_worked = hours;
} // WagedEmployee::WagedEmployee(char*, int)


WagedEmployee::WagedEmployee(const WagedEmployee &source)
   : Employee(source.name, source.age)
{
   wage = source.wage;
   hours_worked = source.hours_worked;
} // WagedEmployee::WagedEmployee(const WagedEmployee&)


WagedEmployee::~WagedEmployee()
{
} // end of destructor


float WagedEmployee::getWage()
{
   return wage;
} // getWaged


void WagedEmployee::setWage(float wage)
{
   this->wage = wage;
} // setName


float WagedEmployee::getHours()
{
   return hours_worked;
} // getWaged


void WagedEmployee::setHours(float hours)
{
   hours_worked = hours;
} // getWaged
