// file name -- employee.h

#ifndef CLASS_EMPLOYEE_
#define CLASS_EMPLOYEE_

class Employee
{
   protected:
      char* name;
      int age;

   public:
      Employee();
      Employee(char*, int);
      Employee(const Employee &);// copy Constructor
      ~Employee();// destructor

      char* getName();
      int getAge();
      void setName(char*);
      void setAge(int);
}; // class Employee

#endif
