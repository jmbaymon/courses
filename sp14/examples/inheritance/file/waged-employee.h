// file name -- waged-employee.h

#ifndef CLASS_WAGED_EMPLOYEE_
#define CLASS_WAGED_EMPLOYEE_
#include "employee.h"

class WagedEmployee : public Employee
{
   protected:
      float wage;
      float hours_worked;

   public:
      WagedEmployee();
      WagedEmployee(char*, int, float, float);
      WagedEmployee(const WagedEmployee &);
      ~WagedEmployee();

      float getWage();
      void setWage(float);
      float getHours();
      void setHours(float);
}; // class WagedEmployee

#endif
