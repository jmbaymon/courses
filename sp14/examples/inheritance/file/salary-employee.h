// file name -- salary-employee.h

#ifndef CLASS_SALARY_EMPLOYEE_
#define CLASS_SALARY_EMPLOYEE_
#include "employee.h"

class SalaryEmployee : public Employee
{
   protected:
      float salary;

   public:
      SalaryEmployee();
      SalaryEmployee(char*, int, float);
      SalaryEmployee(const SalaryEmployee &);
      ~SalaryEmployee();

      float getSalary();
      void setSalary(float);
}; // class SalaryEmployee

#endif
