// file name -- test-employee03.cpp

//                     ----------
//                    | Employee |
//                     ----------
//                         ^
//                         |
//           -------------------------------
//           |                             |
//    --------------                 -------------
//   |SalaryEmployee|               |WagedEmployee|
//    --------------                 -------------

// This program shows how inheritance works and how to instantiate
// objects of SalaryEmplyee and WagedEmployee classes by reading data
// from a text file.

// ========================= header files =============================
#include <iostream>                  // for console I/O
#include <fstream>                   // for file I/O
#include "waged-employee.h"          // for Employee and its subclasses
#include "salary-employee.h"         // for Employee and its subclasses
#include "utility.h"                 // for utility programs
using namespace std;

// ===================== symbolic constants ===========================
const int MAX_NAME = 101;
const int NAME_SIZE = 21;

// ===================== function prototypes ==========================
void openFile(ifstream&);
void readData(ifstream&);
void displaySalaryEmployee(SalaryEmployee *);
void displayWagedEmployee(WagedEmployee);


int main()
{
   WagedEmployee *who;
   ifstream infile;

   clearScreen();
   openFile(infile);
   readData(infile);

   return 0;
} // end of main


void openFile(ifstream &infile)
// purpose: open a data file for read access.
// preconditions: none
// postconditions: a data file has been associated with infile.
{
   char infile_name[MAX_NAME];

   do
   {
      cout << "Enter data file name: ";
      cin.getline(infile_name, MAX_NAME);
      infile.open(infile_name);
      if (!infile)
         cout << infile_name
              << " was not opened successfully."
              << endl;
   } while (!infile);
}  // end of openFile


void readData(ifstream &infile)
// purpose: read data about different employees and instantiate
//          objects in accordance with its type.
// preconditions: infile has been associated with an input data file.
// postconditions: objects are instantiated and their contents are
//          displayed based on its data type.
{
   SalaryEmployee *who;
   WagedEmployee *one;
   char type;
   char name[NAME_SIZE];
   int age;
   float salary;
   float wage;
   float hours;

   infile >> type;
   while (infile)
   {
      infile.get(name, NAME_SIZE);
      infile >> age;
      switch (type)
      {
         case 'S': infile >> salary;
                   who = new SalaryEmployee(name, age, salary);
                   displaySalaryEmployee(who);
                   delete who;
              break;
         case 'W': infile >> wage >> hours;
                   one = new WagedEmployee(name, age, wage, hours);
                   displayWagedEmployee(*one);
                   delete one;
              break;
         default: cout << "Recond not recognized" << endl;
              break;
      } // switch
      pauseScreen();


      infile >> type;
   }  // while
}  // end of readData


void displaySalaryEmployee(SalaryEmployee *one)
{
   cout << "displaySalaryEmployee Function is called"
        << endl
        << "Name: "
        << one -> getName()
        << endl
        << "Age: "
        << one -> getAge()
        << endl
        << "Salary: "
        << one -> getSalary()
        << endl;
} // end of displayEmployee


void displayWagedEmployee(WagedEmployee one)
{
   cout << "displayWagedEmployee Function is called"
        << endl
        << "Name: "
        << one.getName()
        << endl
        << "Age: "
        << one.getAge()
        << endl
        << "Wage: "
        << one.getWage()
        << endl
        << "Hours worked: "
        << one.getHours()
        << endl;
} // end of displayEmployee
