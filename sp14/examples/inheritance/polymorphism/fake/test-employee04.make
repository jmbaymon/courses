group1=test-employee04.o employee.o waged-employee.o utility.o
group2=salary-employee.o

test-employee04: $(group1) $(group2)
	c++ $(group1) $(group2) -o test-employee04

test-employee04.o: test-employee04.cpp
	c++ -c test-employee04.cpp

employee.o: employee.cpp employee.h
	c++ -c employee.cpp

waged-employee.o: waged-employee.cpp employee.cpp waged-employee.h
	c++ -c waged-employee.cpp

salary-employee.o: salary-employee.cpp employee.cpp salary-employee.h
	c++ -c salary-employee.cpp

utility.o: utility.h utility.cpp
	c++ -c utility.cpp
