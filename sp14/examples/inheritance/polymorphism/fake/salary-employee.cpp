// file name -- salary-employee.cpp

#include <iostream>              // for console I/O
#include "salary-employee.h"     // for SalaryEmployee class
using namespace std;

SalaryEmployee::SalaryEmployee()
   : Employee("", 0)
{
   salary = 20;
} // SalaryEmployee::SalaryEmployee()


SalaryEmployee::SalaryEmployee(char *name,
                               int age,
                               float salary)
   : Employee(name, age)
{
   this->salary = salary;
} // SalaryEmployee::SalaryEmployee(char*, int)


SalaryEmployee::SalaryEmployee(const SalaryEmployee &source)
   : Employee(source.name, source.age)
{
   salary = source.salary;
} // SalaryEmployee::SalaryEmployee(const SalaryEmployee&)


SalaryEmployee::~SalaryEmployee()
{
} // end of destructor


float SalaryEmployee::getSalary()
{
   return salary;
} // getSalary


void SalaryEmployee::setSalary(float salary)
{
   this->salary = salary;
}; // setName


void SalaryEmployee::display()
{
   cout << "Name: "
        << name
        << endl
        << "Age: "
        << age
        << endl
        << "Salary: "
        << salary
        << endl;
} // display
