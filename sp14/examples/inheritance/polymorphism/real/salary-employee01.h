// file name -- salary-employee01.h

#ifndef CLASS_SALARY_EMPLOYEE_
#define CLASS_SALARY_EMPLOYEE_
#include "employee01.h"

class SalaryEmployee : public Employee
{
   protected:
      float salary;

   public:
      SalaryEmployee();
      SalaryEmployee(char*, int, float);
      SalaryEmployee(const SalaryEmployee &);
      ~SalaryEmployee();

      float getSalary();
      void setSalary(float);
      void display();
}; // class SalaryEmployee

#endif
