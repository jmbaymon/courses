// file name -- waged-employee01.h

#ifndef CLASS_WAGED_EMPLOYEE_
#define CLASS_WAGED_EMPLOYEE_
#include "employee01.h"

class WagedEmployee : public Employee
{
   protected:
      float wage;
      float hours_worked;

   public:
      WagedEmployee();
      WagedEmployee(char*, int, float, float);
      WagedEmployee(const WagedEmployee &);
      ~WagedEmployee();

      float getWage();
      void setWage(float);
      float getHours();
      void setHours(float);
      void display();
}; // class WagedEmployee

#endif
