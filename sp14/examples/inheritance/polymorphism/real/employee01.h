// file name -- employee01.h

#ifndef CLASS_EMPLOYEE_
#define CLASS_EMPLOYEE_

class Employee
{
   protected:
      char* name;
      int age;

   public:
      Employee();
      Employee(char*, int);
      Employee(const Employee &);
      ~Employee();

      char* getName();
      int getAge();
      void setName(char*);
      void setAge(int);
      virtual void display();
}; // class Employee

#endif
