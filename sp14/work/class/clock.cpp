// file name -- Clock.cpp
// This file contains function defin itions of clock class.

// ======================== header files ======================
#include <iostream>                  // for console I/O
#include "clock.h"                  // for Clock class
using namespace std;                 // for standard library


Clock::Clock( int new_hour,
               int new_minute,bool new_state)
{

      on_state = new_state;
      minute = new_minute;
       hour = new_hour;

} // default constructor

void Clock::displayTime()
{
   cout<<" Time: "
       << hour
       << ":"
       << minute
       << endl;

} // displayTime

bool Clock::isOn()
{

   return on_state;

}// isOn
