// file name -- clock.h
// This file contains the Clock class definition

// ======================== header files ======================

using namespace std;               // for standard library

#ifndef CLASS_CLOCK_
#define CLASS_CLOCK_

class Clock
{
   private:
   bool on_state;
      int hour;
      int minute;

   public:
      Clock(int new_hour = 10, int new_minute = 10,bool new_state = true);
      void powerPressed();
      void forwardPressed();
      void backwardPressed();
      void restPressed();
      void displayTime();
      bool isOn();

}; // class Clock

#endif
