// file name -- test-clock.cpp

// ============================ header files =========================
#include <iostream>                  // for console I/O
#include "clock.h"
using namespace std;                  // for I/O library


// ========================= function prototypes =====================
void displayClock(Clock);

int main()
{
   Clock one;
   Clock two(5,20,true);

   displayClock(one);
   displayClock(two);

   return 0;
} // end of main
void displayClock(Clock clock)
{
   clock.displayTime();
   cout<< "The clock is";
   if (clock.isOn())
      cout<<" on ";
   else
         cout<<"off";
   cout << endl;
}// displayClock
