#ifndef CLASS_CRUISECONTROL_
#define CLASS_CRUISECONTROL_

class CruiseControl

{

private:
bool on_state; // cruise control on/off state
bool engaged; // engaged (true) or disengaged (false)
int speed; // set speed

public:
CruiseControl(bool new_state = false, bool new_engaged = false, int new_speed = 0);//default constructor
void pressOnOff();
void pressSet(int);
void pressResume();
void pressAccel();
void pressCoast();
void disengage();
bool isOn();
bool isEngaged();
int getSpeed();

};
#endif
