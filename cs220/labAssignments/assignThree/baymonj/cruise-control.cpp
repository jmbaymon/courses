// file name -- cruise-control.cpp
// This file contains function definitions of clock class.

// ======================== header files ======================
#include <iostream>                  // for console I/O
#include "cruise-control.h"                  // for CruiseControl class
using namespace std;                 // for standard library


CruiseControl::CruiseControl(bool new_state, bool new_engaged, int new_speed)

{
on_state = new_state;
engaged = new_engaged;
speed = new_speed;
} // default constructor

void CruiseControl::pressOnOff()
{
if(on_state == true)
{
   on_state = false;
   engaged = false;
   speed = 0;
cout<<"cruise control is off"<<endl;
cout<<"cruise control is disengaged"<<endl;
cout<<"Automobile speed: "<< speed<<endl;

}
else
{
on_state = true;
engaged = true;
speed = getSpeed();
cout<<"cruise control is on"<<endl;
cout<<"cruise control is Engaged"<<endl;
cout<<"Automobile speed: "<< speed<<endl;

}



//cout<<"On/Off status:"<< on_state<< endl;


} // pressOnOff


bool CruiseControl::isOn()
{

   return on_state;

}// isOn
void CruiseControl::pressSet(int speed)
{


if(speed >= 40 || speed <= 100)
   {
      if (on_state == true)
    engaged = true;
     //speed = getSpeed();
    speed = getSpeed();
      }
cout<< " speed is now: "<< speed <<endl;
cout<< "Engaged/Disengaged: "<< engaged<< endl;


}
int CruiseControl::getSpeed()
{

   return speed;


}
bool CruiseControl::isEngaged ()
{

return engaged;

}
void CruiseControl:: pressAccel()
{

if(speed < 100 && engaged == true)
 {
    speed+=1;

cout<< "Accel Speed:"<< speed<<endl;
}
else
cout<< "Current speed:"<< speed<<endl;

}
void CruiseControl:: pressCoast()
{

if(speed > 40 && engaged == true)
 {
    speed-=1;

cout<< "Deacceleration Speed:"<< speed<<endl;
}
else
cout<< "Current speed:"<< speed<<endl;



}
void CruiseControl:: pressResume()
{

 if (on_state == true && engaged == false)
    cout <<"Resume Speed: "<< speed << endl;
else
 cout<< "No Effect "<<endl;
}

void CruiseControl:: disengage()
{
if(engaged == true)
  {
     engaged = false;
cout<<"cruise control is disengaged"<<endl;
}
else
cout<<" No Effect"<< endl;

}





