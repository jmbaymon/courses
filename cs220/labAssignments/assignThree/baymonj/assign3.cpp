// file name -- assign3.cpp

// ============================ header files =========================
#include <iostream>              // for console I/O
#include <iomanip>
#include "cruise-control.h"
using namespace std;                  // for I/O library
const int MIN = 1;
const int MAX = 8;

// ========================= function prototypes =====================
void displayCruise(CruiseControl);
void displayMenu(CruiseControl);
void getChoice(int&);
//void takeAction(int);
int main()
{
 int choice, option;

CruiseControl cruiseControl(true,true,50);
       displayMenu(cruiseControl);

   //CruiseControl cruiseControl(true,false,40);

   displayCruise(cruiseControl);


   return 0;
} // end of main
void displayMenu(CruiseControl cruiseControl)
// Purpose: The function will display the main menu
// Precondition:
// Postcondition:
{
   int choice;
      do
   {
     cout <<"1. Press On/Off button  " << endl
   << "2. Press Set button " << endl
   << "3.Press Accel button " << endl
   <<"4.Press Coast button " << endl
   <<"5.Press Resume button" << endl
   <<"6.Disengage" << endl
   <<"7.Display cruise control" << endl
   <<"8.Exit" << endl;
   cout << "Enter your choice ==> ";
   cin >> choice;

   switch(choice)
   {
 case 1:cruiseControl.pressOnOff();

    cout<<endl;
 break;
      case 2:cruiseControl.pressSet(cruiseControl.getSpeed());

    cout<<endl;

      break;
      case 3:cruiseControl.pressAccel();

    cout<<endl;

      break;
      case 4:cruiseControl.pressCoast();

    cout<<endl;

      break;
      case 5:cruiseControl.pressResume();

    cout<<endl;

      break;

      case 6:cruiseControl.disengage();

    cout<<endl;

      break;
      case 7:displayCruise(cruiseControl);

      break;
      case 8:cout<< "Exit Cruise Control"<< endl;

      break;

      default:cout<< "Number invalid. Please try again "<<endl;
      break;
      }
   } while (choice != 8);
                                   // we had it before the switch statement
}


void displayCruise(CruiseControl cruiseControl)
{
  // cruiseControl.pressOnOff();
   cout<< "The Car Cruise Control is ";
   if (cruiseControl.isOn())
      cout<<" on ";
   else
         cout<<" off ";

  cout << endl;

   //cout<<"Engaged Or Disengage: "<< cruiseControl.isEngaged()<< endl;
   if(cruiseControl.isEngaged() == true)
    cout<<"Cruise Control is Engaged"<<endl;
    else
    cout<<"Cruise Control is Disengaged"<<endl;


  cout<<"Current Speed: "<< cruiseControl.getSpeed() <<endl;





}


//cruiseControl.pressSet(0);
