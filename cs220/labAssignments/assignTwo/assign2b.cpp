//Filename:    assign2b.cpp
// Name:Jamal Baymon
// Class:   CSCI 220
// Section/Lab: 01


// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>

using namespace std;

// =============== symbolic constants ==============================
 const int MAX_SIZE = 8;
 const int WIDTH = 6;


// =================== function prototypes ==========================
void displayIntArray(int [] , int);
void testdriver();
void add(int, int[], int&);

int numbers[MAX_SIZE], size;
int main()
{
      testdriver();

    return 0;
}// end main


void displayIntArray(int numbers[] , int size)

{
       int index;

       for(index = 0; index < size; index++)
         {
            cout<< setw(WIDTH)
            << numbers[index];
            if((index + 1) % MAX_SIZE == 0)
               cout << endl;
         }
         cout << endl;
}// end of displayIntArray


void testdriver()
{

   int numbers[MAX_SIZE], size ,value;
  int index;
  index = 0;
cout<< "Test Case 1"<<endl;
value = 4;
size = 5;

numbers[0] = 1;
numbers[1] = 2;
numbers[2] = 3;
numbers[3] = 5;
numbers[4] = 6;
add(value,numbers,size);

      // call function
    displayIntArray(numbers, size);
cout<< "Test Case 2"<<endl;
value = 3;
size = 5;


numbers[0] = -1;
numbers[1] = 0;
numbers[2] = 1;
numbers[3] = 2;
numbers[4] = 4 ;


add(value,numbers,size);

      // call function
    displayIntArray(numbers, size);

       // display test data - contents of the array and the result from the function call



}// end testdrivers

void add(int value ,int numbers[], int& size)

// Purpose:
// Precondition:
// Postcondition:
{
  int index, one, k , temp;
  temp = 0;
  int count = 0;





  for(index = 0; index < size; index++)
  {
     if(value == numbers[index])
        count++;
  }

 //increment size by 1 if count is 0
  if(count == 0)
{




        //Put array in ascending order
        for(k = size -1; k > 0; k--)
        {
           one = 0;
           for(int j = 1; j<= k; j++)
           {
              if(numbers[j] > numbers[one])
                 one = j;


           }
         }
            {
           temp = numbers[one];
           numbers[one] = numbers[k];
           numbers[k] = temp;
            }

}

  else

        //Put array in ascending order
        for(k = size -1; k > 0; k--)
        {
           one = 0;
           for(int j = 1; j<= k; j++)
           {
              if(numbers[j] > numbers[one])
                 one = j;
            }

            }
         {
          temp = numbers[one];
           numbers[one] = numbers[k];
           numbers[k] = temp;
         }
}//end add
