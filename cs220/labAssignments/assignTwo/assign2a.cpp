//Filename:    assign2a.cpp
// Name:Jamal Baymon
// Class:   CSCI 220
// Section/Lab: 01


// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>

using namespace std;

// =============== symbolic constants ==============================
 const int MAX_SIZE = 8;
 const int WIDTH = 6;



// =================== function prototypes ==========================
void displayIntArray(int [] , int );
void testdriver();
int lastPositivePostion( int[], int);


int main()
{
      testdriver();
    //displayIntArray(values, size);


    return 0;
}// end main


void displayIntArray(int values[] , int size)

{
       int index;

       for(index = 0; index < size; index++)
         {
            cout<< setw(WIDTH)
            << values[index];
            if((index + 1) % MAX_SIZE == 0)
               cout << endl;
         }
         cout << endl;
}// end of displayIntArray


void testdriver()
{

   int numbers[MAX_SIZE],size;
   int index,j, num;

       cout <<"Test Case 1" << endl;
       cout << "Only positive integers in array  "
       << endl;

       // initialize array and array size for the specific
       size = 8 ;
       num = 3;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;

          num += 3;

          index++;
       }// end for j

       // call function
       cout << "The position of the last positive integer: "
            << lastPositivePostion(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       displayIntArray(numbers, size);

 cout <<"Test Case 2" << endl;
       cout << "Some positive integers  in array"
       << endl;

       // initialize array and array size for the specific
       size = 8 ;
       num = 3;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;

          num -= 1 ;

          index++;
       }// end for j

       // call function
       cout << "The position of the last positive integer: "
            << lastPositivePostion(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       displayIntArray(numbers, size);

      cout <<"Test Case 3" << endl;
       cout << "No  positive integers  in  array"
       << endl;

       // initialize array and array size for the specific
       size = 8 ;
       num = -2;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;

          num -= 2 ;

          index++;
       }// end for j

       // call function
       cout << "The position of the last positive integer: "
            << lastPositivePostion(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       displayIntArray(numbers, size);



}// end testdrivers


int lastPositivePostion(int numbers[], int size )
{
   int index = 0;
   int k;
   int pos = 0;
   for(k = 1; k <= size; k++)
    {

       index++;
       if(numbers[index] > 0 )
         pos++;
    }
    return pos;
 if(index < 0)
       return -1;








}//end lastPositivePostion
