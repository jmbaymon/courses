//Filename:    assign1a.cpp
// Name:Jamal Baymon
// Class:   CSCI 220
// Section/Lab: 01


// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>

using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;


// ================== data type declarations =======================
int valueX, valueY;
// =================== function prototypes ==========================
void testdriver1();
int determineLocation( int, int);


int main()
{

    testdriver1();


    return 0;
}// end main


void testdriver1()

{
       int x,y;

       // Case 1:
       cout<<"Test Case 1 : First Quadrant" << endl;
         // call function
       cout << "The returned value is "
            << determineLocation(5,3)
            << endl;

            // Case 2:
       cout<<"Test Case 2 : Second Quadrant" << endl;
      // call function
       cout << "The returned value is "
            << determineLocation(-6,8)
            << endl;



               // Case 3:
       cout<<"Test Case 3 : Third Quadrant" << endl;
         // call function
       cout << "The returned value is "
            << determineLocation(-4,-6)
            << endl;


   // Case 4:
       cout<<"Test Case 4 : Fourth Quadrant" << endl;



      // call function
       cout << "The returned value is "
            << determineLocation(5,-7)
            << endl;


// Case 5:
       cout<<"Test Case 5 :  X-Axis" << endl;



      // call function
       cout << "The returned value is "
            << determineLocation(-4,0)
            << endl;


         // Case 6:
       cout<<"Test Case 6 : Y-Axis" << endl;



      // call function
       cout << "The returned value is "
            << determineLocation(0,6)
            << endl;



         // Case 7:
       cout<<"Test Case 7 : Origin" << endl;



      // call function
       cout << "The returned value is "
            << determineLocation(0,0)
            << endl;



         }// end testdriver


int determineLocation(int valueX, int valueY)
{
   if(valueX > 0 && valueY > 0)
      return 1;
   else if(valueX < 0 && valueY > 0)
      return 2;

   else if (valueX < 0 && valueY < 0)
         return 3;
   else if (valueX > 0 && valueY < 0)
         return 4;
   else if (valueX != 0 && valueY == 0)
         return 5;
   else if (valueX == 0 && valueY != 0)
            return 6;
   else
         return 7;
      }//end determineLocation
