//Filename:    assign1b.cpp
// Name:Jamal Baymon
// Class:   CSCI 220
// Section/Lab: 01
//This program indicates whether a person calorie intake is High, Normal, or Low.

// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>

using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;


// ================== data type declarations =======================
char gender;
int wgtPounds, inCals;
// =================== function prototypes ==========================
void testdriver1();
char checkIntake( char,int, int);


int main()
{

    testdriver1();


    return 0;
}// end main


void testdriver1()

{



       // Case 1:
       cout<<"Test Case 1:Female High Calorie Intake" << endl;

      // call function
       cout << "Your Calorie Intake is  "
            << checkIntake('F',200,6000)
            << endl;

      // Case 2:
       cout<<"Test Case 2:Female Normal Calorie Intake" << endl;

      // call function
       cout << "Your Calorie Intake is "
            << checkIntake('F',190,3419)
            << endl;

         // Case 3:
       cout<<"Test Case 3:Female Low Calorie Intake" << endl;

      // call function
       cout << "Your Calorie Intake is "
            << checkIntake('F',130,50)
            << endl;

      // Case 4:
       cout<<"Test Case 4:Male High Calorie Intake" << endl;

      // call function
       cout << "Your Calorie Intake is "
            << checkIntake('M',225,5000)
            << endl;

      // Case 5:
       cout<<"Test Case 5:Male Normal Calorie Intake" << endl;

      // call function
       cout << "Your Calorie Intake is "
            << checkIntake('M',180,3730)
            << endl;

            // Case 6:
       cout<<"Test Case 6:Male Low Calorie Intake" << endl;

      // call function
       cout << "Your Calorie Intake is "
            << checkIntake('M',170,400)
            << endl;






         }


char checkIntake(char gender , int wgtPounds, int inCals)
{
   int calories;

   if(gender =='F')
      {
      calories = 18 * wgtPounds;

      if(inCals > calories + 50)
         return 'H';

      else if(inCals <= calories + 50 && inCals >= calories - 50)
         return 'N';


      else
         return 'L';


      } // end female nested if statemen

      if(gender =='M')
      {
      calories = 21 * wgtPounds;

      if(inCals > calories + 50)
         return 'H';

      else if(inCals <= calories + 50 && inCals >= calories - 50)
         return 'N';


      else
         return 'L';
      }// end Male nested if statement



}//end checkIntake function
