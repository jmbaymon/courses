// Filename:    programname.cpp
// Name: Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 2 - Tuesday and Thursday

// Problem Description:




// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>           // for output format
#include <fstream>
#include "utility.h"
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 21;


// ================== data type declarations =======================


// =================== function prototype ==========================
void openFile(ifstream&);
void processFacebookFile(ifstream&,int&, int&);
void computePrintAverage (int, int);

int main()
{
    // data declarations
    ifstream facefile;
    int totalAccounts , totalFriends;
   system("clear");
   openFile(facefile);
   processFacebookFile(facefile, totalAccounts , totalFriends);
   computePrintAverage(totalAccounts , totalAccounts);
   facefile.close();


   return 0;
}// end main

