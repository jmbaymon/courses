// Filename: lab8a-2.cpp
// Name: Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1 - Tuesday

// Problem Description:
// ==================== header files ===============================
#include <iostream>  // for input/output
#include <iomanip>  // for output format
#include "utility.h"
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 20;
const int FLAT_ZERO = 0;

// =================== function prototypes ==========================
void testdriver1();
void testdriver2();
void testdriver3();
void printArray(const int[], int);
int allPositive(int[], int);
int findOddLocation(int[],int);
int sameTotals(int[],int[],int);

int main()
{

    clearScreen();
    testdriver1();
    testdriver2();
    testdriver3();

    return 0;
}// end main


void testdriver1()
// Purpose:To test the allPositive function
// Precondition:values have been assigned to numbers[] and size
// Postcondition:the return  values of the test cases will be display.
{
       int numbers[MAX_SIZE], size;
       int index, j, num;

       // Case 1: All the elements of array are above 0
       cout <<"Test Function: allPositive" << endl;
       cout << "Case 1:All the elements of array are above 0. "
       << endl;

       // initialize array and array size for the specific
       size = 5;
       num = 10;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num += 10;
          index++;
       }// end for j

       // call function
       cout << "The returned value is "
            << allPositive(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
       cout << "Case 2: None of the elements are above 0. " << endl;

       // initialize array for the specific test case
       size = 5;
       num = -12;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num--;
          index++;
       }// end for j
       // call function
      cout << "The returned value is "
            << allPositive(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
      printArray(numbers, size);

      pauseScreen();
       clearScreen();
       cout << "Case 3:Some of elements are  above 0   "
       << endl;

       size = 7;
       num = 3;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num--;
          index++;
       }// end for j
       // call function
      cout << "The returned value is "
            << allPositive(numbers, size)
            << endl;
       // display test data and results from function
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
}// end

void printArray(const int list[], int size)
// Purpose: display the size and contents of an integer array
// Precondition: values have been assigned to list[] and size
// Postcondition: the contents of the array list[] and its size are displayed
//                on the computer screen
{
     int i, index;

     cout << "Size of the array: " << size << endl;
     cout << "Contents of the array: ";
     index = 0;   // initialize array index

     for ( i = 1; i <= size; i++)
     {
         cout << list[index] << ", ";
         index++;          // increment array index
     }// end for i

     cout << endl;

}// end printArray
//===========================================================
void testdriver2()
// Purpose:To test the findOddLocation function.
// Precondition:values have been assigned to numbers[] and size
// Postcondition:the return  values of the case will be display.
{
int numbers[MAX_SIZE], size;
       int index, j, num;

       // Case 1:More than 3 odd integers
       cout <<"Test Function: findOddLocation" << endl;
       cout << "Case 1:More than 3 odd integers "
       << endl;

       // initialize array and array size for the specific
       size = 5;
       num = 51;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num += 3;
          index++;
       }// end for j

       // call function
       cout << "The returned value is "
            <<findOddLocation(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();

       // Case 2:Less than 3 odd integers.
       cout << "Case 2:Less than 3 odd integers." << endl;

       // initialize array for the specific test case
       size = 5;
       num = 10;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num += 5;
          index++;
       }// end for j
       // call function
      cout << "The returned value is "
            << findOddLocation(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
      printArray(numbers, size);

      pauseScreen();
       clearScreen();
       //Case 3:Exactly three odd integers
       cout << "Case 3:Exactly three odd integers"
       << endl;

       size = 6;
       num = 30;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num += 15;
          index++;
       }// end for j
       // call function
      cout << "The returned value is "
            << findOddLocation(numbers, size)
            << endl;
       // display test data and results from function
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
}// end
//==================================================
void testdriver3()
{
   cout<<"testdriver3 in progress"<<endl;
}// end
//==================================================

int allPositive(int numbers[], int size)
// Purpose:This function tell whether the contents of the array are above zero.
// Precondition:Determine if the whole array  is positive.
// Postcondition:Tested in testdriver1
{
 int index,j,posCount;

   index = 0;
   posCount = 0;


   for (j = 1; j <= size; j++)
   {
      if ( numbers[index] >= FLAT_ZERO)
      {
         posCount++;
         }

      index++;
      } // end for k
if(posCount == size)
return 1;
else
return 0;
   }// end
//=======================================================

int findOddLocation (int numbers[], int size)
// Purpose:Find the 3rd odd integer in the array.
// Precondition:Determine the location of the third odd integer in array.
// Postcondition:Tested in testdriver2
{
   int index,j,oddCount;

   index = 0;
   oddCount = 0;


   for (j = 1; j <= size; j++)
   {
      if ( numbers[index]% 2 != 0)
      oddCount++;

      if(oddCount == 3)
         return index;

      index++;


   } // end for k

   return -1;
}// end
//========================================================
int sameTotals (int numbers1[],int numbers2[], int size)
{

}// end function
