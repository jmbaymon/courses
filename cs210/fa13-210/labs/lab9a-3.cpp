// File Name ---lab9a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 9a
// Problem Description:


// ======================= Header Files ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 23;
const int WIDTH2 =20;
const int MAX_SIZE = 20;
const int JUDGES= 20;
const int CONTEST_SPOTS = 25;

// ================ Function Prototypes ==========================
void openFile(ifstream&);
void fileFill(ifstream&, int[], int);
void printArray(int[], int);
int computePoints(int[], int);
int underScores(float,int[],int);
int countAbove18(int[], int);
void scoringHeading();
void scoringResult(int, float, int );

int main()
{
// data declarations
ifstream scorefile;
int scores[JUDGES];
int numContest;
int totalPoints;
int underCount;
float avg;




clearScreen();

openFile(scorefile);

fileFill(scorefile,scores,JUDGES);

scoringHeading();

numContest = 1;


   while(scorefile)
   {
//printArray(scores,JUDGES);

totalPoints = computePoints(scores,JUDGES);

avg = totalPoints/JUDGES;

underCount = underScores(avg,scores,JUDGES);

      cout << setw(WIDTH2) << numContest
      << setw(WIDTH2)<< avg
      <<setw(WIDTH2)<< underCount
      <<endl;







fileFill(scorefile,scores,JUDGES);

numContest++;
}//end while


scorefile.close();





return 0;
}// end main

void openFile(ifstream& scorefile)
// Purpose:
// Precondition:
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   scorefile.open(filename);

   //verify link to file is GOOD
   if (!scorefile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !scorefile);
   }
// end openFile


void fileFill(ifstream& scorefile, int numbers[], int size )
// Purpose:
// Precondition:
// Postcondition:
{
      int k, index, data;



      // initialize index
      index = 0;


      // test not end of numFile
      for(k = 1;scorefile && index < JUDGES;k++)
      {
         scorefile >> data;

         // assign data to numbers array
         numbers[index] = data;


         // update index
         index++;
      }// end while not end of numFile

}// end


void scoringHeading()
{
   cout<<setw(WIDTH)<<"Contestant No."
       <<setw(WIDTH)<<"Average Score"
       << setw(WIDTH)<<"#Scores Under Average"<<endl;

}//end scoringHeading
int computePoints(int scores[], int size)
{
   int j, index;

   int points;

   //intialize
   index = 0;

   points = 0;



   for(j=1;j<=size;j++)
      {
         points = points + scores[index];


      //update index
         index++;
      }//end for j
return points;

}//end computePoints

int underScores(float avg , int scores[],int size)
{
int index ,k;

int avgCount;

index = 0;

avgCount = 0;

for(k=1;k<=size;k++)
{

   if(scores[index]< avg)
      avgCount++;


      index++;
}// end k

return avgCount;

}//end underScores








