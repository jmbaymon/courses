// File Name ---lab9a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 9a
// Problem Description:


// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 25;
const int WIDTH2 =20;
const int MAX_SIZE = 23;
const int MIDTERM_Qs= 20;


// ================ Function Prototypes ==========================
void openFile(ifstream&);
void fileFill(ifstream&, int[], int);
void testHeading();
int correctA( int[] , int[], int );


int main()
{
// data declarations
ifstream answerfile;
int answers[MIDTERM_Qs];
int studentNum;
int countCorrect;

clearScreen();
openFile(answerfile);
testHeading();
correctA(answerfile,answers,MIDTERM_Qs);

studentNum = 1;
countCorrect = 0;

   while(answerfile)
   {

      countCorrect = correctA(answerfile,answers,MIDTERM_Qs);

      cout<< setw(WIDTH2)<< studentNum
          <<setw(WIDTH2)<< countCorrect<<endl;


fileFill(answerfile,answers,MIDTERM_Qs);



studentNum++;



   }//end while



   return 0;
}// end main

void openFile(ifstream& answerfile)
// Purpose:
// Precondition:
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   answerfile.open(filename);

   //verify link to file is GOOD
   if (!answerfile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !answerfile);
   }
// end openFile


void fileFill(ifstream& answerfile, int numbers[], int size )
// Purpose:
// Precondition:
// Postcondition:
{
      int k, index, data;



      // initialize index
      index = 0;

      // test not end of numFile
      for(k = 1;answerfile && index < MIDTERM_Qs;k++)
      {
         answerfile >> data;


         // assign data to numbers array
         numbers[index] = data;

         //cout<< numbers[index]<<",";




         // update index
         index++;
      }// end while not end of numFile
         //cout<<endl;
}// end

void testHeading()
{
   cout<<"Professor E.Z. Ayes Class Results"<<endl;
   cout<<"==============================================================================================="<<endl;
   cout<<setw(WIDTH)<<"Student No."
       <<setw(WIDTH)<<"Number Correct"
       << setw(WIDTH)<<"Mid Term"<<endl;
      cout<<"==============================================================================================="<<endl;

}//end testHeading
 int correctA (int numbers1[],int numbers2[], int size)
{

   int index,j;


   index = 0;


  for (j = 1; j <= size; j++)
   {
      numbers1[index];

      sumTwo = sumTwo + numbers2[index];


      index ++;
   } // end for k
   if(sumOne == sumTwo)
      return 0;

   else if(sumOne > sumTwo)
          return 1;

   else
      return -1;
}// end function
