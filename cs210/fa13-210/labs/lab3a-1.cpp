//Filename:lab3a-1.cpp
// Name:Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1- Tuesday

//This program shows how to test a function using a test driver.

// ==================== header files ===============================
#include <iostream>           // for input/output
#include <iomanip>
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int INCOME_MIN = 8000;
const int INCOME_MID = 24900;
const int INCOME_MAX = 63500;
const int MIN_COST = 560;
const int MID_COST = 2588;
const int MAX_COST = 11273;
const float PERCENT_A  = 0.07;
const float PERCENT_B = 0.12;
const float PERCENT_C = 0.225;
const float PERCENT_D = 0.269;


// ================== data type declarations =======================


// =================== function prototype ==========================

void findFederalTax(int,float&);

int main()
{


 int income;
 float tax;


income = 7999;
findFederalTax( income,tax);
 cout << "Taxable Income: $"<< income<< endl
       << "Federal Tax = $"<< tax <<endl;



income = 8000;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl
       << "Federal Tax = $"<< tax<<endl;

income = 8001;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl
       << "Federal Tax = $"<< tax<<endl;
income = 24899;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl
       << "Federal Tax = $"<< tax<<endl;

income = 24900;
   findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl<< "Federal Tax = $"<< tax<<endl;

income = 24901;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl<< "Federal Tax = $"<< tax<<endl;

income = 63499;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl<< "Federal Tax = $"<< tax<<endl;

income = 63500;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl<< "Federal Tax = $"<< tax<<endl;

income = 63501;
findFederalTax( income,tax);
 cout << "Taxable Income: $" << income<< endl<< "Federal Tax = $"<< tax<<endl;






return 0;
}
void findFederalTax(int income ,float& tax)

// Purpose: This function will find  the federal tax on on income
// Precondition:
//
// Postcondition:
//
{

   if ( income <= INCOME_MIN)
      {
        tax = income * PERCENT_A;
       }

     else if (income > INCOME_MIN && income <= INCOME_MID)
      {
        tax = MIN_COST + (income * PERCENT_B);
       }

     else if (income > INCOME_MID && income <= INCOME_MAX)
       {
        tax = MID_COST + (income * PERCENT_C);
        }
     else
        {
        tax = MAX_COST + (income * PERCENT_D);
        }



}   // end of function findFederalTax
