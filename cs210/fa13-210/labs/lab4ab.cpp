// File Name ---lab4ab.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 4a
// Problem Description:Write an application that allows the user to compute their automobile repair costs.
// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 5;
const int LABOR_RATE = 35;
const float SALE_TAXES = 0.05;
// ================ Function Prototypes ==========================
void readCustomerInfo(int&, int&, float&);
void calculateCustomerCost ( int, float,float&, float&, float&);
void displayCustomerResults(int,int,float , float,float);
int main()
{
// data declarations
int customerNum,laborHours;
float partsCost,laborCost ,totalpartsCost,totalCost;

system("clear");

readCustomerInfo(customerNum, laborHours, partsCost);

calculateCustomerCost(laborHours, partsCost,laborCost,totalpartsCost, totalCost);

displayCustomerResults(customerNum, laborHours,laborCost,totalpartsCost, totalCost);
return 0;
}// end main
void readCustomerInfo(int&, int&, float&)
// Purpose: This function reads in the account number,hours,and the cost of parts and supplies.
// Precondition: None.
// Postcondition: The account number,hours,and the cost of parts and supplies are entered from the keyboard.
// output parameters - (int)customerNum, (int)laborHours, (float)partsCost
{
cout << "function readCustomerInfo is under construction" << endl;
}// end readCustomerInfo function
void calculateCustomerCost ( int, float,float&, float&, float&)
// Purpose: This function calculates labor cost, total cost of parts and supplies,and total cost.
// Precondition:  The account number,hours,and the cost of parts and supplies have been read from the keyboard.
// Postcondition: Labor cost, total cost of parts and supplies,and total cost are calculated.
// output parameters-(float)laborCost,(float)totalpartsCost, (float)totalCost
{
cout << "function calculateCustomerCost is under construction" << endl;
}
// end calculateCustomerCost

void displayCustomerResults(int,int,float, float,float)
// Purpose: This function displays the output.
// Precondition:  The account number,hours,and the cost of parts and supplies have been read from the keyboard and labor cost, total cost of parts, and supplies,and total cost are calculated.
// Postcondition:: customerNum , laborHours , laborCost, totalpartsCost, and totalCost have printed on the computer screen.
{
cout << "function displayCustomerResults is under construction" << endl;
}
// end displayCustomerResults


