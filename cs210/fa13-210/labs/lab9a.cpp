// File Name ---lab9a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 9a

// ======================= Header Files ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 1;
const int WIDTH = 23;
const int WIDTH2 = 20;
const int EIGHTEEN = 18;
const int FOUR = 4;
const int MAX_SIZE = 20;
const int JUDGES= 20;
const int CONTEST_SPOTS = 25;
const int HUNDRED = 100;
const int AT_LEAST_SCORE = 325;

// ================ Function Prototypes ==========================
void openFile(ifstream&);
void fileFill(ifstream&, int[], int);
void printArray(int[], int);
int computePoints(int[], int);
int underScores(float,int[],int);
int countAbove18(int[], int);
void scoringHeading();
void scoringResult(int, float, int );

int main()
{
// data declarations
ifstream scorefile;
int scores[JUDGES];
int numContest;
int totalPoints;
int underCount, countFour,atLeast325Count ,scoreTop,topContest;
float avg,percentAbove18;




clearScreen();

openFile(scorefile);

fileFill(scorefile,scores,JUDGES);

scoringHeading();

numContest = 1;

countFour = 0;

atLeast325Count = 0;

scoreTop = -1;

topContest = -1;


   while(scorefile)
   {


   totalPoints = computePoints(scores,JUDGES);

      avg = totalPoints/JUDGES;

      underCount = underScores(avg,scores,JUDGES);

      cout << setw(WIDTH2) << numContest
      << setw(WIDTH2)<<  fixed << showpoint
      << setprecision(DECIMAL)<< avg
     <<setw(WIDTH2)<< underCount
      <<endl;

      if(totalPoints >= AT_LEAST_SCORE)

         ++atLeast325Count;


      if(countAbove18(scores, JUDGES) >= FOUR )

      ++countFour;

      if(avg > scoreTop)
         {
            scoreTop = avg;


            topContest= numContest;

         }

      fileFill(scorefile,scores,JUDGES);

      numContest++;

   }//end while loop

   percentAbove18 = countFour/float(CONTEST_SPOTS)* HUNDRED;

   scoringResult( atLeast325Count,percentAbove18,topContest);





scorefile.close();





return 0;
}// end main

void openFile(ifstream& scorefile)
// Purpose: Opens scorefile and verify link.
// Precondition:none
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   scorefile.open(filename);

   //verify link to file is GOOD
   if (!scorefile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !scorefile);
   }// end openFile


void fileFill(ifstream& scorefile, int numbers[], int size )
// Purpose: Reads scorefile and scorefile fill the array.
// Precondition: Reads scorefile
// Postcondition:scorefile fill the array
{
      int k, index, data;



      // initialize index
      index = 0;


      // test not end of numFile
      for(k = 1;scorefile && index < JUDGES;k++)
      {
         scorefile >> data;

         // assign data to numbers array
         numbers[index] = data;


         // update index
         index++;
      }// end while not end of numFile

}// end fileFill



void scoringHeading()
//Purpose: Display table headers.
// Precondition:none
// Postcondition:none
{

   cout<<"====================================================================================="<<endl;
   cout<<setw(WIDTH)<<"Contestant No."
       <<setw(WIDTH)<<"Average Score"
       << setw(WIDTH)<<"#Scores Under Average"<<endl;
    cout<<"====================================================================================="<<endl;

}//end scoringHeading
int computePoints(int scores[], int size)
//Purpose: Computes the number of points and returns "points" to main function.

{

   int j, index;

   int points;

   //intialize
   index = 0;

   points = 0;



   for(j=1;j<=size;j++)
      {
         points = points + scores[index];


      //update index
         index++;
      }//end for j
return points;

}//end computePoints

int underScores(float avg , int scores[],int size)
//Purpose: Determines if the array index is less than the average and returns the average count.
{
int index ,k;

int avgCount;
//intitalize
index = 0;

avgCount = 0;

for(k=1;k<=size;k++)
{

   if(scores[index]< avg)
      avgCount++;

      //update index
      index++;
}// end k

return avgCount;

}//end underScores

int countAbove18(int scores[], int size)
//Purpose:Determines if the array index is greater than or equal to 18 and returns count18 to the main function.
{
  int index , a;

  int count18;
//intitalize
  index = 0;

  count18 = 0;

   for(a=1;a<=size;a++)
   {
      if(scores[index]>= EIGHTEEN)

         count18++;

         // update index
         index++;

      }// end a

   return count18;


}//end countAbove18

void scoringResult(int atLeast325Count, float percentAbove18 , int topContest)
//Purpose:Display results of competition.
{
 cout<<"====================================================================================="<<endl;

   cout<< "# of contestants with at least 325 total points = "<< atLeast325Count<<endl;
   cout<<"% of contestants with 4 or more scores 18 or higher = "<<percentAbove18<<"%"<<endl;
   cout<<"The number of the contestant with the highest average is "<<"#"<<topContest<<endl;


}//end scoringResult






