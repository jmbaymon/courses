// File Name ---lab5a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 5a
// Problem Description:Write a program for the Skegee Credit Card to process account operations for the month of  September.

// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 5;
const float INTEREST_REG = 0.18;
const float INTEREST_GOLD = 0.06;
const float OVER_BAL = 500.00;

// ================ Function Prototypes ==========================
void initializeBalance( float& ,float&, int&);
void processOperation(float& ,float&, int&);
void displayBalance(float,float, int);
int main()
{
// data declarations
float strBal;
char statusCard;
char operType;
float operAmt;
float operTotal;
int numCharge;
float totalPayment;
float finBal;
system("clear");
initializeBalance(finBal,totalPayment, numCharge);
processOperation(finBal,totalPayment, numCharge);
displayBalance(finBal,totalPayment, numCharge);
return 0;
}// end main
void initializeBalance( float& finBal ,float& totalPayment, int& numCharge)

// Purpose:User inputs starting balance.
// Precondition:none
// Postcondition:Final Balance, Total Payment, Number of Charges are sent toprocessOperation.

{
   float strBal;
// intialize input
   totalPayment = 0;
   numCharge = 0;
   cout << "Enter Starting Balance: "<< endl;
   cin >> strBal;
   finBal = strBal;
}
// end
void processOperation(float& finBal ,float& totalPayment, int& numCharge)
// Purpose:Calculates Final Balance, Total Payment, Number of Charges.
// Precondition:none
// Postcondition:Final Balance, Total Payment, Number of Charges are sent displayBalance.

{
char statusCard;
char operType;
float operAmt;

cout << "Enter your status level(R-Regular or G-Gold)" << endl;
   cin >> statusCard;
   cout<< "Enter Operation Type(P-payment, C-charge, or Q-quit):"<<endl;
   cin>> operType;
   while(operType != 'Q' &&  operType != 'q')
 {
   cout <<"Enter Operaton Amount:"<<endl;
   cin >> operAmt;

if (operType == 'c' || operType == 'C')

{
if(statusCard == 'r' || statusCard =='R')
finBal = finBal + operAmt  + (operAmt * INTEREST_REG);

else if(statusCard == 'g' || statusCard == 'G')
finBal = finBal + operAmt + (operAmt * INTEREST_GOLD);
//update Number of Charges
numCharge++;
}

else if(operType == 'p' || operType == 'P')
{
        finBal = finBal - operAmt;

   totalPayment = totalPayment + operAmt;
}

cout << "Enter Operation Type(P-payment, C-charge, or Q-quit):"<< endl;
     cin >> operType;
}//end while loop


}

// end processOperation

void displayBalance(float finBal , float totalPayment, int numCharge)
// Purpose: This function displays the output.
// Precondition:Final Balance, Total Payment, Number of Charges are displayed.
// Postcondition:none
{
   cout << fixed << showpoint;
cout << setprecision(DECIMAL);
cout << endl << endl;
cout <<"Number of Charges:"<< numCharge <<endl
     <<"Total Payment for Month:"<< totalPayment<<endl;

      if(finBal < OVER_BAL)
      cout<<"The final balance is less than 500 dollars:$"<<finBal<< endl;
      else
      cout<<"The final balance is more than 500 dollars:$"<<finBal<< endl;

   }
// end displayBalance
