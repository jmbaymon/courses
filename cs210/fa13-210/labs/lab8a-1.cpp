// Filename: lab8a-1.cpp
// Name: Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1 - Tuesday

// Problem Description:
// ==================== header files ===============================
#include <iostream>  // for input/output
#include <iomanip>  // for output format
#include "utility.h"
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 20;
const int FLAT_ZERO = 0;

// ================== data type declarations =======================


// =================== function prototypes ==========================
void testdriver1();
void testdriver2();
void testdriver3();
void printArray(const int[], int);
int allPositive( int[], int);
int findOddLocation(int[],int);
int sameTotals(int[],int);

int main()
{

    clearScreen();
    testdriver1();
    testdriver2();
    testdriver3();

    return 0;
}// end main


void testdriver1()
// Purpose:
// Precondition:
// Postcondition:
{
       int numbers[MAX_SIZE], size;
       int index, j, num;

       // Case 1: All the elements of array are above 0
       cout <<"Test Function: allPositive" << endl;
       cout << "Case 1:All the elements of array are above 0. "
       << endl;

       // initialize array and array size for the specific
       size = 5;
       num = 10;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num += 10;
          index++;
       }// end for j

       // call function
       cout << "The returned value is "
            << allPositive(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
       cout << "Case 2: None of the elements are above 0. " << endl;

       // initialize array for the specific test case
       size = 5;
       num = -12;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num--;
          index++;
       }// end for j
       // call function
      cout << "The returned value is "
            << allPositive(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
      printArray(numbers, size);

      pauseScreen();
       clearScreen();
       cout << "Case 3:Some of elements are  above 0   "
       << endl;

       size = 7;
       num = 3;
       index = 0;
       for ( j = 1; j <= size; j++)
       {
          numbers[index] = num;
          num--;
          index++;
       }// end for j
       // call function
      cout << "The returned value is "
            << allPositive(numbers, size)
            << endl;
       // display test data and results from function
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
}// end

void printArray(const int list[], int size)
// Purpose: display the size and contents of an integer array
// Precondition: values have been assigned to list[] and size
// Postcondition: the contents of the array list[] and its size are displayed
//                on the computer screen
{
     int i, index;

     cout << "Size of the array: " << size << endl;
     cout << "Contents of the array: ";
     index = 0;   // initialize array index

     for ( i = 1; i <= size; i++)
     {
         cout << list[index] << ", ";
         index++;          // increment array index
     }// end for i

     cout << endl;
}// end printArray
//===========================================================
void testdriver2()

{
   cout<<"testdriver2 in progress."<<endl;

}// end
//================================================================
void testdriver3()

{
   cout<<"testdriver3 in progress."<<endl;
}// end


//==================================================

int allPositive(int numbers[], int size)
// Purpose:This function tell whether the contents of the array are above zero.
// Precondition:Determine if the whole array  is positive.
// Postcondition:Tested in testdriver1
{
 int index,j,posCount;

   index = 0;
   posCount = 0;


   for (j = 1; j <= size; j++)
   {
      if ( numbers[index] >= FLAT_ZERO)
      {
         posCount++;
         }




      index++;
   } // end for k
if(posCount == size)
return 1;
else
return 0;
   }// end
//=======================================================

int findOddLocation (int numbers[], int size)
{

}// end
//========================================================
int sameTotals (int numbers[], int size)
{

}// end function
