// File Name ---lab5a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 5a
// Problem Description:Write a program for the Skegee Credit Card to process account operations for the month of  September.

// File Name ---lab5a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 5a
// Problem Description:Write a program for the Skegee Credit Card to process account operations for the month of  September.

// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 21;
const float INTEREST_REG = 0.18;
const float INTEREST_GOLD = 0.06;
const float OVER_BAL = 500.00;

// ================ Function Prototypes ==========================
void openFile(ifstream&);
void processbalfile( ifstream& , float&, float&, int&);
void displayBalance(float,float, int);
int main()
{
// data declarations
ifstream balfile;
int numCharge;
float totalPayment;
float finBal;
system("clear");
openFile(balfile);
processbalfile(balfile,finBal,totalPayment, numCharge);
displayBalance(finBal,totalPayment, numCharge);
balfile.close();
return 0;
}// end main

void openFile(ifstream& balfile)
// Purpose:
// Precondition:
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   balfile.open(filename);

   //verify link to file is GOOD
   if (!balfile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !balfile);
   }
// end openFile

void processbalfile(ifstream& balfile , float& finBal , float& totalPayment ,  int& numCharge)
// Purpose:
// Precondition:
// Postcondition:
 {
 cout<< "processbalfile is incomplete"<< endl;
   }
// end processbalfile


void displayBalance(float finBal , float totalPayment , int numCharge )
// Purpose:
// Precondition:
// Postcondition:
 {
 cout<< "displayBalance is incomplete"<< endl;
   }
// end displayBalance
