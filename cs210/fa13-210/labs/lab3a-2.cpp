//Filename:lab3a-2.cpp
// Name:Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1- Tuesday

//This program shows how to test a function using a test driver.

// ==================== header files ===============================
#include <iostream>           // for input/output
#include <iomanip>
#include <cmath>
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int CONST_NUM = 4;
const int EQUAL_NUM = 0;
const int CODE_A = 1;
const int CODE_B = 2;
const int CODE_C = 3;




// ================== data type declarations =======================


// =================== function prototype ==========================

void determineCode(float, float,float,int&);

int main()
{


 float a,b,c;
 int quadCode;


a = 0;
b = 0;
c = 0;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;

a = 1;
b = 1;
c = 1;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;

a = 0.9;
b = 0.9;
c = 0.9;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;

a = 1.1;
b = 1.1;
c = 1.1;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;

 a = -0.9;
b = -0.9;
c = -0.9;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;
a = 0;
b = 1;
c = 2;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;
a = -2;
b = -1;
c = 0;
determineCode(a,b,c,quadCode);
 cout << " Quadratic Code = "<< quadCode <<endl;

return 0;
}
void determineCode (float a ,float b ,float c,int& quadCode)

// Purpose: This function will determine the quadratic Code.
// Precondition:
//
// Postcondition:
//
{

   if (pow (b,2.0) - (CONST_NUM * a * c) == EQUAL_NUM  )
     {
         quadCode = CODE_A;
      }


     else if (pow (b,2.0) - (CONST_NUM * a * c) >  EQUAL_NUM )
        {
         quadCode = CODE_B;
        }

     else if (pow (b,2.0) - (CONST_NUM * a * c)  <  EQUAL_NUM )
             {

        quadCode = CODE_C;
              }

}   // end of function determineCode
