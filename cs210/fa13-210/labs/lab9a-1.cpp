// File Name ---lab9a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 9a
// Problem Description:


// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 25;
const int WIDTH2 =20;
const int MAX_SIZE = 23;
const int MIDTERM_Qs= 20;


// ================ Function Prototypes ==========================
void openFile(ifstream&);
void fileFill(ifstream&, int[], int);
void printArray(int[], int);
void testHeading();


int main()
{
// data declarations
ifstream answerfile;
int answers[MIDTERM_Qs];


clearScreen();
openFile(answerfile);
fileFill(answerfile,answers,MIDTERM_Qs);
testHeading();

   while(answerfile)
   {
printArray(answers,MIDTERM_Qs);

fileFill(answerfile,answers,MIDTERM_Qs);


   }

   return 0;
}// end main

void openFile(ifstream& answerfile)
// Purpose:
// Precondition:
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   answerfile.open(filename);

   //verify link to file is GOOD
   if (!answerfile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !answerfile);
   }
// end openFile


void fileFill(ifstream& answerfile, int numbers[], int size )
// Purpose:
// Precondition:
// Postcondition:
{
      int k, index, data;



      // initialize index
      index = 0;

      // test not end of numFile
      for(k = 1;answerfile && index < MIDTERM_Qs;k++)
      {
         answerfile >> data;


         // assign data to numbers array
         numbers[index] = data;

         // update data from scorceFile


         // update index
         index++;
      }// end while not end of numFile

}// end

void printArray(int numbers[], int size)
// Purpose:
// Precondition:
// Postcondition:
{
      int index, k;

      cout << "The array contents are: "<< endl;
      cout << "Array size = " << size << endl;

      // initialize array index
      index = 0;

      for (k = 1; k <= size; k++)
      {
         // display array numbers
         cout << numbers[index] << ", ";

         // update array index
         index++;
      }// end for k
      cout << endl;

}// end printArray

void testHeading()
{
   cout<<"Professor E.Z. Ayes Class Results"<<endl;
   cout<<"==============================================================================================="<<endl;
   cout<<setw(WIDTH)<<"Student No."
       <<setw(WIDTH)<<"Number Correct"
       << setw(WIDTH)<<"Mid Term"<<endl;
      cout<<"==============================================================================================="<<endl;

}//end TestHeading

