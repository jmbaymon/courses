// Filename:    programname.cpp
// Name: Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 2 - Tuesday and Thursday

// Problem Description:




// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>           // for output format
#include <fstream>
#include "utility.h"
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 21;


// ================== data type declarations =======================


// =================== function prototype ==========================
void openFile(ifstream&);
void processFacebookFile(ifstream&,int&, int&);
void computePrintAverage (int, int);

int main()
{
    // data declarations
    ifstream facefile;
    int totalAccounts , totalFriends;
   system("clear");
   openFile(facefile);
   processFacebookFile(facefile, totalAccounts , totalFriends);
   cout << "Number of facebook accounts: "<< totalAccounts<<endl
         << "Total number of friends for all accounts: "
         << totalFriends
         <<endl;

   computePrintAverage(totalAccounts , totalAccounts);
   facefile.close();


   return 0;
}// end main

void openFile(ifstream& infile)
// Purpose:
// Precondition:
// Postcondition:
{
   char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   infile.open(filename);

   //verify link to file is GOOD
   if (!infile)
   cout<< filename<< "is an invalid file name!Try again!"<<endl;
} while ( !infile);

}//end openFile

void processFacebookFile(ifstream& infile, int& total, int& allFriends)
// Purpose:
// Precondition:
// Postcondition:
{
   int numFriends;

   //initialize total, allFriends
   total = 0;
   allFriends = 0;

   //read numFriends from infile
   infile >> numFriends;

   //verify not end of file
   while(infile)
   {
      //update total(number of accounts processed)
      total++;

      //update total number of friends for all accounts
      allFriends += numFriends;


      //update infile
      infile >> numFriends;
   } // end while not end of infile

}//end processFacebookFile

void computePrintAverage(int allAccts , int allFriends)
// Purpose:
// Precondition:
// Postcondition:
{
float friend_average;


cout << fixed << showpoint << setprecision(DECIMAL);

if (allAccts != 0)
{
friend_average = float(allFriends)/allAccts;
cout << "Average number of Facebook friends per account:  "
<< friend_average << endl;
}//end if at least 1 account processed

else
cout << "No accounts were processed. The file was empty!"<< endl;

}//end computePrintAverage


