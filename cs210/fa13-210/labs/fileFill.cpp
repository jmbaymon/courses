// Filename:    programname.cpp
// Name: Ms. Thomas
// Class:   CSCI 210 Programming I
// Section/Lab: Section 2 - Tuesday and Thursday

// Problem Description:




// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>            // for output format
#include "utility.h"
#include <fstream>            // for file IO
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 21;
const int MAX_ARRAY_SIZE = 20;


// ================== data type declarations =======================


// =================== function prototype ==========================
void openFile(ifstream&);
void fileFill(ifstream&, int[], int&);
void printArray(int[], int);
void printReverse(int[], int);

int main()
{
   // data declarations
   int data[MAX_ARRAY_SIZE], n;
   ifstream dataFile;

   system("clear");
   openFile(dataFile);
   fileFill(dataFile, data, n);
   printArray(data, n);
   printReverse(data, n);
   dataFile.close();

   return 0;
}// end main

void fileFill(ifstream& numFile, int numbers[], int& size)
// Purpose:
// Precondition:
// Postcondition:
{
      int k, index, data;

      // initialize data by reading from numFile
      numFile >> data;

      // initialize index
      index = 0;

      // test not end of numFile
      while ( numFile && index < MAX_ARRAY_SIZE)
      {
         cout << "input value: #" << index
              << " is " << data << endl;

         // assign data to numbers array
         numbers[index] = data;

         // update data from numFile
         numFile >> data;

         // update index
         index++;
      }// end while not end of numFile
      size = index;
}// end

void printArray(int numbers[], int size)
// Purpose:
// Precondition:
// Postcondition:
{
      int index, k;

      cout << "The array contents are: "<< endl;
      cout << "Array size = " << size << endl;

      // initialize array index
      index = 0;

      for (k = 1; k <= size; k++)
      {
         // display array numbers
         cout << numbers[index] << ", ";

         // update array index
         index++;
      }// end for k
      cout << endl;

}// end printArray

void printReverse(int numbers[], int size)
// Purpose:
// Precondition:
// Postcondition:
{
      int index, k;

      cout << "The array contents are: "<< endl;
      cout << "Array size = " << size << endl;

      // initialize array index
      index = size-1;

      for (k = 1; k <= size; k++)
      {
         // display array numbers
         cout << numbers[index] << ", ";

         // update array index
         index--;
      }// end for k
      cout << endl;

}// end printArray

void openFile(ifstream& infile)
// Purpose:
// Precondition:
// Postcondition:
{
    char filename[MAX_SIZE];

    do
    {
       cout << "Enter the name of the input file: ";
       cin >> filename;
       cin.ignore(MAXNUMBER, EOLN);

       infile.open(filename);

       if (!infile)
         cout << filename
              << " was not opened. Try again!"
              << endl;
    } while ( !infile );
}//

