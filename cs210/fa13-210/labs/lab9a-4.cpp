// File Name ---lab9a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 9a
// Problem Description:Music Producer Alex Miller wants you to write a program to help determine the contestants’ performance and other statistics for last weeks’ TU’s Star competition. The competition has 25 contestants and the stats reflect 20 judges’ scores.


// ======================= Header Files ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 23;
const int WIDTH2 = 20;
const int EIGHTEEN = 18;
const int FOUR = 4;
const int MAX_SIZE = 20;
const int JUDGES= 20;
const int CONTEST_SPOTS = 25;
const int HUNDRED = 100;

// ================ Function Prototypes ==========================
void openFile(ifstream&);
void fileFill(ifstream&, int[], int);
void printArray(int[], int);
int computePoints(int[], int);
int underScores(float,int[],int);
int countAbove18(int[], int);
void scoringHeading();
void scoringResult(int, float, int );

int main()
{
// data declarations
ifstream scorefile;
int scores[JUDGES];
int numContest;
int totalPoints;
int underCount, countFour;
float avg,percentAbove18;




clearScreen();

openFile(scorefile);

fileFill(scorefile,scores,JUDGES);

scoringHeading();

numContest = 1;

countFour = 0;


   while(scorefile)
   {


   totalPoints = computePoints(scores,JUDGES);

      avg = totalPoints/JUDGES;

      underCount = underScores(avg,scores,JUDGES);

      cout << setw(WIDTH2) << numContest
      << setw(WIDTH2)<< avg
      <<setw(WIDTH2)<< underCount
      <<endl;

      if(countAbove18(scores, JUDGES) >= FOUR )

      ++countFour;

      fileFill(scorefile,scores,JUDGES);

      numContest++;

   }//end while

   percentAbove18 = countFour/float(CONTEST_SPOTS)* HUNDRED;
   // Testing percentAbove 18
   cout<<"Percent:"<<percentAbove18<<"%"<<endl;



scorefile.close();





return 0;
}// end main

void openFile(ifstream& scorefile)
// Purpose:
// Precondition:
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   scorefile.open(filename);

   //verify link to file is GOOD
   if (!scorefile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !scorefile);
   }
// end openFile


void fileFill(ifstream& scorefile, int numbers[], int size )
// Purpose:
// Precondition:
// Postcondition:
{
      int k, index, data;

      // initialize data by reading from scoreFile
      scorefile >> data;

      // initialize index
      index = 0;


      // test not end of numFile
      for(k = 1;scorefile && index < JUDGES;k++)
      {


         // assign data to numbers array
         numbers[index] = data;

         // update data from scorceFile
         scorefile >> data;

         // update index
         index++;
      }// end while not end of numFile

}// end



void scoringHeading()
{
   cout<<setw(WIDTH)<<"Contestant No."
       <<setw(WIDTH)<<"Average Score"
       << setw(WIDTH)<<"#Scores Under Average"<<endl;

}//end scoringHeading
int computePoints(int scores[], int size)
{
   int j, index;

   int points;

   //intialize
   index = 0;

   points = 0;



   for(j=1;j<=size;j++)
      {
         points = points + scores[index];


      //update index
         index++;
      }//end for j
return points;

}//end computePoints

int underScores(float avg , int scores[],int size)
{
int index ,k;

int avgCount;

index = 0;

avgCount = 0;

for(k=1;k<=size;k++)
{

   if(scores[index]< avg)
      avgCount++;


      index++;
}// end k

return avgCount;

}//end underScores

int countAbove18(int scores[], int size)
{
  int index,a;

  int count18;

  index = 0;

  count18 = 0;

   for(a=1;a<=size;a++)
   {
      if(scores[index]>= EIGHTEEN)

         count18++;

         index++;



   }// end a

   return count18;


}//end countAbove18






