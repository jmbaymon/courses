// Filename:    lab6a-b.cpp
// Name: Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 2 - Tuesday and Thursday

// Problem Description:
/*
  Write a program to demonstrate how to handle menu-driven
  programming. The program displays the following menu:

   1. Compute percent
   2. Generate a table
   3.Compute doubles
   4.Exit

  When the program executed, the program displays the menu and
  waits for the user to make a choice.

  If the user choose option 1, the program asks the user to enter a list of characters until 3 alphabets have been entered. Compute and print the percent of punctuation marks entered. After displaying the result, the program prompts the user to see if he/she wants to process another list. If the user does not want to continue, the program returns to the main menu; otherwise, the program asks the user to enter another list of characters until 3 alphabets have been entered and displays the proper result. This process continues until the user ends this option.
  If the user choose option  2, the program reads in the limit of the table (between 3 and 15 inclusive), and displays the table containing the square root and square in accordance with the limit (increment by 0.5) entered by the user (accurate to 2 decimal places). After displaying the table, the program prompts the user to see if he/she wants to generate another table. If the user does not want to continue, the program returns to the main menu; otherwise, the program asks the user to enter another limit and displays a new table. This process continues until the user ends this option.

  If the user choose option 3,
  the program that simulates rolling a pair of dice. Processing stops as soon as the number 6 appears on die 1 or the pair of dice has been rolled 12 times. The program prints the face-up values (die 1 and die 2) for each roll and the number of times both dice have the same face-up values (doubles). After displaying the message, the program then asks the user to see if he/she wants to roll the dice again. If the user does not want to continue, the program returns to the main menu; otherwise, the program repeats the process mentioned above. This process continues until the user ends this option. The program terminates when option 4 is selected.
  until the user presses <enter>. After the user presses <enter>,
  the program returns to the main menu.

  If the user chooses option  4, the program terminates.
 */

// ==================== header files ===============================
#include <iostream>           // for input/output
#include "utility.h"
#include <iomanip>
#include <cctype>
#include <cmath>
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int WIDTH2 = 10;
const int WIDTH3 = 10;
const int MAX = 3;
const int MIN = 1;
const float INCREMENT = 0.5;
const int SQUARE_NUM = 2;
const int ROLL_MAX = 12;
const int MAX_FACE = 6;
const int MIN_FACE = 1;
// =================== function prototype ==========================
void displayMenu();
void getChoice(int&);
void takeAction(int);
void computeDoubles();
void generateTable();

int main()
{
   int choice;

   do
   {
      displayMenu();
      getChoice(choice);
      takeAction(choice);
   } while ( choice != MAX );

   return 0;
}// end main

void displayMenu()
// Purpose: The function will display the main menu
// Precondition:
// Postcondition:
{
   cout << "Today's Menu" << endl << endl;
   cout <<"1. Generate Table. " << endl
   << "2.Compute doubles. " << endl
   << "3.Exit  " << endl << endl;
   cout << "Enter your choice ==> ";
}// end displayMenu function

void getChoice(int& option)
// Purpose: This function asks the user to enter a choice based on
// the menu. If the user enters a choice which is not on the
// menu, the function asks the user to re-enter the value. The
// process continues until the user enters a valid choice.
// Precondition:
// Postcondition:
{
   do
   {
        cin >> option;
        cin.ignore(MAXNUMBER, EOLN);

        if ( option < MIN || option > MAX )
        {
               cout << "The choice "
                    << option
                    << " is out of range."
                    << endl;
               cout << "Enter the choice again ==> ";
       } // end if invalid
   } while ( option < MIN || option > MAX);
}// end getChoice

void takeAction(int choice)
// Purpose: The function will perform the tasks selected by the user.
// Precondition:
// Postcondition:
{
  cout << "the choice is "
   << choice
   << endl;


    if ( choice == 1)
      generateTable();
   else if ( choice == 2)
     computeDoubles();
   else
      cout << "Terminating Program" << endl;
   pauseScreen();

}// end  takeAction

void computeDoubles()
{
   int face1, face2, rollNum;
   int count;

   clearScreen();
//intialize
   rollNum = 1;
   count = 0;

   while ( face1 == MAX_FACE ||rollNum <= ROLL_MAX)
   {
      face1 = random(MAX_FACE) + MIN_FACE;
      face2 = random(MAX_FACE) + MIN_FACE;

      if ( face1 == face2)
         ++count;

      cout << "("
           << face1
           << ","
           << face2
           << ")"
           << endl;

      // update
      rollNum++;


   }// end while

   cout << "Number of times Doubles occurred:"
        << count
        << " time(s)"
        << endl;


   pauseScreen();
}
void generateTable()
{
 int max_size;
 float num ,square,root;


   clearScreen();
   cout << "Enter table size:";
   cin >> max_size;
   cin.ignore(MAXNUMBER, EOLN);


cout << setw(WIDTH)<< " Number "
     << setw(WIDTH) << " Square Root "
    << setw(WIDTH)<< " Square "<< endl;

cout << fixed << showpoint << setprecision(DECIMAL);
for (num = 1;  num <= max_size ; num = num + INCREMENT)
{
   root = sqrt(num);
   square = pow(num,SQUARE_NUM);

   cout << setw(WIDTH) << num
         << setw(WIDTH2) << root
         << setw(WIDTH3) << square
           << endl;

}//end of table
pauseScreen();
}
/*
void functionName()
{

}
*/
