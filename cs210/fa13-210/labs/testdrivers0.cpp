// Filename:    programname.cpp
// Name: Ms. Thomas
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1 - Tuesday

// Problem Description:




// ==================== header files ===============================
#include <iostream>           // for input/output
#include <iomanip>            // for output format
#include "utility.h"
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 20;

// ================== data type declarations =======================


// =================== function prototypes ==========================
void testdriver1();
void testdriver2();
void printArray(const int[], int);
int sum5Compare(int[], int);
int find3rd30Index  (int[], int);

int main()
{

    clearScreen();
    testdriver1();
    testdriver2();

    return 0;
}// end main


void testdriver1()
// Purpose:
// Precondition:
// Postcondition:
{
       int numbers[MAX_SIZE], size;
       int index, k, num;

       // Case 1:
       cout <<"Test Function: sum5Compare" << endl;
       cout << "Case 1:  First five is larger than the last five"
       << endl;

       // initialize array and array size for the specific
       size = 6;
       num = 60;
       index = 0;
       for ( k = 1; k <= size; k++)
       {
          numbers[index] = num;
          num -= 10;
          index++;
       }// end for k

       // call function
       cout << "The returned value is "
            << sum5Compare(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
       cout << "Case 2: First five is smaller than the last five" << endl;

       // initialize array for the specific test case
       size = 8;
       num = 11;
       index = 0;
       for ( k = 1; k <= size; k++)
       {
          numbers[index] = num;
          num++;
          index++;
       }// end for k
       // call function
      cout << "The returned value is "
            << sum5Compare(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

      pauseScreen();
       clearScreen();
       cout << "Case 3:  first 5 sum is the same as the last 5"
       << endl;

       size = 5;
       num = 10;
       index = 0;
       for ( k = 1; k <= size; k++)
       {
          numbers[index] = num;
          num++;
          index++;
       }// end for k
       // call function
      cout << "The returned value is "
            << sum5Compare(numbers, size)
            << endl;
       // display test data and results from function
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
}// end

void printArray(const int list[], int size)
// Purpose: display the size and contents of an integer array
// Precondition: values have been assigned to list[] and size
// Postcondition: the contents of the array list[] and its size are displayed
//                on the computer screen
{
     int i, index;

     cout << "Size of the array: " << size << endl;
     cout << "Contents of the array: ";
     index = 0;   // initialize array index

     for ( i = 1; i <= size; i++)
     {
         cout << list[index] << ", ";
         index++;          // increment array index
     }// end for i

     cout << endl;
}// end printArray

//void function1(parameter list)
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end


void testdriver2()
// Purpose:
// Precondition:
// Postcondition:
{
       int numbers[MAX_SIZE], size;
       int index, k, num;

       // Case 1:
       cout <<"Test Function: find3rd30Index" << endl;
       cout << "Case 1: More than 3 numbers over 30 "
       << endl;

       // initialize array and array size for the specific
       size = 5;
       num = 30;
       index = 0;
       for ( k = 1; k <= size; k++)
       {
          numbers[index] = num;
          num++;
          index++;
       }// end for k

       // call function
       cout << "The returned value is "
            << find3rd30Index(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
       cout << "Case 2: less than 3 numbers over 30 "
       << endl;

       // initialize array and array size for the specific
       size = 6;
       num = 26;
       index = 0;
       for ( k = 1; k <= size; k++)
       {
          numbers[index] = num;
          num++;
          index++;
       }// end for k

       // call function
       cout << "The returned value is "
            << find3rd30Index(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
       cout << "Case 3: Exactly 3 numbers over 30 "
       << endl;

       // initialize array and array size for the specific
       size = 5;
       num = 29;
       index = 0;
       for ( k = 1; k <= size; k++)
       {
          numbers[index] = num;
          num++;
          index++;
       }// end for k

       // call function
       cout << "The returned value is "
            << find3rd30Index(numbers, size)
            << endl;

       // display test data - contents of the array and the result from the function call
       printArray(numbers, size);

       pauseScreen();
       clearScreen();
}// end

int sum5Compare(int data[], int n)
// Purpose:
// Precondition:
// Postcondition:
{
   int indexF, indexL, firstSum, lastSum, k;

    // initialize accumulators
   firstSum = 0;
   lastSum = 0;

   // initialize array indices
   indexF = 0;
   indexL = n - 1;

   for ( k = 1; k <= 5; k++)
   {

      // update accumulators
      firstSum += data[indexF];
      lastSum += data[indexL];

      // update array indices
      indexF++;
      indexL--;
   }// end for k

   // determine return value
   if ( firstSum > lastSum)
      return 1;
   else if ( firstSum < lastSum)
      return 0;
   else
      return -1;
}// end

int find3rd30Index (int numbers[], int n)
{
   int index, k, count30;

   index = 0;
   count30 = 0;

   for ( k = 1; k <= n; k++)
   {
      if ( numbers[index] > 30)
         count30++;

      if ( count30 == 3)
         return index;

      index++;
   } // end for k

   return - 1;
}// end function
