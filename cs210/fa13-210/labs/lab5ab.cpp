// File Name ---lab5a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 5a
// Problem Description:Write a program for the Skegee Credit Card to process account operations for the month of  September.

// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 5;
const float INTEREST_REG = 0.18;
const float INTEREST_GOLD = 0.06;
const float OVER_BAL = 500.00;

// ================ Function Prototypes ==========================
void initializeBalance( float& ,float&, int&);
void processOperation( float& ,float&, int&);
void displayBalance(float,float, int);
int main()
{
// data declarations
float strBal;
char statusCard;
char operType;
float operAmt;
int numCharge;
float totalPayment;
float finBal;
system("clear");
initializeBalance(finBal,totalPayment, numCharge);
processOperation(finBal,totalPayment, numCharge);
displayBalance(finBal,totalPayment, numCharge);
return 0;
}// end main
void initializeBalance(float& finBal ,float& totalPayment, int&numCharge)
// Purpose:
// Precondition:
// Postcondition:
{
   cout << "function initializeBalance is under construction " << endl;
   }
// end
void processOperation(float& finBal ,float& totalPayment, int& numCharge)
// Purpose:
// Precondition:
// Postcondition:
{
   cout << "function processOperation is under construction " << endl;
   }
// end

void displayBalance(float finBal , float totalPayment, int numCharge)
// Purpose: This function displays the output.
// Precondition:
// Postcondition::
{
cout << "function displayResults is under construction " << endl;
   }
// end
