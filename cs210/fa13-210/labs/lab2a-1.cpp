// Filename:lab2a-1.cpp
// Name:Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1- Tuesday

//

// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>            // for output format
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const float MAX_PRICE = 200.00;
const int MAX_NUM = 4;



// ================== data type declarations =======================


// =================== function prototype ==========================


int main()
{
    // data declarations
   int countSpent,readPurchase;



   float wonAmt,totalAmt,amtMsg;



   system("clear");
// initialize / read in input
cout<< "Enter amount won for the shopping spree:";
cin >> wonAmt;


   // initialize
   countSpent = 0;


// calculate output

while (countSpent < MAX_NUM)
{

cout << "Enter amount of purchase:"<<endl;
cin >> readPurchase;






if (readPurchase > MAX_PRICE)
++ countSpent;
totalAmt = readPurchase + readPurchase;






}

 amtMsg = totalAmt - wonAmt;


// display output

   cout<<fixed<<setprecision(2);
cout <<"Amount Rosa spent:"<<totalAmt<< endl;
cout<< "Rosa overspent by:"<< amtMsg<<endl;


   return 0;
}// end main

//datatype functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end

//void functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end
