// File Name ---lab5a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 5a
// Problem Description:Write a program for the Skegee Credit Card to process account operations for the month of  September.

// File Name ---lab5a.cpp
// Name: Jamal Baymon
// Course: CSCI 210
// Instructor: Dr. C. Thomas
// Assignment: Lab 5a
// Problem Description:Write a program for the Skegee Credit Card to process account operations for the month of  September.

// ======================= Header File ===========================
#include <iostream> // for input/output
#include <cstdlib> // for system
#include <iomanip> // for output format
#include <fstream>// for file I/O
#include "utility.h"
using namespace std;
// =================== Symbolic Constants ========================
const int DECIMAL = 2;
const int WIDTH = 5;
const int MAX_SIZE = 21;
const float INTEREST_REG = 0.18;
const float INTEREST_GOLD = 0.06;
const float OVER_BAL = 500.00;
const float FLAT_ZERO = 0.00;

// ================ Function Prototypes ==========================
void openFile(ifstream&);
void processbalfile( ifstream& , float&, float&, int&);
void displayBalance(float,float, int);
int main()
{
// data declarations
ifstream balfile;
int numCharge;
float totalPayment;
float finBal;
system("clear");
openFile(balfile);
processbalfile(balfile,finBal,totalPayment, numCharge);
displayBalance(finBal,totalPayment, numCharge);
balfile.close();
return 0;
}// end main

void openFile(ifstream& balfile)
// Purpose:Opens balfile and verify link.
// Precondition:
// Postcondition:
 {

char filename[MAX_SIZE];


   do
   {
      // get filename from the user

      cout << "Enter the name of the input file: ";
      cin >>filename;
      cin.ignore(MAXNUMBER , EOLN);

   // link the file to program
   balfile.open(filename);

   //verify link to file is GOOD
   if (!balfile)
   cout<< filename << " is an invalid file name! Re enter a valid file name."<<endl;
} while ( !balfile);
   }
// end openFile

void processbalfile(ifstream& balfile , float& finBal , float& totalPayment ,  int& numCharge)
// Purpose:Processes balfile ,finBal, totalPayment , and numcharge.
// Precondition:
// Postcondition:
 {
float strBal;
char statusCard;
char operType;
float operAmt;

//intialize numCharge,strBal,totalPayment
totalPayment = 0;
numCharge = 0;
strBal = 0;
balfile >> strBal;

finBal = strBal;

// read statusCard from balfile
balfile >> statusCard;
// read operType from balfile
  balfile >> operType;
 // read operAmt from balfile
balfile >> operAmt;

//not end of file
while(balfile)
{

if(operType == 'p' || operType == 'P')
{
   totalPayment = totalPayment + operAmt;
   finBal = finBal - operAmt;

}

else if (operType == 'c' || operType == 'C')
{
if(statusCard == 'r' || statusCard =='R')
finBal = finBal + operAmt  + (operAmt * INTEREST_REG);

else if(statusCard == 'g' || statusCard == 'G')
finBal = finBal + operAmt + (operAmt * INTEREST_GOLD);
//update Number of Charges
numCharge++;

}
// read operType from balfile
  balfile >> operType;
 // read operAmt from balfile
balfile >> operAmt;


}//end while not end of balfile


}// end processbalfile


void displayBalance(float finBal , float totalPayment , int numCharge )
// Purpose:Display results
// Precondition:
// Postcondition:
 {
cout << fixed << showpoint;
cout << setprecision(DECIMAL);
cout << endl << endl;

if(finBal!= FLAT_ZERO )

{
      if(finBal < OVER_BAL)
{
   cout <<"Number of Charges:"<< numCharge <<endl
     <<"Total Payment for Month: $"<< totalPayment<<endl;
cout<<"The final balance is less than 500 dollars: $"<<finBal<< endl;
}
      else
   {   cout <<"Number of Charges:"<< numCharge <<endl
     <<"Total Payment for Month: $"<< totalPayment<<endl;
      cout<<"The final balance is more than 500 dollars: $"<<finBal<< endl;
   }
   }
   else
   cout<< "Start Balance not processed. The file contains no data."<< endl;
 }
// end displayBalance
