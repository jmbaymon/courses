// Filename:lab1a-1.cpp
// Name:Jamal Baymon
// Class:   CSCI 210 Programming I
// Section/Lab: Section 1- Tuesday

//

// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>            // for output format
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;
const float PLAN1 = 505.00;
const float PRICE1 = 38.57;
const int DAYS = 7;
const float PER_MILES = .18;
const float STR_FEE = 0.345;


// ================== data type declarations =======================


// =================== function prototype ==========================


int main()
{
    // data declarations
    int mileage;

    float plan1,plan2,plan3;



   system("clear");
   // initialize / read in input
   cout << "Enter how many miles you will travel:";

   cin >> mileage;


// calculate output

plan1 = PLAN1;

plan2 = (PRICE1 * DAYS) + (PER_MILES * mileage);

plan3 = STR_FEE * mileage;





// display output

   cout<<fixed<<setprecision(2);

   cout << "Plan 1 Fees:" << PLAN1<< endl;

   cout << "Plan 2 Fees:"<< plan2 <<endl;

   cout << "Plan 3 Fees:"<< plan3 <<endl;


if (PLAN1 < plan2 && PLAN1 < plan3)
{
cout<< "Best Plan is: Plan 1:"<< PLAN1 << endl;
}


else if (plan2 < PLAN1 && plan2 < plan3)
{
   cout<< "Best Plan is:Plan 2:" << plan2<< endl;
}
else if(plan3 < PLAN1 && plan3 < plan2)
{
   cout<< "Best Plan is: Plan 3:" << plan3 << endl;
}
   return 0;
}// end main

//datatype functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end

//void functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end
