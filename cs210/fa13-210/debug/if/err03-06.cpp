//   file name -- err03-06.cpp
//   This program reads two integers and prints the larger one.

#include <iostream>
using namespace std;


int main()
{
   int first, second;

   // get numbers
   cout << "Enter first number: ";
   cin >> first;
   cout << "Enter second number: ";
   cin >> second;

   // determine which number is larger
   if (first >= second)
      cout << first << " is larger than "<< second << endl;
   else
      cout << second << " is larger than " << first <<endl;

   return 0;
}   // end of function main
