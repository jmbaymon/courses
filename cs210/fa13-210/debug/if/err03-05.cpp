//   file name -- err03-05.cpp
//   This program reads a character and checks to see if it is a letter.

#include <iostream>
#include <cctype>
using namespace std;


int main()
{
   char ch;

   // get a character
   cout << "Enter a character: ";
   cin >> ch;

   // determine whether or not the character is a letter
   if (not isalpha(ch)) then
      cout << number << " is a letter" << endl;
   else
      cout << number << " is not a letter" << endl;

   return 0;
}   // end of function main
