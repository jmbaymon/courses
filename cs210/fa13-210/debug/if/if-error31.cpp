#include <iostream>
using namespace std;

int main()
{
   char ch;

   ch = 't';

   if ( ch == tolower('T'))
       cout << ch << " is the letter T" <<endl;
   else
      cout << ch << " is not the letter T" << endl;

   return 0;
}// end main
