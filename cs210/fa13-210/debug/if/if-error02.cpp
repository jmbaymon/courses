#include <iostream>
using namespace std;

int main()
{
   int x, y;

   x = 12;
   y = 3;

   if (x < y)
      cout << y <<" is larger than " << x << endl;
   else
     cout << x <<" is larger than " << y << endl;

   return 0;
}// end main
