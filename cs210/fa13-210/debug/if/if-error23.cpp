#include <iostream>
using namespace std;

int main()
{
   int p, r;

   p = 15;
   r = 10;

   if ( p == r )
       cout << p << " and " << r << " are the same" << endl;
   else
       cout << p << " and " << r << " are not the same" << endl;

   return 0;
}// end main
