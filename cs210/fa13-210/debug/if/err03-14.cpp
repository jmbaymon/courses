//   file name -- err03-14.cpp
//   This program reads a character and determines whether or not it
//   is either an alphabet or a digit character

#include <iostream>
using namespace std;

int main()
{
   char ch;

   // get a character
   cout << "Enter a character: ";
   cin >> ch;

   // determine whether or not the character is either an alphabet or
   // a digit character
   if (isalpha(ch) || isdigit(ch))
      cout << ch << " is either an alphabet or a digit" << endl;
   else
      cout << ch << " is neither an alphabet nor a digit" << endl;

   return 0;
}   // end of function main
