//   file name -- err03-12.cpp
//   This program reads an integer and determines whether or not it is
//   an even number.

#include <iostream>
using namespace std;

int main()
{
   int number;

   // get a number
   cout << "Enter an integer: ";
   cin >> number;

   // determine whether or not number is an even number
   if (number % 2 <> 0)
      cout << number << " is not an even number." << endl;
   else
      cout << number << " is an even number." << endl;

   return 0;
}   // end of function main
