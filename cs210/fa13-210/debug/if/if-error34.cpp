#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	int a , b, c;

	a = 7;
	b = 5;
	c = 10;

	if ( a > 8 )
	   b = 2 * c;
	else if ( a > 5 )
	   c = a + 5;
	else
	   a = b + c;

	cout << setw(3) << a 
	     << setw(3) << b
		 << setw(3) << c
		 << endl;

   return 0;
}// end main
