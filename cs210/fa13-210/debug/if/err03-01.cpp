//   file name -- err03-01.cpp
//   This program reads an integer and checks to see if it is a positive
//   number.

#include <iostream>
using namespace std;

int main()
{
   int number;

   // get an integer
   cout << "Enter an integer: ";
   cin >> number;

   // determine whether or not the number is a positive integer
   if (number > 0)
      cout << number << " is a positive number" << endl;
   else
      cout << number << " is not a positive number" << endl;

   return 0;
}   // end of function main
