#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	int a , b, c;

	a = 2;
	b = 4;
	c = 6;

	if ( a > 5 )
	   b = 2 * c;
	else if ( a == 5 )
	   c = a + 5;
	else
	   a = b + c;

	cout << setw(3) << a 
	     << setw(3) << b
		 << setw(3) << c
		 << endl;

   return 0;
}// end main
