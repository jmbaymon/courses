//   file name -- err03-18.cpp
//   This program reads an integer representing a temperature reading
//   obtained in Texas. Based on the temperature reading, prints a
//   message in accordance with the following criteria:
//
//           tempeature reading           message
//              below 40                  Very cold
//              [41, 60]                  Cool
//              [61, 70]                  Moderate
//              [71, 85]                  Warm
//              above 85                  Hot

#include <iostream>
using namespace std;

int main()
{
   int reading;

   // get the temperature reading
   cout << "Enter the temperature reading: ";
   cin >> reading;

   // print the proper message based on the reading
   if (reading < 40)
      cout << "Very cold" << endl;
   else if (reading <= 60)
      cout << "Cool" << endl;
   else if (reading <= 70)
      cout << "Moderate" << endl;
   else if (reading <= 85)
      cout << "Warm" << endl;
   else
      cout<< "Hot" << endl;

   return 0;
}   // end of function main
