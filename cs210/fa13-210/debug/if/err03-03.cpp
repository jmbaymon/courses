//   file name -- err03-03.cpp
//   This program reads an integer and checks to see if it is a multiple
//   of 4.

#include <iostream>
using namespace std;


int main()
{
   int number;

   // get an integer
   cout << "Enter an integer: ";
   cin >> number;

   // determine whether or not the number is a multiple of 4
   if (number % 4 > 0)
      cout << number << " is not a multiple of 4" << endl;
   else
      cout << number << " is a multiple of 4" << endl;

   return 0;
}   // end of function main
