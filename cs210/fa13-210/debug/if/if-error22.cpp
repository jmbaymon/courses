#include <iostream>
using namespace std;

int main()
{
   char ch;

   ch = 't';

   if ( ch == 'A' && 'a')
       cout << ch << " is the letter A" <<endl;
   else
      cout << ch << " is not the letter A" << endl;

   return 0;
}// end main
