//   file name -- err03-20.cpp
//   This program reads an integer and determines whether or not it
//   is a multiple of 5.

#include <iostream>
using namespace std;

int main()
{
   int number;

   // get data
   cout << "Enter an integer: ";
   cin >> number;

   // print the proper message based on the number
   if (number % 5 == 0)
   {
      cout << number;
      cout << " is a multiple of 5" << endl;
   else
   {
      cout << number;
      cout << " is not a multiple of 5" << endl;
   }
   return 0;
}   // end of function main
