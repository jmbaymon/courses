//   file name -- err03-04.cpp
//   This program reads a character and checks to see if it is a letter.

#include <iostream>
using namespace std;


int main()
{
   char ch;

   // get a character
   cout << "Enter a character: ";
   cin >> ch;

   // determine whether or not the character is a letter
   if (isalpha(ch))
      cout << ch << " is a letter" << endl;
   else
      cout << ch << " is not a letter" << endl;

   return 0;
}   // end of function main
