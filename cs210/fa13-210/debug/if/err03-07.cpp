//   file name -- err03-07.cpp
//   This program reads two integers representing the x and y coordinates
//   of a point in Cartesian Coordinate.  It determines whether or not
//   the point is in the first quadrant.

#include <iostream>
using namespace std;


int main()
{
   int x, y;

   // get coordinates
   cout << "Enter x: ";
   cin >> x;
   cout << "Enter y: ";
   cin >> y;

   // determine whether or not (x, y) is in the first quadrant
   If (x > 0 && y > 0)
      cout << "(" << x << ", " << y << ") is in 1st quadrant" << endl;
   else
      cout << "(" << x << ", " << y << ") is not in 1st quadrant" << endl;

   return 0;
}   // end of function main
