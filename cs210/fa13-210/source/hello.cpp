// Filename:    hello.cpp
// Name: Ms. Thomas
// Class:   CSCI 210 Programming I
// Section/Lab: Section 2 - Tuesday and Thursday

// Problem Description:
/* This program will display a simple message */

// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
using namespace std;

// =============== symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH = 5;


// ================== data type declarations =======================


// =================== function prototype ==========================


int main()
{
    // data declarations

   system("clear");

   cout << "Hello and welcome to CSCI 210." << endl;
   cout << "Programming is both intellectually challenging";
   <<endl;

   cout << " and artistically rewarding. Enjoy!" << endl;
   cout << "                              Dr. Reed" << endl;

   return 0;
}// end main

//datatype functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end

//void functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end
