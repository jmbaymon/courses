// Filename:      programname.cpp
// Name:          Ms. Thomas
// Class:         CSCI 210 Programming I
// Section/Lab:   Section 2 - Tuesday and Thursday

// Problem Description:




// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>            // for output format
using namespace std;

// ============ symbolic constants ===========================
const int DECIMAL = 2;
const int WIDTH = 5;


// ============== data type declarations =======================


// =================== function prototype ==========================


int main()
{
    // data declarations
   float tempInFahr;

   system("clear");
   // initialize / read in input
   cout << "Enter the temperature (in degrees Fahrenheit): ";
   cin >> tempInFahr;

   // calculate and display output
   cout << "You entered " << tempInFahr
        << " degrees Fahrenheit."  << endl
        << "That's equivalent to " << (5/9 * (tempInFahr -32))
        << " degrees Celsius." << endl;


   return 0;
}// end main

//datatype functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end

//void functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end
