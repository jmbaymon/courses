// Filename:    programname.cpp
// Name: Ms. Thomas
// Class:   CSCI 210 Programming I
// Section/Lab: Section 2 - Tuesday and Thursday

// Problem Description:




// ==================== header files ===============================
#include <iostream>           // for input/output
#include <cstdlib>
#include <iomanip>            // for output format
#include <fstream>
#include "utility.h"
using namespace std;

// ============ symbolic constants ==============================
const int DECIMAL = 2;
const int WIDTH_M = 6;
const int WIDTH_L = 10;
const int MAX_SIZE = 20;


// =============== data type declarations =======================
struct orderType
{
   int itemCode;
   float price;
   char desc[21];
   int crdCardNum;
   char dateOrdered[21];
   char status;
};

// ============== function prototype ==========================
void openFile(ifstream&);
void readOrder(ifstream&, orderType&);
void printOrder(orderType);
void printHeaders();

int main()
{
    // data declarations
    orderType birthdayGifts;
    orderType giftList[MAX_SIZE];
    ifstream giftFile;

    system("clear");
    openFile(giftFile);
    printHeaders();
    readOrder(giftFile, birthdayGifts);


   return 0;
}// end main

//datatype functionName()
// Purpose:
// Precondition:
// Postcondition:
//{

//}// end

void printOrder(orderType gift)
// Purpose:
// Precondition:
// Postcondition:
{
   cout << setw(WIDTH_L) << gift.crdCardNum
        << setw(WIDTH_L) << gift.price
        << setw(WIDTH_M) << gift.itemCode
        << setw(WIDTH_M) << gift.status
        << endl;
}// end

void printHeaders()
// Purpose:
// Precondition:
// Postcondition:
{
   cout << setw(WIDTH_L) << "Credit Card#"
        << setw(WIDTH_L) << "Price"
        << setw(WIDTH_M) << "Code"
        << setw(WIDTH_M) << "Status"
        << endl;
}// end

void openFile(ifstream& infile)
// Purpose:
// Precondition:
// Postcondition:
{
    char filename[MAX_SIZE];

    do
    {
       cout << "Enter the name of the input file: ";
       cin >> filename;
       cin.ignore(MAXNUMBER, EOLN);

       infile.open(filename);

       if (!infile)
         cout << filename
              << " was not opened. Try again!"
              << endl;
    } while ( !infile );
}//

void  readOrder(ifstream& numFile, orderType& purchase)
// Purpose:
// Precondition:
// Postcondition:
{
      // initialize data by reading from numFile
      numFile >> purchase.crdCardNum;

            // test not end of numFile
      while ( numFile )
      {
         numFile >> purchase.price
                 >> purchase.status
                 >> purchase.itemCode;

         printOrder(purchase);

                 // update data from numFile
         numFile >> purchase.crdCardNum;

      }// end while not end of numFile

}// end

